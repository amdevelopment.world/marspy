import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyConfiguration
from marspy import MarspyTask
from marspy import MarspyMongo

logging.setLevelFirst(logging.INFO)

NUMBER_OF_TASKS = 100
COLLECTION_NAME = "marspy_test"

async def main():
    await MarspyEnvironment.get("PROD.MONGO")

    configuration = MarspyConfiguration({
        "name": "marspy-concurrency-test",
        "subscription_interval": 2
    })

    await configuration.set_schema({
        "properties": {
            "concurrency": {
                "type": "number",
                "minimum": 0,
                "default": 0,
                "title": "Maximum number of simulateous requests to MongoDB"
            }
        }
    })

    async def configuration_changed(configuration):
        print(f"modified concurrency:{configuration['concurrency']}")
        await mongo.set_concurrency(configuration["concurrency"])

    configuration = await configuration.subscribe(configuration_changed)
    print(f"initial concurrency:{configuration['concurrency']}")

    mongo = MarspyMongo({
        "url": "mongodb://localhost:27017/",
        "concurrency": configuration["concurrency"]
    })

    await mongo.create_collection({
        "name": COLLECTION_NAME,
        "indexes": ["name"]
    })

    async def task(task_id):
        number = 0
        while True:
            number += 1
            name = f"test_{task_id}"
            try:
                await mongo.upsert_document(COLLECTION_NAME, {
                    "$set": {
                        "name": name,
                        "a number": number
                    }
                }, {
                    "name": name,
                })
                await mongo.get_documents(COLLECTION_NAME, "name", name)
                await mongo.delete_document(COLLECTION_NAME, {
                    "name": name
                })
            except RuntimeError:
                pass
            except Exception as e:
                logging.error(e, type(e).__name__)
                logging.error(e)

    for task_id in range(NUMBER_OF_TASKS):
        MarspyTask(task(task_id))

    last_tasks_completed = 0

    while True:
        await asyncio.sleep(5)
        tasks_completed = mongo.tasks_completed()
        tasks_completed_per_second = int((tasks_completed - last_tasks_completed)/5)
        last_tasks_completed = tasks_completed
        print(f"{mongo.tasks_pending()} in queue, {tasks_completed} completed {tasks_completed_per_second} per second")

    await configuration.delete()
    logging.info("done testing MarspyTask")

try:
    asyncio.run(main())
except KeyboardInterrupt:
    pass
