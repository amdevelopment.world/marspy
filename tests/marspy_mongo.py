import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment, MarspyMongo

logging.setLevelFirst(logging.INFO)

async def main():
    environment = await MarspyEnvironment.get("PROD.MONGO")

    mongo = MarspyMongo({
        "url": environment["MONGO_URL"],
        "database": "orchestrator"
    })

    document = await mongo.get_documents("environments", "name", "PROD")
    logging.info(f"document: {document}")

    await mongo.stop()
    logging.info("done testing MarspyMongo")

asyncio.run(main())
