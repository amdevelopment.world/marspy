import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyRedshift
from marspy import MarspyStream

logging.setLevelFirst(logging.INFO)

S3_URL = "s3://big-data-media/playground/marspy_tests/s3_to_redshift.json"
REDSHIFT_TABLE = "marspy_redshift_test"

async def main():
    await MarspyEnvironment.get("PROD")

    # create data in S3
    s3 = MarspyStream({
        "url": S3_URL
    })

    await s3.put([
        {
            "uid": "homer",
            "firstname": "Homer",
            "lastname": "Simpson"
        }, {
            "uid": "marge",
            "firstname": "Marge",
            "lastname": "Simpson"
        }, {
            "uid": "bart",
            "firstname": "Bart",
            "lastname": "Simpson"
        }, {
            "uid": "lisa",
            "firstname": "Lisa",
            "lastname": "Simpson"
        }, {
            "uid": "maggie",
            "firstname": "Maggie",
            "lastname": "Simpson"
        }
    ])
    
    await s3.stop()

    # import data into redshift
    redshift = MarspyRedshift()

    await redshift.query(f"""
        CREATE TABLE IF NOT EXISTS "{REDSHIFT_TABLE}" (LIKE "marspy_gigya");
    """)

    result = await redshift.load({
        "url": S3_URL,
        "into": REDSHIFT_TABLE
    })

    result = await redshift.query(f"""
        SELECT COUNT(*) FROM "{REDSHIFT_TABLE}";
    """)

    print(result)

    await redshift.query(f"""
       -- DROP TABLE "{REDSHIFT_TABLE}";
    """)

    await redshift.stop()
    logging.info("done testing MarspyRedshift")

asyncio.run(main())
