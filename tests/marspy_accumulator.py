import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyAccumulator

logging.setLevelFirst(logging.INFO)

async def main():
    accumulator = MarspyAccumulator()

    async def async_do(items):
        logging.info("\"async do\" called")

        for item in items:
            logging.info(f"    {item}")

    def sync_do(items):
        logging.info("\"sync do\" called")

        for item in items:
            logging.info(f"    {item}")

    for i in range(0, 102):
        await accumulator.accumulate(10, f"item{i}", async_do)

    await accumulator.finish()

    for i in range(0, 102):
        await accumulator.accumulate(10, f"item{i}", sync_do)

    await accumulator.finish()
    logging.info("done testing MarspyAccumulator")

asyncio.run(main())
