import asyncio
from marspy import MarspyLogging as logging

async def main():
    def log_at_all_levels(message):
        print(f"-------- {message} --------")
        print(f"log level: {logging.level} disabled: {logging.disabled}")
        logging.debug("logging debug")
        logging.info("logging info")
        logging.warning("logging warning")
        logging.error("logging error")
        logging.critical("logging critical")

    log_at_all_levels("before setLevel")
    
    logging.setLevel(logging.DEBUG)
    log_at_all_levels("DEBUG")
    logging.setLevel(logging.INFO)
    log_at_all_levels("INFO")
    logging.setLevel(logging.WARNING)
    log_at_all_levels("WARNING")
    logging.setLevel(logging.ERROR)
    log_at_all_levels("ERROR")
    logging.setLevel(logging.CRITICAL)
    log_at_all_levels("CRITICAL")

asyncio.run(main())
