import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyFissionFunction

logging.setLevelFirst(logging.INFO)

async def loop():
    environment = await MarspyEnvironment.get("PROD")

    fission_configuration = {
        "router": environment["FISSION_ROUTER"],
        "path": "echo",
        "concurrency": 5
    }

    function = MarspyFissionFunction(fission_configuration)

    def callback(error, result, payload):
        if error:
            logging.info(f"the function returned error {error}")
        else:
            logging.info(f"result type:{type(result)}result:{result}\npayload:{payload}")

    for i in range(1, 11):
        await function.post({
            "first": "Bart",
            "last": "Simpson",
            "index": i
            }, callback)
        logging.info(f"function call #{i}")

    await function.finish()

    logging.info("done testing fission")

asyncio.run(loop())

