import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyConfiguration

logging.setLevelFirst(logging.INFO)

# -----------------------------------------------------------------------------------------------
# simply get the configuration from yaml files
# -----------------------------------------------------------------------------------------------

async def yaml_configuration():
    # read the configuration in a local yaml file
    conf = MarspyConfiguration.read_yaml("DEV")
    logging.info(f"configuration in yaml: {conf}")

# -----------------------------------------------------------------------------------------------
# get, and subscribe to a configuration stored in documentDb
# -----------------------------------------------------------------------------------------------

async def simple_configuration():
    await MarspyEnvironment.get("PROD")

    configuration = MarspyConfiguration()

    def configuration_changed(modified_configuration):
        logging.info(f"modified configuration: {modified_configuration}")

    # subscribe can be called with 1, 2 or 3 parameters, the callback is required, a name and default values for the configuration are optional
    current_configuration = await configuration.subscribe(configuration_changed, {
        "number": 123,
        "message": "the quick brown fox jumps over the lazy dog",
        "OK": True
    })

    logging.info(f"current_configuration: {current_configuration}")

    # sleep for two minutes while waiting for changes
    await asyncio.sleep(120)
    await configuration.delete()
    await configuration.stop()
    logging.info("done testing simple_configuration")

# -----------------------------------------------------------------------------------------------
# get, and subscribe to a another configuration stored in documentDb described with a JSON schema
# -----------------------------------------------------------------------------------------------

async def configuration_with_schema():
    await MarspyEnvironment.get("PROD")

    configuration = MarspyConfiguration({
        "name": "configuration with schema"
    })

    # optional, declare a JSON schema
    await configuration.set_schema({
        "required": [],
        "properties": {
            "number": {
                "type": "number",
                "minimum": 0,
                "default": 0
            },
            "name": {
                "type": "string",
                "enum": ["Marge", "Homer", "Lisa", "Bart", "Maggie"],
                "default": "Bart"
            },
            "boolean": {
                "type": "boolean",
                "default": True,
                "title": "A Boolean with a Title"
            }
        },
        # "users": ["martijn.s@gmail.com"]
    })

    def configuration_changed(modified_configuration):
        logging.info(f"modified configuration: {modified_configuration}")

    # subscribe can be called with 1, 2 or 3 parameters, the callback is required, a name and default values for the configuration are optional
    current_configuration = await configuration.subscribe(configuration_changed)
    logging.info(f"current_configuration: {current_configuration}")

    # sleep for two minutes while waiting for changes
    await asyncio.sleep(120)
    await configuration.delete()
    await configuration.stop()
    logging.info("done testing MarspyConfiguration")


async def fails():
    await MarspyEnvironment.get("PROD")

    configuration = MarspyConfiguration({
        "name": "configuration with schema"
    })

    def callback(configuration):
        pass

    try:
        await configuration.set()
    except Exception as e:
        print(e)

    try:
        await configuration.set("name")
    except Exception as e:
        print(e)

    try:
        await configuration.subscribe(callback)
    except Exception as e:
        print(e)

    try:
        await configuration.subscribe({})
    except Exception as e:
        print(e)

    try:
        await configuration.subscribe("name")
    except Exception as e:
        print(e)

    

    
# -----------------------------------------------------------------------------------------------
# demo :-)
# -----------------------------------------------------------------------------------------------

async def demo():
    # await fails()
    await yaml_configuration()
    # await simple_configuration()
    # await configuration_with_schema()

asyncio.run(demo())
