from datetime import datetime
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyKafka

logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("PROD.KAFKA.AWS")

    kafka = MarspyKafka({
        "topic": "marspy-kafka-test"
    })

    for i in range(0, 10):
        await kafka.put({
            "hello": "world",
            "index": i,
            "date": datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)")
        })

    await kafka.stop()
    logging.info("done testing MarspyKafka")

asyncio.run(main())
