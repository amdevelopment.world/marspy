import asyncio
import random
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment, MarspyMetrics

logging.setLevelFirst(logging.INFO)

async def main():
    environment = await MarspyEnvironment.get("DEV")

    metrics = MarspyMetrics({
        "name": "test-metrics",
        "url": environment["MONGO_URL"],
        "database": "mars-dashboard-dev",
        "duration": 60
    })

    for i in range(2 * 60):
        metrics.measure("temperature", random.randint(4, 30))

        for car in range(random.randint(1, 3)):
            metrics.count("car")
            metrics.signal("car", f"{random.randint(1, 2)}")

        await asyncio.sleep(1)

    await metrics.stop()

    logging.info("done testing MarspyMetrics")

asyncio.run(main())
