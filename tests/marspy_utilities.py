import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyUtilities

logging.setLevelFirst(logging.INFO)

async def main():
    root_path = MarspyUtilities.find_directory_containing("logs/")
    logging.info(f"find_directory_containing: {root_path}")

    url = MarspyUtilities.processURL('http://domain.com', {
        "scheme": "https",
        "user": "user",
        "password": "psw"
    })

    logging.info(url)

    async def async_fn():
        return 123

    def sync_fn():
        a = 123
        b = 321
        return a, b

    def function_with_named_args(**kwargs):
        print(kwargs)


    function_with_named_args(name="Bart", family_name="Simpsons")

    logging.info(sync_fn()[0])

    logging.info("sync_fn is " + ("async" if MarspyUtilities.is_async(sync_fn) else "sync"))
    logging.info("async_fn is " + ("async" if MarspyUtilities.is_async(async_fn) else "sync"))

    logging.info(await MarspyUtilities.smart_await(async_fn()))
    logging.info(await MarspyUtilities.smart_await(sync_fn()))

    logging.info("done testing MarspyUtilities")

asyncio.run(main())
