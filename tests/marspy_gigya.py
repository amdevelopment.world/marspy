import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyGigya
from marspy import MarspyAccumulator
from marspy import MarspyUtilities
from marspy import MarspyStream
from marspy import MarspyRedshift
from marspy import MarspyData

logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("PROD")

    gigya = MarspyGigya()
    #redshift = MarspyRedshift()
    accumulator = MarspyAccumulator()

    stream = MarspyStream({
        "url": "s3://big-data-media/playground/gigya/audit_log.json"
    })


    def map_user(user):
        # insert code here
        return user

    def stream_to_s3(records):
        stream.put(records)

    line_count = 0
    async def receive_from_gigya(line):
        MarspyUtilities.prettyprint(line)
        nonlocal line_count
        line_count += 1
        if (line_count % 1000) == 0:
            print(line_count)
        await accumulator.accumulate(100, line, stream_to_s3)
    await gigya.query_api("audit.search", 'select * from auditLog WHERE NOT endpoint = "accounts.search"', receive_from_gigya)
    #await gigya.query("select * from accounts limit 10", receive_from_gigya)
    #await gigya.query("select * from accounts", receive_from_gigya)

    #await redshift.load({
    #    "from": "s3://big-data-media/playground/microservice-template/gigya_example.json",
    #    "into": "marspy_gigya"
    #})

    await gigya.stop()
    await accumulator.stop()
    await stream.stop()
    #await redshift.stop()

    logging.info("done testing MarspyGigya")


asyncio.run(main())
