function.py contains a "fission" serverless function

#to deploy

create the function
```bash
fission function create --name echo --env python --code function.py
```

create the http route

```bash
fission route create --method POST  --url /echo --function echo 
```

#to update

```bash
 fission function update --name echo --code function.py 
```

#to test

```bash
 fission function test --name echo
```
