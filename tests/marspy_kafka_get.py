import asyncio
from marspy import MarspyEnvironment, MarspyKafka
from marspy import MarspyLogging as logging

logging.setLevelFirst(logging.INFO)

async def main():
    logging.setLevel(logging.WARN)
    await MarspyEnvironment.get("PROD.KAFKA.AWS")

    kafka = MarspyKafka({
        "topic": "marspy_test",
        "from": "start",
        "group_id": "marspy_test"
    })

    await kafka.create_topic()

    def receive_from_kafka_with_info(line, info):
        print(info["timestamp"], line)

    kafka.get(receive_from_kafka_with_info)

    # go thru main loop 12 times, then stop, a main loop is required when consuming from a kafka topic
    counter = 12
    while counter:
        counter -= 1
        await kafka.put({
            "message": "hello"
        })

        await asyncio.sleep(10)


    # clean up
    await kafka.delete_topic()
    await kafka.stop()
    print("done testing kafka")

asyncio.run(main())
