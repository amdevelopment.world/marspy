from marspy import MarspyLogging as logging

logging.setLevel(logging.ERROR)

print("testing marspy_accumulator")
import marspy_accumulator

print("testing marspy_configuration one minute")
import marspy_configuration

print("testing marspy_environment")
import marspy_environment

print("testing marspy_fission_function")
import marspy_fission_function

print("testing marspy_gigya")
import marspy_gigya

print("testing marspy_http")
import marspy_http

print("testing marspy_kafka")
import marspy_kafka

print("testing marspy_logging")
import marspy_logging

#print("testing marspy_metrics two minutes")
#import marspy_metrics

print("testing marspy_mongo")
import marspy_mongo

print("testing marspy_sqs")
import marspy_sqs

print("testing marspy_neo4j")
import marspy_neo4j

print("testing marspy_queue")
import marspy_queue

print("testing marspy_sql")
import marspy_sql

print("testing marspy_stream")
import marspy_stream

print("testing marspy_utilities")
import marspy_utilities