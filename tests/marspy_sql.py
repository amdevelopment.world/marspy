import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspySQL
from marspy import MarspyCryo
from marspy import MarspyRedshift

logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("PROD")

    sql = MarspySQL({
        "database": "a360"
    })

    async def callback(row):
        logging.info(row)

    result = await sql.query('SELECT count(*) FROM marspy_gigya')
    print(result)

    await sql.stop()


    result = await MarspyRedshift.query('SELECT count(*) FROM marspy_gigya')
    print(result)
    result = await MarspyRedshift.query('SELECT count(*) FROM marspy_gigya')
    print(result)

    result = await MarspyCryo.query('SELECT count(*) FROM article.article')
    print(result)
    result = await MarspyCryo.query('SELECT count(*) FROM article.article')
    print(result)

    
    logging.info("done testing MarspySQL")

asyncio.run(main())
