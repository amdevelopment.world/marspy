import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyEmail


logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("PROD")
    
    #-------------------------- configuration avec les valeurs par défaut
    # notez que "to" peut etre une addresse dans un str, ou une list

    email = MarspyEmail({
        "from": "Gestion Donnees <gestion-donnees@rtbf.be>",
        "to": "martijn.s@gmail.com",
        "subject": "alerte !"
    })

    #-------------------------- example #1
    await email.send("il pleut !")

    #-------------------------- example #2
    body = """
    <html>
        <body>
            <img src="machin.jpg" />
        </body>
    </html>
    """

    await email.send({
        "to": "martijn.s@gmail.com",
        "subject": "test with html body",
        "body": body
    })

    logging.info("done testing MarspyEmail")


asyncio.run(main())
