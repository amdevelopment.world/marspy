import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment

logging.setLevelFirst(logging.INFO)

async def main():
    prod = await MarspyEnvironment.get("PROD")

    environment = MarspyEnvironment({
        # optional "repository_secret_name" can contain the name of the secret that contains the url to the mongo db containing envs
        #"repository_secret_name": "MARSPY_REPOSITORY_URL",
        # optional, for testing you can provide a url directly
        #"repository_url": "localhost:27017/orchestrator"
    })

    # list all environment documents
    list = await environment.list()
    printable_list = '\n'.join(list)
    logging.info(f"---- list ----\n{printable_list}")

    # list all environment documents
    list = await environment.list_hierarchical()
    printable_list = '\n'.join(list)
    logging.info(f"---- hierarchical list ----\n{printable_list}")

    # get all values contained in the PROD document, including secrets from SSM
    values = await environment.get("PROD")
    logging.info("---- values in \"PROD\" ----")
    for name in values:
        logging.info(f'{name}: "{values[name]}"')

    # get all values contained in the PROD and UAT.MONGO documents, in that order
    # here i'm using a comma separated list of environment names, you can also provide a list like ["PROD", "UAT.MONGO"]
    values = await environment.get("PROD,UAT.MONGO")
    logging.info("---- values in \"PROD\" and \"UAT.MONGO\" (see how the value for MONGO_URL is different ----")
    for name in values:
        logging.info(f"{name}: \"{values[name]}\"")

    logging.info("done testing MarspyEnvironment")


asyncio.run(main())
