import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyUtilities
from marspy import MarspyData

#logging.setLevel(logging.INFO)

data = {
    "data": {
        "id": 11100093,
        "feed": "RTBFSPORT",
        "creationDate": "2022-11-07 12:24:02",
        "lastUpdateDate": "2022-11-07 13:24:02",
        "lastUpdateDateUnique": "0000000001667849427.11100093000000000000",
        "deletionDate": "2045-12-31 00:00:00",
        "displayDate": "2022-11-07 12:23:08",
        "fromDate": "2022-11-07 12:23:08",
        "toDate": "2045-12-31 00:00:00",
        "validated": 1,
        "published": 1,
        "highlighted": 1,
        "updated": 0,
        "deleted": 0,
        "commentStatus": "NO",
        "commentCount": 0,
        "commentEndTime": 365,
        "top": 420,
        "source": "qvo@rtbf.be",
        "sourceId": "temporary-RTBFSPORT-082533a41bb36e451ee8509ec7879a94",
        "title": "La s\u00e9ance de rattrapage\u00a0: Onuachu voit quadruple, des gestes Pro league plus nombreux que jamais et des supporters pas contents",
        "title_url": "",
        "synopsis": "Comme chaque semaine, la s\u00e9ance de rattrapage est l\u00e0 pour les distraits dans La\u00a0Tribune. Si vous aviez un mariage, un...",
        "synopsisAuto": 1,
        "summaryTitle": "",
        "summary": "summary",
        "mainParagraph": "",
        "signature": "Quentin Volvert",
        "templateId": 98,
        "localisation": "string",
        "mainImage": "b47767f992ce8624345aca182b76b202-1667832278.png",
        "stamp": "",
        "amountImage": 0,
        "version": 1,
        "versionId": 123,
        "categoryId": 2002,
        "category": "FOOTBALL",
        "amountAudio": 0,
        "amountVideo": 1,
        "primaryKeyword": "DIVERS",
        "dossierId": 1863,
        "hidefromfeed": 0,
        "new": 1,
        "mainImageAuto": 0,
        "nopub": 0,
        "charCounter": 202,
        "readingTime": 212,
        "html": "<p>Comme chaque semaine, la séance de rattrapage est l\u00e0 pour les distraits dans La\u00a0Tribune. Si vous aviez un mariage, un voyage ou un peu trop la flemme pour regarder tous les matches de la semaine, on vous montre tout\u00a0ce qu\u2019il ne fallait pas louper.</p>\n",
        "text": "Comme chaque semaine, la s\u00e9ance de rattrapage est l\u00e0 pour les distraits dans La\u00a0Tribune. Si vous aviez un mariage, un voyage ou un peu trop la flemme pour regarder tous les matches de la semaine, on vous montre tout\u00a0ce qu\u2019il ne fallait pas louper.\n",
        "entities": [
            {
                "label": "Jelle Vossen",
                "score": 0.2618
            }
        ],
        "topics": [
            {
                "label": "Association football league seasons",
                "score": 0.608
            }
        ],
        "categories": [
            {
                "label": "Association football league seasons",
                "score": 0.608
                    }
        ],
        "images": {
            "format180": "https://ds.static.rtbf.be/article/image/180x100/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png",
            "format370": "https://ds.static.rtbf.be/article/image/370x208/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png",
            "format770": "https://ds.static.rtbf.be/article/image/770x433/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png",
            "format1248": "https://ds.static.rtbf.be/article/image/1248x702/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png"
        },
        "image_url": "https://ds.static.rtbf.be/article/image/1248x702/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png"
    }
}

async def main():
    #MarspyData.create_schema(data)
    #return

    environment = await MarspyEnvironment.get("PROD")

    article = await MarspyData.get("enriched", "article", 11164023)
    MarspyUtilities.prettyprint(article)
    return
    
    async def receive_articles(article):
        enriched_article = await MarspyData.get("enriched", "article", article["id"])
        MarspyData.create_schema(enriched_article)

    await MarspyData.get("articles", "cryo", "", receive_articles)
    return

    article = await MarspyData.get("article", "cryo", 11162814)
    print("article:")
    MarspyUtilities.prettyprint(article)
    return

    media = await MarspyData.get("media", "mongo", 3004330)
    MarspyUtilities.prettyprint(media)

    media = await MarspyData.get("media", "cryo", 3004330)
    MarspyUtilities.prettyprint(media)

    user = await MarspyData.get("user", "gigya", environment["GIGYA_TEST_UID"])
    MarspyUtilities.prettyprint(user)

    user = await MarspyData.get("user", "redshift", environment["GIGYA_TEST_UID"])
    MarspyUtilities.prettyprint(user)

    categories = await MarspyData.call("article/categorize", 11160100, 5)
    MarspyUtilities.prettyprint(categories)

    


asyncio.run(main())


