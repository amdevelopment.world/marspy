import asyncio
from marspy import MarspyJSONSchema

logging.setLevelFirst(logging.INFO)


async def example():
    schema = MarspyJSONSchema({
    })

    def traverser(name, settings, path):
        print(name, settings, path)

    schema.traverse(traverser)

asyncio.run(example())
