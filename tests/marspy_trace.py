import asyncio
from marspy import MarspyTrace
from marspy import MarspyEnvironment

async def example():
    # MarspyEnvironment.get is required before any tracing, to obtain the kafka_url
    await MarspyEnvironment.get("PROD")
    
    MarspyTrace.trace("marspy_trace_test", "a simple string")
    MarspyTrace.trace("marspy_trace_test", {
        "key": "value"
    })

    # optionaly call trace when done, to be sure all messages have been sent
    print("stopping trace")
    await MarspyTrace.stop()
    
    print("done testing MarspyTrace")
    

asyncio.run(example())
