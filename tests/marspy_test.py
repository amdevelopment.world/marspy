the following function decodes the file using the method you described
first it opens the file, reads it line by line, splits the line into the number and the words and creates a dictionary where the key is the number, and the value the word

then it calculates the number that would be the last in a stack of rows, where each row is one element longer than the previous one, like the stairs in a pyramid
the function prints the word corresponding to that number

def decode_file(file_path):    
    with open(file_path, 'r') as file:
        words = {}
        for line in file:
            number_and_word = line.strip().split()
            number = int(number_and_word[0])
            word = number_and_word[1]
            words[number] = word

        number = 1
        length = 1

        while number <= len(words):
            print(words[number])

            length = length + 1
            number = number + length

decode_file("coding_qual_input.txt")