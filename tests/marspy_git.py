import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyUtilities
from marspy import MarspyGit

logging.setLevelFirst(logging.INFO)

async def main1():
    await MarspyEnvironment.get("PROD")
    # source = await MarspyGit.get(repository="bigdata-media/marspy_tool", path="functions/article/cryo/get.py")
    # print(f"\n---------------------- MarspyGit.get ----------------------\n{source}")
    #
    # result = await MarspyGit.compile("bigdata-media/marspy_tool", "functions/article/cryo/get.py")
    # print(f"\n---------------------- MarspyGit.compile ------------------\n{MarspyUtilities.stringify(result)}")
    #
    # result = await MarspyGit.call("bigdata-media/marspy_tool", "functions/article/cryo/get.py", "function", 123454321)
    # print(f"\n---------------------- MarspyGit.call ---------------------\n{result}")

    result = await MarspyGit.call("bigdata-media/marspy_tool", "functions/media/mongo/get.py", "function", 2039908)
    print(f"\n---------------------- MarspyGit.call mongo ---------------\n{result}")

    logging.info("done testing MarspyGit")

async def main():
    await MarspyEnvironment.get("PROD")

    result = await MarspyGit.call("bigdata-media/marspy_tool", "functions/media/mongo/get.py", "function", 2039908)
    MarspyUtilities.prettyprint(result)


asyncio.run(main())
