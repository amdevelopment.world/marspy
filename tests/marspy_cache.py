import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyCache
from marspy import MarspyLogging as logging


logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("PROD")
    
    async def test_cache(type):
        cache = MarspyCache({
            "type": type,
            "name": "Simpsons",
            "ttl": 60
        })

        # get the value for the key, should be None
        print(await cache.get("bart"), "should be None")
        
        # set the value for the key
        await cache.set("bart", {
            "first": "Bart",
            "last": "Simpson",
            "age": 10
        })


        await cache.set("lisa", {
            "first": "Lisa",
            "last": "Simpson",
            "age": 8
        })

        # does the cache have this key ?
        print(await cache.has("bart"), "should be True")
        
        print(await cache.get_and_set("bart", {
            "first": "Bart",
            "last": "Simpson",
            "age": 11
        }), "should be 10 years old")

        # get the value for the key
        print(await cache.get("bart"), "should be bart at 11 years old")
        await cache.expire_in("bart", 5)
        await asyncio.sleep(7)
        print(await cache.get("bart"), "should be None, when expire_at is implemented")

        #delete the value for the key
        await cache.delete("bart")
        print(await cache.get("bart"), "should be None")

        await cache.stop()

    await test_cache("memory")
    #await test_cache("redis")

    print("done testing MarspyCache")

asyncio.run(main())
