import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyHTTP

logging.setLevelFirst(logging.INFO)


async def main():
    # first example, specify the url when calling the static method of MarspyHTTP, this is now the prefered way to use MarspyHTTP
    result = await MarspyHTTP.post("https://httpbin.org/anything", {
        "hello": "from static http post",
        "bool": True
    })

    logging.info(result)

    # second example, specify the url once when instanciating MarspyHTTP, this is deprecated, not practical, except in some cases
    http = MarspyHTTP({
        "url": "https://httpbin.org/anything"
    })

    result = await http.put({
        "hello": "from http put"
    })

    logging.info(result)
    logging.info("done testing MarspyHTTP")

asyncio.run(main())
