import asyncio
import random
from marspy import MarspyEnvironment, MarspyMetrics


async def example():
    environment = await MarspyEnvironment.get("PROD")

    configuration = {
        "name": "name-of-service",
        "url": environment["MONGO_URL"]
    }

    metrics = MarspyMetrics(configuration)

    # measure temperature
    metrics.measure("temperature", random.randint(4, 30))

    # count cars
    metrics.count("car")
    metrics.count("car")

    # count occurences of specific cars
    metrics.count("car", "111111")
    metrics.count("car", "222222")
    metrics.count("car", "333333")
    metrics.count("car", "111111")

    # or, count recommended articles

    metrics.count("123456", "option4")
    metrics.count("654321", "option1")

    print("done testing MarspyDashboard")

asyncio.run(example())
