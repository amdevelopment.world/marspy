import asyncio
import json
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyConfiguration

logging.setLevelFirst(logging.INFO)

# ----------------------------------------first example -----------------------------------------
# simply get the configuration from yaml files
# the /conf/DEV.yaml file will be merged with the /conf/all.yaml file
#
# "all/yaml" contains values that are valid in all environments
# for this demo it contains:
# app: 
#   name: "marspy library"
#
# "dev.yaml" contains:
# app: 
#   env: "DEV"
# -----------------------------------------------------------------------------------------------

async def yaml_configuration():
    # read the configuration from local .yaml files
    conf = MarspyConfiguration.read_yaml("DEV")
    logging.info(f"DEV configuration: {json.dumps(conf, indent=4)}")
    
    # same, for PROD
    conf = MarspyConfiguration.read_yaml("PROD")
    logging.info(f"PROD configuration: {json.dumps(conf, indent=4)}")
    
    # just read the shared values
    conf = MarspyConfiguration.read_yaml()
    logging.info(f"configuration: {json.dumps(conf, indent=4)}")
    

# --------------------------------------- second example ----------------------------------------
# get, and subscribe to a configuration stored in documentDb
# -----------------------------------------------------------------------------------------------

async def simple_configuration():
    await MarspyEnvironment.get("PROD")

    # not specifying the name of the configuration, by default the name of the root directory is used
    configuration = MarspyConfiguration()

    # the function that is called when the configuration is changed
    def configuration_changed(modified_configuration):
        logging.info(f"modified configuration: {modified_configuration}")

    # subscribe to the configuration, providing initial values
    current_configuration = await configuration.subscribe({
        "number": 123,
        "message": "the quick brown fox jumps over the lazy dog",
        "OK": True
    }, configuration_changed)

    logging.info(f"current_configuration: {current_configuration}")

    # sleep for two minutes while waiting for changes
    await asyncio.sleep(120)

    # clean up
    await configuration.delete()
    await configuration.stop()
    logging.info("done testing simple_configuration")

# --------------------------------------- third example -----------------------------------------
# get, and subscribe to a another configuration stored in documentDb described with a JSON schema
# -----------------------------------------------------------------------------------------------

async def configuration_with_schema():
    await MarspyEnvironment.get("PROD")

    configuration = MarspyConfiguration({
        "name": "configuration with schema"
    })

    # optional, declare a JSON schema
    await configuration.set_schema({
        "required": [],
        "properties": {
            "number": {
                "type": "number",
                "minimum": 0,
                "default": 0
            },
            "name": {
                "type": "string",
                "enum": ["Marge", "Homer", "Lisa", "Bart", "Maggie"],
                "default": "Bart"
            },
            "boolean": {
                "type": "boolean",
                "default": True,
                "title": "A Boolean with a Title"
            },
            "list": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            }
        }
    })

    def configuration_changed(modified_configuration):
        logging.info(f"modified configuration: {modified_configuration}")

    # subscribe, no need to provide default values, as they can be deduced
    # from the schema, if values are provided they will be merged with the schema
    current_configuration = await configuration.subscribe(configuration_changed)
    logging.info(f"current_configuration: {current_configuration}")

    # sleep for two minutes while waiting for changes
    await asyncio.sleep(120)

    # clean up
    await configuration.delete()
    await configuration.stop()
    logging.info("done testing MarspyConfiguration")


# -----------------------------------------------------------------------------------------------
# demo :-)
# -----------------------------------------------------------------------------------------------

async def demo():
    #await yaml_configuration()
    #await simple_configuration()
    await configuration_with_schema()

asyncio.run(demo())
