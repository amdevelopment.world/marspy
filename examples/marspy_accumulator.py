import asyncio
from marspy import MarspyAccumulator


async def main():
    accumulator = MarspyAccumulator()
    list_accumulator = MarspyAccumulator()

    accumulator_total = 0
    list_accumulator_total = 0

    async def list_accumulator_callback(items):
        nonlocal list_accumulator_total
        list_accumulator_total += len(items)

    async def accumulator_callback(items):
        nonlocal accumulator_total
        accumulator_total += len(items)
        await list_accumulator.accumulate(4, items, list_accumulator_callback)

    for i in range(0, 102):
        await accumulator.accumulate(10, f"item{i}", accumulator_callback)

    await accumulator.finish()
    await list_accumulator.finish()

    if accumulator_total != list_accumulator_total:
        raise Exception("test of MarspyAccumulator failed")

asyncio.run(main())
