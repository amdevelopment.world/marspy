import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyUtilities
from marspy import MarspyNeo4j

logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("DEV")

    neo4j = MarspyNeo4j()


    # -----------------------------------------------------------------------------------------------
    # use "query" to receive dictionaries, and native objects via a callback
    # use this if you get a large number of lines
    # -----------------------------------------------------------------------------------------------
    print("\n----------------------------------------------------------------------------------------------")
    
    print("first example, get article with \"query\" and a callback")
    def receive_from_neo4j(dictionary, record):
        print(f"callback received dict: {MarspyUtilities.stringify(dictionary)}")
        print(f"callback received record: {MarspyUtilities.stringify(record)}")

    await neo4j.query("MATCH (a:Article) return a LIMIT 1", receive_from_neo4j)

    # -----------------------------------------------------------------------------------------------
    # don't use a callback, the result is a list of neo4j records
    # use this only if you receive a small number of results
    # -----------------------------------------------------------------------------------------------
    print("\n----------------------------------------------------------------------------------------------")

    articles = await neo4j.query("MATCH (a:Article) return a LIMIT 1")
    print("\nuse \"query\" to get 1 record as a result, without callback", MarspyUtilities.stringify(articles))
    
    # -----------------------------------------------------------------------------------------------
    # a raw query, allows to inspec the result
    #
    # the result is of type AsyncResult:
    # https://neo4j.com/docs/api/python-driver/current/async_api.html#neo4j.AsyncResult.keys
    #
    # you can get the summary of the transaction with the "consume" method
    # https://neo4j.com/docs/api/python-driver/current/api.html#summarycounters
    #
    # use this for more complicated cases where you need access to the raw info,
    # usualy when inserting nodes
    # -----------------------------------------------------------------------------------------------
    print("\n----------------------------------------------------------------------------------------------")
    
    raw_result = await neo4j.raw_query("MATCH (a:Article) return a LIMIT 3")
    print("third example, raw_query", raw_result)

    summary = await raw_result.consume()
    print("summary", MarspyUtilities.stringify(summary.counters.nodes_created))

    # -----------------------------------------------------------------------------------------------
    # using "query" with a session
    # you can use query with or without a callback or, raw_query, just like in the examples above
    #
    # use this when using neo4j in a web service
    # -----------------------------------------------------------------------------------------------

    print("\n----------------------------------------------------------------------------------------------")
    print("fourth example, count articles, using a thread safe session")
    async with neo4j.session() as session:
        result = await neo4j.query("MATCH (a:Article) return count(a)", session=session)
        print("result", MarspyUtilities.stringify(result))

    await neo4j.stop()
    logging.info("done testing MarspyNeo4j")


asyncio.run(main())
