import asyncio
from marspy import MarspyTrace
from marspy import MarspyEnvironment

async def example():
    # MarspyEnvironment.get is required before any tracing, to obtain the kafka_url
    await MarspyEnvironment.get("PROD")
    
    MarspyTrace.trace("article_1234567890", "a simple string")
    MarspyTrace.trace("article_1234567890", {
        "key": "value"
    })

    # optionaly call trace when done, to be sure all messages have been sent
    await MarspyTrace.stop()
    
    print("done testing MarspyTrace")
    

asyncio.run(example())
