import asyncio
from marspy import MarspyEnvironment, MarspyKafka


async def main():
    await MarspyEnvironment.get("PROD.KAFKA.AWS")

    kafka = MarspyKafka({
        "topic": "cryo_replication",
        "from": "start",
        "group_id": "marspy_kafka_test"
    })

    def receive_from_kafka(line):
        print(line)

    kafka.get(receive_from_kafka)

    # a main loop is needed here, while events are consumed

    while True:
        await asyncio.sleep(30)

    await kafka.stop()

asyncio.run(main())
