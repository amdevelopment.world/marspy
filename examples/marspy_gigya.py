import asyncio
import os
from marspy import MarspyEnvironment
from marspy import MarspyGigya
from marspy import MarspyAccumulator
from marspy import MarspyStream
from marspy import MarspyRedshift

ENV = os.environ.get("ENV_VARIABLE", "DEV")

async def main():
    await MarspyEnvironment.get(ENV)
   
    gigya = MarspyGigya()
    accumulator = MarspyAccumulator()
    redshift = MarspyRedshift()

    stream = MarspyStream({
        "url": "s3://big-data-media/playground/microservice-template/gigya_example.json"
    })


    def map_user(user):
        # insert code here
        return user

    def stream_to_s3(records):
        stream.put(records)

    async def receive_from_gigya(user):
        print(f"user: {user}")
        # gigya.stop_query()
        await accumulator.accumulate(100, map_user(user), stream_to_s3)

    # get a single user
    await gigya.query("select * from accounts limit 1", receive_from_gigya)

    # get a all users
    await gigya.query("select * from accounts", receive_from_gigya)

    await redshift.create_table("marspy_test")
    #await redshift.load({
    #    "from": "s3://big-data-media/playground/microservice-template/gigya_example.json",
    #    "into": "marspy_gigya"
    #})

    await gigya.stop()
    await accumulator.stop()
    await stream.stop()
    await redshift.stop()


asyncio.run(main())