import asyncio
import os
from marspy import MarspyEnvironment
from marspy import MarspyConfiguration
from marspy import MarspyGigya
from marspy import MarspyAccumulator
from marspy import MarspyStream
from marspy import MarspyRedshift
from marspy import MarspyData
from marspy import MarspyLogging as logging

ENV = os.environ.get("ENV_VARIABLE", "DEV")

async def main():
    await MarspyEnvironment.get("PROD")
    configuration = MarspyConfiguration.read_yaml("DEV")

    map_user = MarspyData.get_mapper_function(type="user", source="gigya", destination="redshift")

    accumulator = MarspyAccumulator()

    stream = MarspyStream({
        "url": "s3://big-data-media/playground/marspy/examples/gigya_s3_redshift.json"
    })


    async def stream_to_s3(records):
        await stream.put(records)

    async def receive_from_gigya(user):
        await accumulator.accumulate(100, map_user(user), stream_to_s3)

    gigya_fields = ','.join(configuration["app"]["gigya"]["fields"])
    await MarspyGigya.query(f"select {gigya_fields} from accounts limit 10", receive_from_gigya)

    await accumulator.stop()

    redshift = MarspyRedshift()
    #await redshift.load({
    #    "from": "s3://big-data-media/playground/microservice-template/gigya_example.json",
    #    "into": "marspy_gigya"
    #})

    await stream.stop()
    await redshift.stop()


asyncio.run(main())