import os
import asyncio
from marspy import MarspyConfiguration
from marspy import MarspyEnvironment
from marspy import MarspyEmail
from marspy import MarspyCryo
from marspy import MarspyRedshift

ENV = os.environ.get("ENV", "DEV")

mapped_schema_names = {'dossier':'article'}

async def main():
    await MarspyEnvironment.get(ENV)
    conf = MarspyConfiguration.read_yaml(ENV)
    tables = conf["app"]["tables"]

    cryo = MarspyCryo()
    redshift = MarspyRedshift()
    email = MarspyEmail({
        "from": "Gestion Donnees <gestion-donnees@rtbf.be>",
        "to": "qbea@rtbf.be",
        "subject": "alerte changement de schéma détecté!"
    })

    def list_to_set(columns):
        s = set()
        for column in columns:
            s.add(column["column_name"].lower())
        return s

    async def compare_table(name):
        schema_table = name.split(".")
        schema = schema_table[0]
        table = schema_table[1]
        redshift_schema = mapped_schema_names.get(table, schema)
        redshift_columns = await redshift.query(f"""
        select column_name 
        from SVV_COLUMNS 
        where table_schema = '{redshift_schema}_new' and table_name = '{table}' 
        order by column_name;""")

        cryo_columns = await cryo.query(f"""
        SELECT column_name  
        FROM information_schema.columns 
        WHERE table_schema = '{schema}' AND table_name = '{table}' order by column_name;""")

        redshift_columns = list_to_set(redshift_columns)
        cryo_columns = list_to_set(cryo_columns)

        new_cols = cryo_columns - redshift_columns
        del_cols = redshift_columns - cryo_columns
        return(list(new_cols),list(del_cols))


    body = ''
    for table in tables:
        new, deleted = await compare_table(table)
        if len(new):
            body += f'{table} has a new column "{new}"'

        if len(deleted):
            body += f'{table} has a deleted column "{deleted}"'

    if body != '':
        await email.send(body)
    else:
        await email.send('tout va bien!')

asyncio.run(main())
