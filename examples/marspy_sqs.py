import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspySQS

logging.setLevelFirst(logging.INFO)

async def main():
    environement = await MarspyEnvironment.get("PROD")
    exists = await MarspySQS.has("marspy_test")

    if exists == False:
        await MarspySQS.create("marspy_test")

    async def receive_message_1(message):
        print(f"#1 received {message}")

    subscriber1 = await MarspySQS.subscribe("marspy_test", receive_message_1)

    async def receive_message_2(message):
        print(f"#2 received {message}")

    subscriber2 = await MarspySQS.subscribe("marspy_test", receive_message_2)

    loop_counter = 0

    while loop_counter < 10:
        loop_counter += 1
        await MarspySQS.put("marspy_test", f"this is message # {loop_counter} in the sqs queue")
        await asyncio.sleep(6)

    # await MarspySQS.delete("marspy_test")
    await MarspySQS.stop()

    print("done testing MarspySQS")

asyncio.run(main())