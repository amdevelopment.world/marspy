from flask import request
import time
import random

def main():
    body = request.get_json()
    sleep = random.randint(1, 8)
    time.sleep(sleep)

    return {
        "message": f"hello, hello hello hello from echo after sleeping for {sleep} seconds",
        "body": body
    }