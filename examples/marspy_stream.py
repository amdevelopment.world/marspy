import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyAccumulator
from marspy import MarspyStream
from marspy import MarspyNeo4j
from marspy import MarspyUtilities

async def main():
    environment = await MarspyEnvironment.get("DEV,PROD.FTP.PARTENAIRES") # or PROD

    neo4j = MarspyNeo4j({
        "database": "test" # optionaly select a specific database
    })

    stream = MarspyStream({
        "url": f"{environment['FTP_URL']}/playground/marspy/articles_from_neo4j.json"
    })

    accumulator = MarspyAccumulator()

    def stream_to_ftp(records):
        # write a list of dictionaries
        stream.put(records)

    async def receive_from_neo4j(dictionary, record):
        await accumulator.accumulate(100, dictionary, stream_to_ftp)

    await neo4j.query("MATCH (a:Article) return a LIMIT 1000", receive_from_neo4j)

    await neo4j.stop()
    await accumulator.stop()
    await stream.stop()

    # copy from http to local "logs" directory
    logs_url = f"file://{MarspyUtilities.root_directory()}/logs"

    MarspyStream.copy("https://c9851ec-az-westeurope.fsly.cdn.ebsd.ericsson.net/rtbf/auvio/assets/6347fb4e20c65_6BA97Bb/materials/4BWmyjgrSk_6BA97Bb/uploads/podcast-2815133.mp3",
        f"{logs_url}/podcast-2815133.mp3")

    # copy from ftp to s3
    from_ftp_url = f"{environment['FTP_URL']}/xml_restart/rss_feed_ZONE_BXL.xml"
    s3_url =  "s3://big-data-media/playground/marspy_tests/rss_feed_ZONE_BXL.xml"

    MarspyStream.copy(from_ftp_url, s3_url)

    # copy from s3 to ftp
    to_ftp_url = f"{environment['FTP_URL']}/playground/marspy/marspy_test.xml"
    MarspyStream.copy(s3_url, to_ftp_url)

    # ------------------ style #1, do not do this with a file on an old ftp server, as the local copy will not get closed and deleted
    for line in MarspyStream.open(to_ftp_url, encoding='utf-8'):
        print(line)

    # reading files, in different styles
    # ------------------ style #2, open, and close the file explicitaly
    file = MarspyStream.open(to_ftp_url, encoding='utf-8')

    for line in file:
        print(line)

    file.close()

    # ------------------ style #3, use the more elegant "with" statement
    with MarspyStream.open(to_ftp_url, encoding='utf-8') as file:
        for line in file:
            print(line)


asyncio.run(main())

