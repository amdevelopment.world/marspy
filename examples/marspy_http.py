import asyncio
from marspy import MarspyHTTP


async def main():
    # first example, specify the url when calling the static method of MarspyHTTP, this is now the prefered way to use MarspyHTTP
    result = await MarspyHTTP.post("https://httpbin.org/anything", {
        "hello": "from static http post"
    })

    print(result)

    # second example, specify the url once when instanciating MarspyHTTP, this is deprecated, not practical, except in some cases
    http = MarspyHTTP({
        "url": "https://httpbin.org/anything"
    })

    result = await http.put({
        "hello": "from http put"
    })

    print(result)

    # third example get (same as post, but parameters are going into the url with ? and &)

    result = await MarspyHTTP.get("http://fission.bda.rtbf.be/reco/article/c2c/live", {
        "article_id": 123,
        "ponderation_jaccard_entity_topic": 70,

        "ponderation_jaccard_entity": 70,
        "ponderation_jaccard_topic": 30,
        "ponderation_jaccard_cosinus": 80,
        "ponderation_jaccard": 20,
        "ponderation_cosinus": 80,
        "min_relevance_topic": 50,
        "min_topic_for_sim":20,
        "min_relevance_entity": 20,
        "min_entity_for_sim": 3,
        "article_id": 10853953,
        "limit": 10
    })

    print(result)
    print("done testing HTTP")

asyncio.run(main())
