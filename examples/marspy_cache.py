import asyncio

from marspy import MarspyEnvironment
from marspy import MarspyCache

async def main():
    await MarspyEnvironment.get("PROD")
    
    async def test_cache(type):
        # you can choose for a global ttl, or one per item
        cache = MarspyCache({
            "type": type,
            "name": "simpsons",
            "ttl": 10
        })

        # get the value for the key, should be None
        print(await cache.get("bart"), "should be None")
        
        # set the value for bart, with expiration
        await cache.set("bart", {
            "first": "Bart",
            "last": "Simpson",
            "age": 10
        })

        await cache.set("lisa", {
            "first": "Lisa",
            "last": "Simpson",
            "age": 8
        })

        await cache.set("maggie", {
            "first": "Maggie",
            "last": "Simpson",
            "age": 1
        })

        # delete the value for the key
        await cache.delete("maggie")
        print(await cache.get("maggie"), "should be None")


        # does the cache have this key ?
        print(await cache.has("bart"), "should be True")
        
        # get the value for the key
        print(await cache.get("bart"), "should be bart")
        
        # set expiration to five seconds
        await cache.expire_in("bart", 5)

        # sleep for 6 seconds
        await asyncio.sleep(6)
        print(await cache.get("bart"), "should be None")
        print(await cache.get("lisa"), "should be lisa")
        await asyncio.sleep(6)
        print(await cache.get("lisa"), "should be None")

    await test_cache("memory")
    #await test_cache("redis")

    print("done testing MarspyCache")

asyncio.run(main())
