import asyncio

from marspy import MarspyEnvironment
from marspy import MarspyCloudwatch

async def main():
    await MarspyEnvironment.get("PROD")
    
    cloudwatch = MarspyCloudwatch({
        "namespace": "marspy_test"
    })

    return
    # don't execute the following code
    await cloudwatch.put([
        {
            'MetricName': 'FPE-percent-video-sent-to-ingestion',
            'Unit': 'Percent',
            'Value': 123
        },
        {
            'MetricName': 'FPE-video-not-sent-to-ingestion',
            'Unit': 'Count',
            'Value': 10
        },
        {
            'MetricName': 'FPE-video-sent-to-ingestion',
            'Unit': 'Count',
            'Value': 20
        },
        {
            'MetricName': 'FPE-video-block-by-redis',
            'Unit': 'Count',
            'Value': 2
        },
    ])

    print("done testing MarspyCache")

asyncio.run(main())

# pour les namespaces, voir:
# https://eu-west-1.console.aws.amazon.com/cloudwatch/home?region=eu-west-1#metricsV2:graph=~(view~'timeSeries~stacked~false~region~'eu-west-1~stat~'Average~period~300)
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudwatch.html#CloudWatch.Client.put_metric_data