import asyncio
from marspy import MarspyEnvironment, MarspyAccumulator, MarspySQL, MarspyNeo4j

async def main():
    environment = await MarspyEnvironment.get("PROD")

    sql = MarspySQL({
        "url": environment["CRYO_URL"]
    })

    neo4j = MarspyNeo4j()
    accumulator = MarspyAccumulator()
    results = MarspyAccumulator()

    async def print_missing_articles(ids):
        print(f"{len(ids)} new ids: {ids}")

    async def find_missing_articles(ids):
        result = await neo4j.query(f"""MATCH (a:Article) WHERE a.id IN {ids} RETURN a.id as id""")

        for node in result:
            ids.remove(node["id"])

        if len(ids):
            await results.accumulate(1000, ids, print_missing_articles)

        return ids

    manualIds = [11148809]

    if len(manualIds):
        missing = await find_missing_articles(manualIds)
        if len(missing):
            print(f"{manualIds} is missing")
        else:
            print(f"{manualIds} is not missing")

    async def sql_callback(row):
        await accumulator.accumulate(1000, row["id"], find_missing_articles)

    days_from = 2
    days_to = 0

    query = f"""
        SELECT a.id as id
        FROM article.article as a 
        WHERE ((a."creationDate" >= current_date - {days_from} AND a."creationDate" <= current_date- {days_to-1})
            OR (a."lastUpdateDate" >= current_date - {days_from}  AND a."lastUpdateDate" <= current_date - {days_to-1}))
            AND a.published = 1
            and a."displayDate" <= current_date+1
            and a."toDate" >= current_date
            AND a.deleted = 0;"""

    await sql.query(query, sql_callback)

    await accumulator.stop()
    await results.stop()
    await sql.stop()

asyncio.run(main())
