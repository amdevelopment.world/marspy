import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyS3
from marspy import MarspyUtilities

logging.setLevelFirst(logging.INFO)

URL = "s3://big-data-media/playground/sqs/part-34a4fbc0-3640-4301-b80d-0e6510501f70-0"
PARQUET = "s3://big-data-media/in/trackers/parquet/2022/01/01/track/part-00000-fd57a30c-ad16-419e-be88-0996173512f8-c000.snappy.parquet"

# ----------------------------------------first example -----------------------------------------
# sample data from an old tracker file in the parquet format
# -----------------------------------------------------------------------------------------------

async def sample():
    await MarspyEnvironment.get("PROD")
    await MarspyS3.sample(PARQUET, callback=MarspyUtilities.prettyprint, lines=100)
    await MarspyS3.stop()

    print("done testing MarspyS3")

# ----------------------------------------second example -----------------------------------------
# query the data
# -----------------------------------------------------------------------------------------------

async def query():
    await MarspyEnvironment.get("PROD")

    await MarspyS3.query(
        url=URL,
        query="""SELECT o.UserId,o.AssetId FROM S3Object o WHERE o.EventType = 'Playback.Started'""",
        callback=MarspyUtilities.prettyprint)

    await MarspyS3.stop()

    print("done testing MarspyS3")

# -----------------------------------------------------------------------------------------------

if 1:
    asyncio.run(query())
else:
    asyncio.run(sample())

