import os
import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyAccumulator
from marspy import MarspyStream
from marspy import MarspySQL

ENV = os.environ.get("ENV_VARIABLE", "DEV")

async def main():
    # load environment
    environment = await MarspyEnvironment.get("DEV")
    
    # connect to redshift using "url": environment["REDSHIFT_URL"]
    #
    # other valid values are
    # "url": environment["CRYO_URL"]
    # "url": environment["POSTGRESQL_URL"]
    # note that if you use POSTGRESQL_URL you have to select the database
    # using "database": "the_database"

    sql = MarspySQL({
        "url": environment["REDSHIFT_URL"]
    }) 

    # create a stream to s3, you an also stream to ftp, the local file system by using the appropriate url
    stream = MarspyStream({
        "url": "s3://big-data-media/playground/microservice-template/redshift_example.json"
    })

    # create an accumulator
    accumulator = MarspyAccumulator()

    # the function that writes to s3
    def stream_to_s3(records):
        stream.put(records)

    # the function that receives data from redshift
    async def receive_from_redshift(record):
        await accumulator.accumulate(100, record, stream_to_s3)

    # the query :)
    await sql.query("SELECT uid, gender FROM marspy_gigya LIMIT 1001", receive_from_redshift)

    # clean up, don't skip this part, as the accumulator might still contain data
    await accumulator.stop()
    await stream.stop()
    await sql.stop()

asyncio.run(main())
