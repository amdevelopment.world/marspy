import asyncio
import random
from marspy import MarspyQueue


queue = MarspyQueue(count=5)

# --------------------------------- test #1 ------------------------------------

async def test_queue_with_async_callback():
    # task
    async def task(index, duration):
        print(f"-> task #{index} starting {duration} seconds of work")
        await asyncio.sleep(duration)

        if index == 4:
            raise Exception("raised exception in task #4")

        return f"<- async callback #{index} called after {duration} seconds"

    # callback
    async def callback(error, result, index, duration):
        if error is None:
            print(f"{result}, {queue.get_length()} tasks remaining")
        else:
            print(f"<- async callback sees {error} of {type(error)}")

    print("----- test #1 queueing with async callback -----")

    for i in range(1, 11):
        await queue.call(task, callback, i, random.randint(1, 8))

    print("emptying queue")

    await queue.empty()

# --------------------------------- test #2 ------------------------------------

async def test_queue_with_sync_callback():
    # task
    async def task(index, duration):
        print(f"-> task #{index} starting {duration} seconds of work")
        await asyncio.sleep(duration)
        return f"<- callback #{index} called after {duration} seconds"

    # callback
    def callback(error, result, index, duration):
        if error is None:
            print(f"{result}, {queue.get_length()} tasks remaining")
        else:
            print(f"<- sync callback sees {error} of {type(error)}")

    print("----- test #2 queueing with sync callback -----")

    queue.set_space(3)

    for i in range(1, 6):
        await queue.call(task, callback, i, random.randint(1, 8))

    print("emptying queue")

    await queue.empty()

# --------------------------------- test #3 ------------------------------------

async def test_simple_queue_without_callback():
    # task
    async def task(index, duration):
        print(f"-> task #{index} starting {duration} seconds of work")
        await asyncio.sleep(duration)

        return f"<- callback #{index} called after {duration} seconds"

    print("---- test #3 queueing without callback ----")

    promises = []
    queue.set_space(2)

    for i in range(1, 11):
        promise = queue.call(task, i, 3)
        promises.append(promise)

    print("gathering results")
    result = await asyncio.gather(*promises)


# ---------------------------------------------------------------------

async def tests():
    if True:
        await test_queue_with_async_callback()

    if True:
        await test_queue_with_sync_callback()

    if True:
        await test_simple_queue_without_callback()

    print("end testing queue")

asyncio.run(tests())
