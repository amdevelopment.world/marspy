import asyncio
from marspy import MarspyEnvironment

async def example():
    print("--------- simple example ---------")
    environment = await MarspyEnvironment.get("PROD")
    print(environment["MONGO_URL"])
    print(environment["LIMECRAFT_URL"])

asyncio.run(example())