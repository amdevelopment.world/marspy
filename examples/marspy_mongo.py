import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyMongo


async def main():
    await MarspyEnvironment.get("PROD.MONGO")

    # if url is not specified MarspyMongo will get it from environment
    mongo = MarspyMongo({
        "database": "test"
    })

    await mongo.create_document("people", {
        "first": "Bart",
        "last": "Simpson",
        "age": 10,
    })

    await mongo.stop()
    print("done testing mongo")

asyncio.run(main())
