import asyncio
import random
from marspy import MarspyUtilities


async def main():
    #---------------------------- merge dicts and lists -------------------------------------
    simpsons = [
        {
            "first": "Bart",
            "age": 10,
            "likes": [
                {
                    "name": "skateboard"
                }
            ]
        }, {
            "first": "Homer",
            "last": "Simpson",
            "likes": [
                {
                    "name": "beer"
                }
            ]
        }
    ]

    simpson = MarspyUtilities.merge(simpsons)
    MarspyUtilities.prettyprint(simpson)
    return
    #---------------------------- collect the desired number of values from multiple functions
    async def function_one(limit):
        count = random.randint(1, limit-3)
        list = []

        while count:
            count-=1
            value = random.randint(0, 100)
            if value < 25:
                raise Exception("raise an exception just for fun")
            list.append(f"random: {value}")

        return list

    async def function_two(limit, first_number, second_number):
        return [first_number, second_number]

    def function_three(limit):
        count = limit
        i = 1
        list = []
        while i <= count:
            i+= 1
            list.append(f"fallback #{i}")
        return list

    def callback(name, lines, error):
        if error is not None:
            print(f"{name} returned error {error}")
        else:
            print(f"{name} returned {len(lines)} lines")

    result = await MarspyUtilities.collect(10, [
        function_one,
        function_two,
        function_three], callback, 1234567, 7654321)

    print(result)


asyncio.run(main())
