import asyncio
from marspy import MarspyEnvironment, MarspyFissionFunction

async def main():
    environment = await MarspyEnvironment.get("PROD")

    fission_configuration = {
        "router": environment["FISSION_ROUTER"],
        "path": "echo",
        "concurrency": 5
    }

    function = MarspyFissionFunction(fission_configuration)

    def callback(error, result, payload):
        if error:
            print(f"the function returned error {error}")
        else:
            print(f"result type:{type(result)}result:{result}\npayload:{payload}")

    for i in range(1, 11):
        await function.post({
            "first": "Bart",
            "last": "Simpson",
            "index": i
            }, callback)
        print(f"function call #{i}")

    await function.finish()

    print("done testing fission")

asyncio.run(main())

