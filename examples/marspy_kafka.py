from datetime import datetime
import asyncio
from marspy import MarspyEnvironment, MarspyKafka


async def main():
    await MarspyEnvironment.get("PROD.KAFKA.AWS")

    kafka = MarspyKafka({
        "topic": "marspy-kafka-test"
    })

    for i in range(0, 10):
        await kafka.put({
            "hello": "world",
            "index": i,
            "date": datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)")
        })

    await kafka.put("coucou")
    
    await kafka.put([
        "Homer",
        "Marge",
        "Bart",
        "Lisa",
        "Maggie"
    ])

    await kafka.stop()

    print("done testing kafka")

asyncio.run(main())
