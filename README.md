# marspy

## MarspyEnvironment
manages environment variables, credentials, secrets
## MarspyConfiguration
manages configurations stored in a database, notifies subscribers about changes
## MarspyQueue
queue function calls
## MarspyTask
execute multiple tasks simulatneouslu
## MarspyAccumulator
accumulate x lines before triggering a function call
## MarspyCache
caches data in various backends, memory, redis, in a uniform way
## MarspyLogging
logs, localy and remotely
## MarspyTrace
a tool to help debugging, logs a trace remotely
## MarspySharedResources
manage shared resources, like connections
## MarspyUtilities
various utility functions
## MarspyJSONSchema
load, store, traverse schemas, and validate data 
## MarspyData
inspect, recognize, get, put and map data, generate mappers from json schema descriptions
## MarspyMetrics
store values, counters, timing data in AWS Cloudwatch (for now) allowing the monitoring of a microservice
## MarspyHTTP
get, put, post, delete from a HTTP endpoint, asyncronously
## MarspyStream
reads, writes and streams to and frm ftp, s3. http and many other protocols in a uniform way
## MarspyKafka
read and write to kafka
## MarspySQS
read and write to SQS
## MarspyMongo
read and write to/from a MongoDB collection
## MarspyNeo4j
read and write to/from a Neo4j database
## MarspySQL
read and write to/from a SQL database
## MarspyRedshift
read and write to/from a redshift database, load data from S3
## MarspyS3
query data stored in S3
## MarspyCloudwatch
set values, and log to cloudwatch
## MarspyFissionFunction
call kubernetes serverless "Fission" functions
## MarspyEmail
send emails
## MarspyGit
fetch data, and code from git, compile and execute functions
## MarspyGigya
fetch users from SAP's Gigya

## MarspyEnvironment
store key values in documents, and secrets
by default MarspyEnvironment will fetch a dictionary from a mongodb collection
if the value is in the form of ssm://NAME_OF_SECRET it will fetch the, secret, value from ssm
documents containing values can also include other documents
for example, i recommend to have PROD, STAGE and DEV documents, that will load documents like PROD.MONGO, DEV.MONGO etc
this allows to load a complete set of credentials with just one call to MarspyEnvironment.get("PROD")
MarspyEnvironment is extendable and can be adapted to store documents, and secret values in other repositories
many Marspy classes, like those used to access databases, will get their credentials directly from the loaded environment, so there is no need to pass credentials when instanciating them
* MarspyEnvironment.get(names)
loads the environment named "names" and all included documents recursively, then fetches all the secret values
names can be a single name, or a comma separated list
for example you can load all production values, but use the DEV db by calling

```python
from marspy import MarspyEnvironment
dictionary_of_values = MarspyEnvironment.get("PROD,DEV.MONGO")
```

* MarspyEnvironment.list()
lists all available environments

## MarspyConfiguration
get, and subscribe to changes to a configuration document
MarspyConfiguration allows to store, non secret, configuration values in a document, and allows to be notified when changes are made to those documents, without having to restart the microservice

### example
```python
import asyncio
import json
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyConfiguration

logging.setLevelFirst(logging.INFO)

# ----------------------------------------first example -----------------------------------------
# simply get the configuration from yaml files
# the /conf/DEV.yaml file will be merged with the /conf/all.yaml file
#
# "all/yaml" contains values that are valid in all environments
# for this demo it contains:
# app: 
#   name: "marspy library"
#
# "dev.yaml" contains:
# app: 
#   env: "DEV"
# -----------------------------------------------------------------------------------------------

async def yaml_configuration():
    # read the configuration from local .yaml files
    conf = MarspyConfiguration.read_yaml("DEV")
    logging.info(f"DEV configuration: {json.dumps(conf, indent=4)}")
    
    # same, for PROD
    conf = MarspyConfiguration.read_yaml("PROD")
    logging.info(f"PROD configuration: {json.dumps(conf, indent=4)}")
    
    # just read the shared values
    conf = MarspyConfiguration.read_yaml()
    logging.info(f"configuration: {json.dumps(conf, indent=4)}")
    

# --------------------------------------- second example ----------------------------------------
# get, and subscribe to a configuration stored in MongoDB
# -----------------------------------------------------------------------------------------------

async def simple_configuration():
    await MarspyEnvironment.get("PROD")

    # not specifying the name of the configuration, by default the name of the root directory is used
    configuration = MarspyConfiguration()

    # the function that is called when the configuration is changed
    def configuration_changed(modified_configuration):
        logging.info(f"modified configuration: {modified_configuration}")

    # subscribe to the configuration, providing initial values
    current_configuration = await configuration.subscribe({
        "number": 123,
        "message": "the quick brown fox jumps over the lazy dog",
        "OK": True
    }, configuration_changed)

    logging.info(f"current_configuration: {current_configuration}")

    # sleep for two minutes while waiting for changes
    await asyncio.sleep(120)

    # clean up
    await configuration.delete()
    await configuration.stop()
    logging.info("done testing simple_configuration")

# --------------------------------------- third example -----------------------------------------
# get, and subscribe to a another configuration stored in MongoDB described with a JSON schema
# the schema is used to check values, provide defaults, and generate the GUI that is used to edit the configuration
# -----------------------------------------------------------------------------------------------

async def configuration_with_schema():
    await MarspyEnvironment.get("PROD")

    configuration = MarspyConfiguration({
        "name": "configuration with schema"
    })

    # optional, declare a JSON schema
    await configuration.set_schema({
        "required": [],
        "properties": {
            "number": {
                "type": "number",
                "minimum": 0,
                "default": 0
            },
            "name": {
                "type": "string",
                "enum": ["Marge", "Homer", "Lisa", "Bart", "Maggie"],
                "default": "Bart"
            },
            "boolean": {
                "type": "boolean",
                "default": True,
                "title": "A Boolean with a Title"
            },
            "list": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            }
        }
    })

    def configuration_changed(modified_configuration):
        logging.info(f"modified configuration: {modified_configuration}")

    # subscribe, no need to provide default values, as they can be deduced
    # from the schema, if values are provided they will be merged with the schema
    current_configuration = await configuration.subscribe(configuration_changed)
    logging.info(f"current_configuration: {current_configuration}")

    # sleep for two minutes while waiting for changes
    await asyncio.sleep(120)

    # clean up
    await configuration.delete()
    await configuration.stop()
    logging.info("done testing MarspyConfiguration")


# -----------------------------------------------------------------------------------------------
# demo :-)
# -----------------------------------------------------------------------------------------------

async def demo():
    #await yaml_configuration()
    #await simple_configuration()
    await configuration_with_schema()

asyncio.run(demo())

```

## MarspyQueue
queue function calls, optionaly with a limited size
an easy way to manage concurency, for example, if you have an endpoint that can handle thousands of calls simultaneously
but, you have a process that can only run 5 times concurently, you can use MarspyQueue as a bottleneck, every call made to your endpoint will politely wait for it's turn to be processed, not wasting any CPU as it's all done asyncronously, in a non blocking way
MarspyQueue is used by many other classes in Marspy to help create optimal performance in a simple way

* queue = MarspyQueue(count=5)
creates a queue

* queue.call(task, callback, i, random.randint(1, 8))
queues a call to "task", passing it any number of parameters, and calls "callback" when the task is completed

* await queue.empty()
finishing processing all the queued function calls

* queue.set_space(space)
change the number of spaces in a queue. this works well with MarspyConfiguration, to try different values to reach the optimal queue size, without restarting the microservice

### examples
```python
import asyncio
import random
from marspy import MarspyQueue


queue = MarspyQueue(count=5)

# --------------------------------- test #1 ------------------------------------

async def test_queue_with_async_callback():
    # task
    async def task(index, duration):
        print(f"-> task #{index} starting {duration} seconds of work")
        await asyncio.sleep(duration)

        if index == 4:
            raise Exception("raised exception in task #4")

        return f"<- async callback #{index} called after {duration} seconds"

    # callback
    async def callback(error, result, index, duration):
        if error is None:
            print(f"{result}, {queue.get_length()} tasks remaining")
        else:
            print(f"<- async callback sees {error} of {type(error)}")

    print("----- test #1 queueing with async callback -----")

    for i in range(1, 11):
        await queue.call(task, callback, i, random.randint(1, 8))

    print("emptying queue")

    await queue.empty()

# --------------------------------- test #2 ------------------------------------

async def test_queue_with_sync_callback():
    # task
    async def task(index, duration):
        print(f"-> task #{index} starting {duration} seconds of work")
        await asyncio.sleep(duration)
        return f"<- callback #{index} called after {duration} seconds"

    # callback
    def callback(error, result, index, duration):
        if error is None:
            print(f"{result}, {queue.get_length()} tasks remaining")
        else:
            print(f"<- sync callback sees {error} of {type(error)}")

    print("----- test #2 queueing with sync callback -----")

    queue.set_space(3)

    for i in range(1, 6):
        await queue.call(task, callback, i, random.randint(1, 8))

    print("emptying queue")

    await queue.empty()

# --------------------------------- test #3 ------------------------------------

async def test_simple_queue_without_callback():
    # task
    async def task(index, duration):
        print(f"-> task #{index} starting {duration} seconds of work")
        await asyncio.sleep(duration)

        return f"<- callback #{index} called after {duration} seconds"

    print("---- test #3 queueing without callback ----")

    promises = []
    queue.set_space(2)

    for i in range(1, 11):
        promise = queue.call(task, i, 3)
        promises.append(promise)

    print("gathering results")
    result = await asyncio.gather(*promises)


# ---------------------------------------------------------------------

async def tests():
    if True:
        await test_queue_with_async_callback()

    if True:
        await test_queue_with_sync_callback()

    if True:
        await test_simple_queue_without_callback()

    print("end testing queue")

asyncio.run(tests())

````
## MarspyTask

```python
import asyncio
from marspy import MarspyTask

def do_something():
   for i in range(1, 11):
        print(i)

def do_something_at_the_same_time():
   for i in range(1, 11):
        print(i)

def main():
  MarspyTask(do_something())
  MarspyTask(do_something_at_the_same_time())
  while True:
    await asyncio.sleep(30)

asyncio.run(main())

```
## MarspyAccumulator
accumulate x lines before triggering an action, allows to regroup calls made to databases for example
MarspyAccumulator allows to both optimize those calls, without using too much memory, it acts like a gauge
MarspyAccumulator is frequently used with other classes like MarspyHTTP, MarspyKafka, MarspySQL, MarspyMongo
for example you can fetch lines from kafka, accumulate them until you have 10, then write all of them in a mongo collection

```python
import asyncio
from marspy import MarspyEnvironment, MarspyKafka, MarspyAccumulator, MarspyMongo


async def main():
    await MarspyEnvironment.get("PROD")
    accumulator = MarspyAccumulator()
    mongo = MarspyMongo()

    kafka = MarspyKafka({
        "topic": "my_kafka_topic",
        "group_id": "marspy_kafka_test"
    })

    async def accumulator_callback(lines):
        # we received 10 lines from kafka
        await mongo.create_document("collection_name", lines)

    def receive_from_kafka(line):
        await accumulator.accumulate(10, items, accumulator_callback)

    kafka.get(receive_from_kafka)

    # a main loop is needed here, while events are consumed

    while True:
        await asyncio.sleep(30)

    await kafka.stop()

asyncio.run(main())

```
## MarspyCache
```python
import asyncio

from marspy import MarspyEnvironment
from marspy import MarspyCache

async def main():
    await MarspyEnvironment.get("PROD")
    
    async def test_cache(type):
        # you can choose for a global ttl, or one per item
        cache = MarspyCache({
            "type": type,
            "name": "simpsons",
            "ttl": 10
        })

        # get the value for the key, should be None
        print(await cache.get("bart"), "should be None")
        
        # set the value for bart, with expiration
        await cache.set("bart", {
            "first": "Bart",
            "last": "Simpson",
            "age": 10
        })

        await cache.set("lisa", {
            "first": "Lisa",
            "last": "Simpson",
            "age": 8
        })

        await cache.set("maggie", {
            "first": "Maggie",
            "last": "Simpson",
            "age": 1
        })

        # delete the value for the key
        await cache.delete("maggie")
        print(await cache.get("maggie"), "should be None")


        # does the cache have this key ?
        print(await cache.has("bart"), "should be True")
        
        # get the value for the key
        print(await cache.get("bart"), "should be bart")
        
        # set expiration to five seconds
        await cache.expire_in("bart", 5)

        # sleep for 6 seconds
        await asyncio.sleep(6)
        print(await cache.get("bart"), "should be None")
        print(await cache.get("lisa"), "should be lisa")
        await asyncio.sleep(6)
        print(await cache.get("lisa"), "should be None")

    await test_cache("memory")
    #await test_cache("redis")

    print("done testing MarspyCache")

asyncio.run(main())

```
## MarspyLogging
## MarspyTrace
## MarspySharedResources
## MarspyUtilities

# Input -> Output

## MarspyJSONSchema
## MarspyData
inspect, recognize, get, put and map data, generate mappers from json schema descriptions

* MarspyData.create_schema
create a json schema from data

```python
from marspy import MarspyData

schema = MarspyData.create_schema({
    "data": {
        "id": 11100093,
        "feed": "RTBFSPORT",
        "creationDate": "2022-11-07 12:24:02",
        "lastUpdateDate": "2022-11-07 13:24:02",
        "lastUpdateDateUnique": "0000000001667849427.11100093000000000000",
        "deletionDate": "2045-12-31 00:00:00",
        "displayDate": "2022-11-07 12:23:08",
        "fromDate": "2022-11-07 12:23:08",
        "toDate": "2045-12-31 00:00:00",
        "validated": 1,
        "published": 1,
        "highlighted": 1,
        "updated": 0,
        "deleted": 0,
        "commentStatus": "NO",
        "commentCount": 0,
        "commentEndTime": 365,
        "top": 420,
        "source": "qvo@rtbf.be",
        "sourceId": "temporary-RTBFSPORT-082533a41bb36e451ee8509ec7879a94",
        "title": "La s\u00e9ance de rattrapage\u00a0: Onuachu voit quadruple, des gestes Pro league plus nombreux que jamais et des supporters pas contents",
        "title_url": "",
        "synopsis": "Comme chaque semaine, la s\u00e9ance de rattrapage est l\u00e0 pour les distraits dans La\u00a0Tribune. Si vous aviez un mariage, un...",
        "synopsisAuto": 1,
        "summaryTitle": "",
        "summary": "summary",
        "mainParagraph": "",
        "signature": "Quentin Volvert",
        "templateId": 98,
        "localisation": "string",
        "mainImage": "b47767f992ce8624345aca182b76b202-1667832278.png",
        "stamp": "",
        "amountImage": 0,
        "version": 1,
        "versionId": 123,
        "categoryId": 2002,
        "category": "FOOTBALL",
        "amountAudio": 0,
        "amountVideo": 1,
        "primaryKeyword": "DIVERS",
        "dossierId": 1863,
        "hidefromfeed": 0,
        "new": 1,
        "mainImageAuto": 0,
        "nopub": 0,
        "charCounter": 202,
        "readingTime": 212,
        "html": "<p>Comme chaque semaine, la séance de rattrapage est l\u00e0 pour les distraits dans La\u00a0Tribune. Si vous aviez un mariage, un voyage ou un peu trop la flemme pour regarder tous les matches de la semaine, on vous montre tout\u00a0ce qu\u2019il ne fallait pas louper.</p>\n",
        "text": "Comme chaque semaine, la s\u00e9ance de rattrapage est l\u00e0 pour les distraits dans La\u00a0Tribune. Si vous aviez un mariage, un voyage ou un peu trop la flemme pour regarder tous les matches de la semaine, on vous montre tout\u00a0ce qu\u2019il ne fallait pas louper.\n",
        "entities": [
            {
                "label": "Jelle Vossen",
                "score": 0.2618
            }
        ],
        "topics": [
            {
                "label": "Association football league seasons",
                "score": 0.608
            }
        ],
        "categories": [
            {
                "label": "Association football league seasons",
                "score": 0.608
                    }
        ],
        "images": {
            "format180": "https://ds.static.rtbf.be/article/image/180x100/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png",
            "format370": "https://ds.static.rtbf.be/article/image/370x208/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png",
            "format770": "https://ds.static.rtbf.be/article/image/770x433/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png",
            "format1248": "https://ds.static.rtbf.be/article/image/1248x702/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png"
        },
        "image_url": "https://ds.static.rtbf.be/article/image/1248x702/4/c/3/b47767f992ce8624345aca182b76b202-1667832278.png"
    }
})



```
### example, work in progress, this example, for now, shows how to use getter functions (stored in git) to get articles and media in various formats
```python
import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyUtilities
from marspy import MarspyData

async def main():
    environment = await MarspyEnvironment.get("PROD")

    article = await MarspyData.get("enriched", "article", 11164023)
    MarspyUtilities.prettyprint(article)
    return
    
    async def receive_articles(article):
        enriched_article = await MarspyData.get("enriched", "article", article["id"])
        MarspyData.create_schema(enriched_article)

    await MarspyData.get("articles", "cryo", "", receive_articles)
    return

    article = await MarspyData.get("article", "cryo", 11162814)
    print("article:")
    MarspyUtilities.prettyprint(article)
    return

    media = await MarspyData.get("media", "mongo", 3004330)
    MarspyUtilities.prettyprint(media)

    media = await MarspyData.get("media", "cryo", 3004330)
    MarspyUtilities.prettyprint(media)

    user = await MarspyData.get("user", "gigya", environment["GIGYA_TEST_UID"])
    MarspyUtilities.prettyprint(user)

    user = await MarspyData.get("user", "redshift", environment["GIGYA_TEST_UID"])
    MarspyUtilities.prettyprint(user)

    categories = await MarspyData.call("article/categorize", 11160100, 5)
    MarspyUtilities.prettyprint(categories)

    


asyncio.run(main())


```

## MarspyMetrics
```python
import asyncio
import random
from marspy import MarspyEnvironment, MarspyMetrics


async def example():
    environment = await MarspyEnvironment.get("PROD")

    configuration = {
        "name": "name-of-service",
        "url": environment["MONGO_URL"]
    }

    metrics = MarspyMetrics(configuration)

    # measure temperature
    metrics.measure("temperature", random.randint(4, 30))

    # count cars
    metrics.count("car")
    metrics.count("car")

    # count occurences of specific cars
    metrics.count("car", "111111")
    metrics.count("car", "222222")
    metrics.count("car", "333333")
    metrics.count("car", "111111")

    # or, count recommended articles

    metrics.count("123456", "option4")
    metrics.count("654321", "option1")

    print("done testing MarspyDashboard")

asyncio.run(example())
```
## MarspyHTTP
get, put, post, delete from a HTTP endpoint, asyncronously
```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyHTTP

logging.setLevelFirst(logging.INFO)


async def main():
    # first example, specify the url when calling the static method of MarspyHTTP, this is now the prefered way to use MarspyHTTP
    result = await MarspyHTTP.post("https://httpbin.org/anything", {
        "hello": "from static http post",
        "bool": True
    })

    logging.info(result)

    # second example, specify the url once when instanciating MarspyHTTP, this is deprecated, not practical, except in some cases
    http = MarspyHTTP({
        "url": "https://httpbin.org/anything"
    })

    result = await http.put({
        "hello": "from http put"
    })

    logging.info(result)
    logging.info("done testing MarspyHTTP")

asyncio.run(main())

```
## MarspyStream
reads, writes and streams to and frm ftp, s3. http and many other protocols in a uniform way
```python
import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyAccumulator
from marspy import MarspyStream
from marspy import MarspyNeo4j
from marspy import MarspyUtilities

async def main():
    environment = await MarspyEnvironment.get("DEV,PROD.FTP.PARTENAIRES") # or PROD

    neo4j = MarspyNeo4j({
        "database": "test" # optionaly select a specific database
    })

    stream = MarspyStream({
        "url": f"{environment['FTP_URL']}/playground/marspy/articles_from_neo4j.json"
    })

    accumulator = MarspyAccumulator()

    def stream_to_ftp(records):
        # write a list of dictionaries
        stream.put(records)

    async def receive_from_neo4j(dictionary, record):
        await accumulator.accumulate(100, dictionary, stream_to_ftp)

    await neo4j.query("MATCH (a:Article) return a LIMIT 1000", receive_from_neo4j)

    await neo4j.stop()
    await accumulator.stop()
    await stream.stop()

    # copy from http to local "logs" directory
    logs_url = f"file://{MarspyUtilities.root_directory()}/logs"

    MarspyStream.copy("https://c9851ec-az-westeurope.fsly.cdn.ebsd.ericsson.net/rtbf/auvio/assets/6347fb4e20c65_6BA97Bb/materials/4BWmyjgrSk_6BA97Bb/uploads/podcast-2815133.mp3",
        f"{logs_url}/podcast-2815133.mp3")

    # copy from ftp to s3
    from_ftp_url = f"{environment['FTP_URL']}/xml_restart/rss_feed_ZONE_BXL.xml"
    s3_url =  "s3://big-data-media/playground/marspy_tests/rss_feed_ZONE_BXL.xml"

    MarspyStream.copy(from_ftp_url, s3_url)

    # copy from s3 to ftp
    to_ftp_url = f"{environment['FTP_URL']}/playground/marspy/marspy_test.xml"
    MarspyStream.copy(s3_url, to_ftp_url)

    # ------------------ style #1, do not do this with a file on an old ftp server, as the local copy will not get closed and deleted
    for line in MarspyStream.open(to_ftp_url, encoding='utf-8'):
        print(line)

    # reading files, in different styles
    # ------------------ style #2, open, and close the file explicitaly
    file = MarspyStream.open(to_ftp_url, encoding='utf-8')

    for line in file:
        print(line)

    file.close()

    # ------------------ style #3, use the more elegant "with" statement
    with MarspyStream.open(to_ftp_url, encoding='utf-8') as file:
        for line in file:
            print(line)


asyncio.run(main())


```
## MarspyKafka
```python
from datetime import datetime
import asyncio
from marspy import MarspyEnvironment, MarspyKafka


async def main():
    await MarspyEnvironment.get("PROD.KAFKA.AWS")

    kafka = MarspyKafka({
        "topic": "marspy-kafka-test"
    })

    for i in range(0, 10):
        await kafka.put({
            "hello": "world",
            "index": i,
            "date": datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)")
        })

    await kafka.put("coucou")
    
    await kafka.put([
        "Homer",
        "Marge",
        "Bart",
        "Lisa",
        "Maggie"
    ])

    await kafka.stop()

    print("done testing kafka")

asyncio.run(main())

```
## MarspySQS
```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspySQS

logging.setLevelFirst(logging.INFO)

async def main():
    environement = await MarspyEnvironment.get("PROD")
    exists = await MarspySQS.has("marspy_test")

    if exists == False:
        await MarspySQS.create("marspy_test")

    async def receive_message_1(message):
        print(f"#1 received {message}")

    subscriber1 = await MarspySQS.subscribe("marspy_test", receive_message_1)

    async def receive_message_2(message):
        print(f"#2 received {message}")

    subscriber2 = await MarspySQS.subscribe("marspy_test", receive_message_2)

    loop_counter = 0

    while loop_counter < 10:
        loop_counter += 1
        await MarspySQS.put("marspy_test", f"this is message # {loop_counter} in the sqs queue")
        await asyncio.sleep(6)

    # await MarspySQS.delete("marspy_test")
    await MarspySQS.stop()

    print("done testing MarspySQS")

asyncio.run(main())
```
## MarspyMongo
```python
import asyncio
from marspy import MarspyEnvironment
from marspy import MarspyMongo


async def main():
    await MarspyEnvironment.get("PROD.MONGO")

    # if url is not specified MarspyMongo will get it from environment
    mongo = MarspyMongo({
        "database": "test"
    })

    await mongo.create_document("people", {
        "first": "Bart",
        "last": "Simpson",
        "age": 10,
    })

    await mongo.stop()
    print("done testing mongo")

asyncio.run(main())

```
## MarspyNeo4j
read and write lines to Neo4j

```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyUtilities
from marspy import MarspyNeo4j

logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("DEV")

    neo4j = MarspyNeo4j()

    # -----------------------------------------------------------------------------------------------
    # use "query" to receive dictionaries, and native objects via a callback
    # use this if you get a large number of lines
    # -----------------------------------------------------------------------------------------------
    print("\n----------------------------------------------------------------------------------------------")
    
    print("first example, get article with \"query\" and a callback")
    def receive_from_neo4j(dictionary, record):
        print(f"callback received dict: {MarspyUtilities.stringify(dictionary)}")
        print(f"callback received record: {MarspyUtilities.stringify(record)}")

    await neo4j.query("MATCH (a:Article) return a LIMIT 1", receive_from_neo4j)

    # -----------------------------------------------------------------------------------------------
    # don't use a callback, the result is a list of neo4j records
    # use this only if you receive a small number of results
    # -----------------------------------------------------------------------------------------------
    print("\n----------------------------------------------------------------------------------------------")

    articles = await neo4j.query("MATCH (a:Article) return a LIMIT 1")
    print("\nuse \"query\" to get 1 record as a result, without callback", MarspyUtilities.stringify(articles))
    
    # -----------------------------------------------------------------------------------------------
    # a raw query, allows to inspec the result
    #
    # the result is of type AsyncResult:
    # https://neo4j.com/docs/api/python-driver/current/async_api.html#neo4j.AsyncResult.keys
    #
    # you can get the summary of the transaction with the "consume" method
    # https://neo4j.com/docs/api/python-driver/current/api.html#summarycounters
    #
    # use this for more complicated cases where you need access to the raw info,
    # usualy when inserting nodes
    # -----------------------------------------------------------------------------------------------
    print("\n----------------------------------------------------------------------------------------------")
    
    raw_result = await neo4j.raw_query("MATCH (a:Article) return a LIMIT 3")
    print("third example, raw_query", raw_result)

    summary = await raw_result.consume()
    print("summary", MarspyUtilities.stringify(summary.counters.nodes_created))

    # -----------------------------------------------------------------------------------------------
    # using "query" with a session
    # you can use query with or without a callback or, raw_query, just like in the examples above
    #
    # use this when using neo4j in a web service
    # -----------------------------------------------------------------------------------------------

    print("\n----------------------------------------------------------------------------------------------")
    print("fourth example, count articles, using a thread safe session")
    async with neo4j.session() as session:
        result = await neo4j.query("MATCH (a:Article) return count(a)", session=session)
        print("result", MarspyUtilities.stringify(result))

    await neo4j.stop()
    logging.info("done testing MarspyNeo4j")


asyncio.run(main())

```
## MarspySQL

```python
import asyncio
from marspy import MarspyEnvironment
from marspy import MarspySQL

async def main():
    await MarspyEnvironment.get("PROD")

    sql = MarspySQL({
        "database": "my_database"
    })

    async def callback(row):
        print(row)

    result = await sql.query('SELECT count(*) FROM marspy_table')
    print(result)

    await sql.stop()

asyncio.run(main())

```
## MarspyRedshift
almost the same class as MarspySQL, but will use the "REDSHIFT_URL" environment value when connecting
* load(s3_url)
loads the content of a json, or csv file in s3 into a redshift table

## MarspyPostgreSQL
the same class as MarspySQL, but will use the "POSTGRESQL_URL" environment value when connecting

## MarspyS3
execute SQL queries on documents stored in s3, json, or parquet
* lines = MarspyS3.sample(url, number_of_lines)
fetches number_of_lines random lines, for inspecting data stored in s3
```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyS3
from marspy import MarspyUtilities

logging.setLevelFirst(logging.INFO)

URL = "s3://big-data-media/playground/sqs/part-34a4fbc0-3640-4301-b80d-0e6510501f70-0"
PARQUET = "s3://big-data-media/in/trackers/parquet/2022/01/01/track/part-00000-fd57a30c-ad16-419e-be88-0996173512f8-c000.snappy.parquet"

# ----------------------------------------first example -----------------------------------------
# sample data from an old tracker file in the parquet format
# -----------------------------------------------------------------------------------------------

async def sample():
    await MarspyEnvironment.get("PROD")
    lines = await MarspyS3.sample(PARQUET, callback=MarspyUtilities.prettyprint, lines=100)
    await MarspyS3.stop()

    print("done testing MarspyS3")

asyncio.run(sample())


```
* MarspyS3.query(url, query, call_back)

```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyS3
from marspy import MarspyUtilities

logging.setLevelFirst(logging.INFO)

URL = "s3://big-data-media/playground/sqs/part-34a4fbc0-3640-4301-b80d-0e6510501f70-0"
PARQUET = "s3://big-data-media/in/trackers/parquet/2022/01/01/track/part-00000-fd57a30c-ad16-419e-be88-0996173512f8-c000.snappy.parquet"

# ----------------------------------------second example -----------------------------------------
# query the data
# -----------------------------------------------------------------------------------------------

async def query():
    await MarspyEnvironment.get("PROD")

    await MarspyS3.query(
        url=URL,
        query="""SELECT o.UserId,o.AssetId FROM S3Object o WHERE o.EventType = 'Playback.Started'""",
        callback=MarspyUtilities.prettyprint)

    await MarspyS3.stop()

    print("done testing MarspyS3")

# -----------------------------------------------------------------------------------------------

asyncio.run(query())


```
## MarspyCloudwatch
write values to AWS cloudwatch
```python
import asyncio

from marspy import MarspyEnvironment
from marspy import MarspyCloudwatch

async def main():
    await MarspyEnvironment.get("PROD")
    
    cloudwatch = MarspyCloudwatch({
        "namespace": "marspy_test"
    })

    return
    # don't execute the following code
    await cloudwatch.put([
        {
            'MetricName': 'FPE-percent-video-sent-to-ingestion',
            'Unit': 'Percent',
            'Value': 123
        },
        {
            'MetricName': 'FPE-video-not-sent-to-ingestion',
            'Unit': 'Count',
            'Value': 10
        },
        {
            'MetricName': 'FPE-video-sent-to-ingestion',
            'Unit': 'Count',
            'Value': 20
        },
        {
            'MetricName': 'FPE-video-block-by-redis',
            'Unit': 'Count',
            'Value': 2
        },
    ])

    print("done testing MarspyCache")

asyncio.run(main())

# pour les namespaces, voir:
# https://eu-west-1.console.aws.amazon.com/cloudwatch/home?region=eu-west-1#metricsV2:graph=~(view~'timeSeries~stacked~false~region~'eu-west-1~stat~'Average~period~300)
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudwatch.html#CloudWatch.Client.put_metric_data
```

## MarspyFissionFunction
Fission functions are serverless functions running in kubernetes
* function = MarspyFissionFunction(configuration)
instantiates a representation of the fission function

* function.post(data)
calls the fission function, and returns the result with a "post" call

* function.get(data)
calls the fission function, and returns the result with a "get" call

### example, calls a fission function 10 times, simultaneously and awaits the result
```python
import asyncio
from marspy import MarspyEnvironment, MarspyFissionFunction

async def main():
    environment = await MarspyEnvironment.get("PROD")

    fission_configuration = {
        "router": environment["FISSION_ROUTER"],
        "path": "echo",
        "concurrency": 5
    }

    function = MarspyFissionFunction(fission_configuration)

    def callback(error, result, payload):
        if error:
            print(f"the function returned error {error}")
        else:
            print(f"result type:{type(result)}result:{result}\npayload:{payload}")

    for i in range(1, 11):
        await function.post({
            "first": "Bart",
            "last": "Simpson",
            "index": i
            }, callback)
        print(f"function call #{i}")

    await function.finish()

    print("done testing fission")

asyncio.run(main())


```
## MarspyEmail
send emails
* MarspyEmail(configuration)
instanciates a MarspyEmail client
you can specify the destination adress here, and or subject, if it is always the same
* client.send(email)
sends an email
"email" must have a body property, and can optionaly have a "subject" and "to"

```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyEmail


logging.setLevelFirst(logging.INFO)

async def main():
    await MarspyEnvironment.get("PROD")
    
    #-------------------------- configuration avec les valeurs par défaut
    # notez que "to" peut etre une addresse dans un str, ou une list

    email = MarspyEmail({
        "from": "Gestion Donnees <gestion-donnees@rtbf.be>",
        "to": "martijn.s@gmail.com",
        "subject": "alerte !"
    })

    #-------------------------- example #1
    await email.send("il pleut !")

    #-------------------------- example #2
    body = """
    <html>
        <body>
            <img src="machin.jpg" />
        </body>
    </html>
    """

    await email.send({
        "to": "martijn.s@gmail.com",
        "subject": "test with html body",
        "body": body
    })

    logging.info("done testing MarspyEmail")


asyncio.run(main())

```
## MarspyGit

fetches, compiles and executes code directly from a git repository
MarspyGit is very practical when a team develops little bits of shared code that change frequently, like mappers, functions to store data etc..
MarspyGit is one of the cornerstones of MarspyData that handles input/output and mapping of data

```python
import asyncio
from marspy import MarspyLogging as logging
from marspy import MarspyEnvironment
from marspy import MarspyUtilities
from marspy import MarspyGit

logging.setLevelFirst(logging.INFO)

async def main1():
    await MarspyEnvironment.get("PROD")
    # source = await MarspyGit.get(repository="bigdata-media/marspy_tool", path="functions/article/cryo/get.py")
    # print(f"\n---------------------- MarspyGit.get ----------------------\n{source}")
    #
    # result = await MarspyGit.compile("bigdata-media/marspy_tool", "functions/article/cryo/get.py")
    # print(f"\n---------------------- MarspyGit.compile ------------------\n{MarspyUtilities.stringify(result)}")
    #
    # result = await MarspyGit.call("bigdata-media/marspy_tool", "functions/article/cryo/get.py", "function", 123454321)
    # print(f"\n---------------------- MarspyGit.call ---------------------\n{result}")

    result = await MarspyGit.call("bigdata-media/marspy_tool", "functions/media/mongo/get.py", "function", 2039908)
    print(f"\n---------------------- MarspyGit.call mongo ---------------\n{result}")

    logging.info("done testing MarspyGit")

async def main():
    await MarspyEnvironment.get("PROD")

    result = await MarspyGit.call("bigdata-media/marspy_tool", "functions/media/mongo/get.py", "function", 2039908)
    MarspyUtilities.prettyprint(result)


asyncio.run(main())
```

## MarspyGigya
```python
import asyncio
import os
from marspy import MarspyEnvironment
from marspy import MarspyGigya
from marspy import MarspyAccumulator
from marspy import MarspyStream
from marspy import MarspyRedshift

ENV = os.environ.get("ENV_VARIABLE", "DEV")

async def main():
    await MarspyEnvironment.get(ENV)
   
    gigya = MarspyGigya()
    accumulator = MarspyAccumulator()
    redshift = MarspyRedshift()

    stream = MarspyStream({
        "url": "s3://big-data-media/playground/microservice-template/gigya_example.json"
    })


    def map_user(user):
        # insert code here
        return user

    def stream_to_s3(records):
        stream.put(records)

    async def receive_from_gigya(user):
        print(f"user: {user}")
        # gigya.stop_query()
        await accumulator.accumulate(100, map_user(user), stream_to_s3)

    # get a single user
    await gigya.query("select * from accounts limit 1", receive_from_gigya)

    # get a all users
    await gigya.query("select * from accounts", receive_from_gigya)

    await redshift.create_table("marspy_test")
    #await redshift.load({
    #    "from": "s3://big-data-media/playground/microservice-template/gigya_example.json",
    #    "into": "marspy_gigya"
    #})

    await gigya.stop()
    await accumulator.stop()
    await stream.stop()
    await redshift.stop()


asyncio.run(main())
```