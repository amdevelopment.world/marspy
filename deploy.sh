# todo, adapt this

__VERSION__=$(grep -o "__version__.*'.*'" ./marspy/__init__.py | grep -o "'.*'" | sed 's/"//g' | cut -d "'" -f 2)
echo $__VERSION__

export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain bda-rtbf-repo --domain-owner 435994096874 --query authorizationToken --output text`
# pip freeze > requirements.txt
aws codeartifact login --tool pip --repository bda_rtbf_repo --domain bda-rtbf-repo --domain-owner 435994096874
aws codeartifact login --tool twine --repository bda_rtbf_repo --domain bda-rtbf-repo --domain-owner 435994096874
python setup.py sdist
twine upload --repository codeartifact dist/marspy-$__VERSION__.tar.gz

# you have to install twine using "pip install twine" first
##FOR INSTALL##
#pip install -i https://aws:$CODEARTIFACT_AUTH_TOKEN@bda-rtbf-repo-435994096874.d.codeartifact.eu-west-1.amazonaws.com/pypi/bda_rtbf_repo/simple/ --extra-index-url https://pypi.org/simple -r requirements.txt