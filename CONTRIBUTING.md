## How to start
  * create a venv
  * pip install the requirements.txt

## How to create a new module
  * think of a nice name that best describes what the module is for
  * create a file in "*/tests*", called "*marspy_name.py*" (where "*name*" is the name of your module in lower snake case)
  * this is the starting point, write some simple tests to test the code you will create in the library, before you even write the code
   writing the tests before the code itself helps deciding how to call the methods, what methods to create etc..
  * create a file in "/marspy" with the same name
  * edit "*/marspy/\_\_init\_\_.py*"
    add the name of the class to the "*\_\_all\_\_*" list
    add the from clause
    ```python
    from .marspy_name import MarspyName
    ```
  * **create the class**
  * create the class in "*/marspy_name.py*", test it by executing "*/tests/marspy_name.py*"
  * the class should be named "*marspyName*" where "*Name*" is the name of your module in camelcase
  * don't hesitate to use existing classes as an example
  * use simple method names, like "*query*", "*get*", "*put*", "*post*", "*delete*"
  * always create a "stop" method that disposes of any resource, connections etc..
  * use async await when possible, use the async versions of your dependencies when they exist
  * add top level dependencies to "*requirements.txt*"

## How to create a new function
  if you just have a handy function that isn't related to any technology or concept in particular:
  just add it to "*/marspy/marspy_utilities.py*"

## How to publish a new version of the library
  * increment the version number in "*/marspy/\_\_init\_\_.py*"
  * execute the ./deploy.sh script
  ```bash
  ./deploy.sh
  ```