# replace following line by yours
#aws codeartifact login --tool pip --repository CODE_ARTIFACT_REPO --domain CODE_ARTIFACT_DOMAIN --domain-owner AWS_ACCOUNT_NUMBER
virtualenv venv
source venv/bin/activate
python3 -m pip install -r requirements.txt