from setuptools import setup, find_packages
import re

def read(fname):
    with open(fname) as fp:
        content = fp.read()
    return content

def find_version(fname):
    """
    Attempts to find the version number in the file names fname.
    Raises RuntimeError if not found.
    """
    version = ''
    with open(fname, 'r') as fp:
        reg = re.compile(r'__version__ = [\'"]([^\'"]*)[\'"]')
        for line in fp:
            m = reg.match(line)
            if m:
                version = m.group(1)
                break
    if not version:
        raise RuntimeError('Cannot find version information')
    return version


__version__ = find_version("marspy/__init__.py")

with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

setup(
    name='marspy',
    version=__version__,
    author='Martijn Scheffer',
    author_email='mars@amdevelopment.world',
    description=('Mars Python Library'),
    long_description=read('README.md'),
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=requirements,
    test_suite='tests',
    include_package_data=True,
    package_data={
        '': ['*.pem'],
    }
)
