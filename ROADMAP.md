### done:
  + √ marspy_stream write
  + √ marspy_configuration, read from yaml
  + √ marspy_gigya search accounts
  + √ marspy_configuration amélioration de la méthode "subscribe"
  + √ get default values from environement when possible
  + √ marspy_logging
  + √ general mecanism for handling concurrency
  + √ mongo concurrency
  + √ marsdashboard configuration forms
  + √ neo concurrency
  + √ sql concurrency
  + √ marspy_configuration delete
  + √ release v0.6.0
  + √ release v0.6.1 (removed stray logging)
  + √ redshift, simple load (COPY command)
  + √ marspy_kafka, get, group_id
  + √ marspy_kafka create_topic, delete_topic
  + √ marspy_email send emails
  + √ v0.10.0
  + √ marspy_stream copy, read
  + √ v0.11.1
  + √ marspy_stream copy from ftp
  + √ marspy_stream open ftp
  + √ marspy_stream copy to ftp
  + √ marspy_stream stream to ftp
  + √ marspy_utilities collect
    une fonctions qui permet de faire une liste
    de fonctions, qui retournent une liste de données (des reco par example ?)
    jusqu'au moment ou le nombre de réponse atteint la limite voulue
  + √ marspy_data create JSON Schema from data
  + √ marspy_stream get file size
  + √ marspy_trace
  + √ marspy_data store JSON Schema in DB
  + √ marspy_data recognize data
  + √ marspy_sqs, create, has, delete queue, subscribe, put messages
  + √ marspy_s3_select
  + √ marspy_cache redis
  
### priority:
  + fix marspy_gigya to read all users
  + marspy_sql, optimize connection

### todo:
  + marspy_data map data
  + marspy_redshift update, merge, detect new and/or deleted lines
  + marspy_cache local memory cache
  + marspy documentation, how ? pydoc ?
  + marspy_logging, stoquage dans s3 ?

### low priority
  + marspy_parquet, parquetisez

### probably not going to do
  + marspy_stream, write to kafka
  + marspy_webservice
    j'ai pas trouvé beaucoup de valeur rajoutée en faisant une classe qui "wrap" les web services