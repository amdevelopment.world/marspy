import types
import json
import aiokafka
from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import TopicAlreadyExistsError
from marspy import MarspyUtilities
from marspy import MarspyEnvironment
from marspy import MarspyTask
from .managers.marspy_kafka_manager import MarspyKafkaManager

class MarspyKafkaConsumer:
    def __init__(self, marspy_kafka, url, topic, group_id, _from, callback):
        self.marspy_kafka = marspy_kafka
        self.url = url
        self.topic = topic
        self.group_id = group_id
        self._from = _from
        self.callback = callback

        self.consumer = None
        self.number_of_parameters_to_callback = MarspyUtilities.count_parameters(self.callback)

    def __del__(self):
        print("__del__ called")

    def start(self):
        info = {
            "topic": self.topic,
            "group_id": self.group_id
        }

        async def consume():
            try:
                if self._from == "start":
                    auto_offset_reset = "earliest"
                else:
                    auto_offset_reset = "latest"

                def deserializer(serialized):
                    return json.loads(serialized)

                self.consumer = aiokafka.AIOKafkaConsumer(
                    self.topic,
                    bootstrap_servers=self.url,
                    group_id=self.group_id,
                    value_deserializer=deserializer,
                    auto_offset_reset=auto_offset_reset
                )

                await self.consumer.start()
                async for message in self.consumer:
                    # await asyncio.sleep(0.00001)
                    # msg.topic, msg.partition, msg.offset, msg.key, msg.value,msg.timestamp
                    
                    value = message.value

                    if self.number_of_parameters_to_callback == 1:
                        await MarspyUtilities.smart_await(self.callback(value))
                    else:
                        await MarspyUtilities.smart_await(self.callback(value, {
                            "key": message.key,
                            "timestamp": message.timestamp,
                            "offset": message.offset,
                            "partition": message.partition,
                            **info
                        }))
            except Exception as e:
                print(e)
        
        MarspyTask(consume())

    async def stop(self):
        if self.consumer is not None:
            await self.consumer.stop()

        self.marspy_kafka._remove_consumer(self)

class MarspyKafka:
    def __init__(self, configuration={}):
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "KAFKA_URL", "MarspyKafka")
        url_bits = MarspyUtilities.splitURL(self.url)
        url_bits['scheme'] = ''
        self.url = MarspyUtilities.joinURL(url_bits)

        self.topic = configuration.get('topic')
        self.group_id = configuration.get('group_id')
        self._from = configuration.get('from')
        self.callback = configuration.get('from')
        self.num_partitions = configuration.get('num_partitions')
        self.replication_factor = configuration.get('replication_factor')

        self.producer = None
        self.consumers = []


    async def stop(self):
        if self.producer:
            await self.producer.stop()

        for i in range(len(self.consumers)-1, -1, -1):
            consumer = self.consumers[i]
            await consumer.stop()

    async def post(self, payload):
        return self.put(payload)

    async def put(self, payload):
        producer = await self._get_producer()

        if isinstance(payload, list):
            for item in payload:
                await self.put(item)
            return
        elif isinstance(payload, str):
            message = payload.encode('utf-8')
        else:
            message = json.dumps(payload, default=str).encode('utf-8')

        await producer.send(self.topic, message)

    def get(self, *arguments):
        settings = MarspyUtilities.get_a(dict, arguments, {})
        callback = MarspyUtilities.get_a(types.FunctionType, arguments, self.callback, "MarspyKafka", "subscribe", "callback")

        url = settings.get("url", self.url)
        topic = settings.get("topic", self.topic)
        _from = settings.get("from", self._from)
        group_id = settings.get("group_id", self.group_id)

        marspy_kafka_consumer = MarspyKafkaConsumer(self, url, topic, group_id, _from, callback)
        self.consumers.append(marspy_kafka_consumer)
        marspy_kafka_consumer.start()

        return marspy_kafka_consumer

    async def create_topic(self, *arguments):
        topic = MarspyUtilities.get_a(str, arguments, self.topic, "MarspyKafkaConsumer", "create_topic", "topic")
        settings = MarspyUtilities.get_a(dict, arguments, {})
        num_partitions = settings.get("num_partitions", self.num_partitions) or 1
        replication_factor = settings.get("replication_factor", self.num_partitions) or 1
        url = settings.get("url", self.url)

        admin_client = KafkaAdminClient(
            bootstrap_servers=url, 
            client_id='marspy_kafka'
        )

        new_topics = [
            NewTopic(name=topic, num_partitions=num_partitions, replication_factor=replication_factor)
        ]
        
        try:
            admin_client.create_topics(new_topics=new_topics, validate_only=False)
            return True
        except TopicAlreadyExistsError:
            return False

    
    async def delete_topic(self, *arguments):
        settings = MarspyUtilities.get_a(dict, arguments, {})
        topic = MarspyUtilities.get_a(str, arguments, self.topic, "MarspyKafkaConsumer", "create_topic", "topic")
        url = settings.get("url", self.url)
        
        admin_client = KafkaAdminClient(
            bootstrap_servers=url, 
            client_id='marspy_kafka'
        )

        admin_client.delete_topics([
            topic
        ])
    
    def _remove_consumer(self, consumer):
        self.consumers.remove(consumer)

    async def _get_producer(self):
        if self.producer is None:
            self.producer = aiokafka.AIOKafkaProducer(bootstrap_servers=self.url)
            await self.producer.start()

        return self.producer

