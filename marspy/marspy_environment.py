import boto3
import urllib
#from .marspy_logging import MarspyLogging as logging

global_environment = None
global_instance = None

class MarspyEnvironment:
    def __init__(self, configuration={}):
        self.get = self._instance_get
        self.initialized = False
        self.region_name = configuration.get("region", "eu-west-1")
        self.ssm_client = boto3.client('ssm', region_name=self.region_name)

        if "repository_url" in configuration:
            self.repository_url = configuration["repository_url"]
        elif "repository_secret_name" in configuration:
            self.repository_url = self.get_secret(configuration["repository_secret_name"])
        else:
            self.repository_url = self.get_secret("MARSPY_REPOSITORY_URL")

        if "://" not in self.repository_url:
            self.repository_url = f"mongodb://{self.repository_url}"

    async def initialize(self):
        if self.initialized:
            return

        parsed_url = urllib.parse.urlsplit(self.repository_url)
        database = parsed_url.path[1:]

        from .marspy_mongo import MarspyMongo

        self.mongo = MarspyMongo({
            "url": self.repository_url,
            "database": database
        })

        self.initialized = True

    async def stop(self):
        if self.initialized:
            self.initialized = False
            await self.mongo.stop()

    def get_secret(self, name):
        response = self.ssm_client.get_parameter(Name=name, WithDecryption=True)
        return response["Parameter"]["Value"]

    async def get_secrets(self, names):
        names = self._ensure_list(names)

        dictionary = {}

        while len(names):
            first_ten_names = names[:10]
            names = names[10:]
            response = self.ssm_client.get_parameters(Names=first_ten_names, WithDecryption=True)

            for parameter in response["Parameters"]:
                dictionary[parameter["Name"]] = parameter["Value"]

        return dictionary

    async def list(self):
        await self.initialize()
        names = await self.mongo.list_values("environments", "name")
        names.sort()
        return names

    async def list_hierarchical(self):
        await self.initialize()
        names = []
        documents = await self.mongo.get_all_documents("environments", {"name": True, "environments": True})

        documents.sort(key=lambda document: document["name"])

        for document in documents:
            names.append(document["name"])

            included_document_names = self._list_of_included_documents(document)
            included_document_names.sort()
            for name in included_document_names:
                names.append(f"  -> {name}")

        return names

    async def _instance_get(self, names):
        values = await self._get_recursively(names)

        per_scheme = {}

        for key in values:
            value = values[key]

            if "://" in value:
                prefix_and_path = value.split("://")
                scheme = prefix_and_path[0]
                path = prefix_and_path[1]

                if scheme not in per_scheme:
                    per_scheme[scheme] = {
                        "list": [],
                        "map": {}
                    }

                per_scheme[scheme]["list"].append(path)
                per_scheme[scheme]["map"][key] = path

        if "ssm" in per_scheme:
            mapped_names = per_scheme["ssm"]["map"]
            secrets = await self.get_secrets(per_scheme["ssm"]["list"])

            for name in mapped_names:
                secret_name = mapped_names[name]

                if secret_name in secrets:
                    secret_value = secrets[secret_name]
                    values[name] = secret_value
                else:
                    print(f"ssm://{name} not found")

        global global_environment
        global_environment = values
        
        await self.stop()
        return values

    async def _get_recursively(self, names):
        await self.initialize()
        values = {}

        if len(names):
            names = self._ensure_list(names)
            documents = await self.mongo.get_documents("environments", "name", names)

            if len(documents) != len(names):
                if len(names) > 1:
                    raise Exception(f"{len(names) - len(documents)} of environments \"{','.join(names)}\" could not be found")
                else:
                    raise Exception(f"environment \"{(names[0])}\" could not be found")

            for document in documents:
                # get all properties from document
                if "properties" in document:
                    for property in document["properties"]:
                        values[property["name"]] = property["value"]

                # get all properties inherited from included environments
                included_document_names = self._list_of_included_documents(document)
                included_values = await self._get_recursively(included_document_names)
                values.update(included_values)

        return values

    def _ensure_list(self, l):
        if isinstance(l, str):
            l = l.split(",")
        return l

    def _list_of_included_documents(self, document):
        if "environments" in document:
            return list(map(lambda entry: entry['name'], document["environments"]))
        else:
            return []

    @staticmethod
    def _get_instance():
        global global_instance

        if global_instance == None:
            global_instance = MarspyEnvironment({})
        else:
            print("you have already called MarspyEnvironment.get(), please call it only once")

        return global_instance

    @staticmethod
    def get(name):
        return MarspyEnvironment._get_instance().get(name)

    @staticmethod
    def get_value(name):
        if global_environment:
            return global_environment.get(name)

    @staticmethod
    def get_configuration_value(configuration, name, env_name, module_name):
        value = configuration.get(name)
        
        if value == None:
            if global_environment == None:
                raise Exception(f"you have to provide a value for \"{name}\" OR call MarspyEnvironment before instanciating \"{module_name}\" if you do environment[\"{env_name}\"] will be used")
            else:
                value = global_environment.get(env_name)

                if value == None:
                    raise Exception(f"you have to provide a value for \"{name}\" or environment[\"{env_name}\"] has to exist when instanciating \"{module_name}\"")

        return value

    @staticmethod
    def help():
        print("----- MarspyEnvironment help -----")
