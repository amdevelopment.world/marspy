import types
import asyncio
from .marspy_logging import MarspyLogging as logging
from .marspy_utilities import MarspyUtilities

class MarspyQueueEntry:
    def __init__(self, queue, fn, callback, arguments, keyarguments):
        self.queue = queue
        self.fn = fn
        self.callback = callback
        self.arguments = arguments
        self.keyarguments = keyarguments

    async def call(self):
        error = None
        result = None

        try:
            result = await MarspyUtilities.smart_await(self.fn(*self.arguments, **self.keyarguments))
        except Exception as exception:
            error = exception
            logging.error(f"exception in MarspyQueueEntry.call: {error}")
        finally:
            self.queue._dequeue(entry=self)
            number_of_parameters_to_callback = MarspyUtilities.count_parameters(self.callback)
            
            try :
                logging.info("calling callback from MarspyQueueEntry")
                if number_of_parameters_to_callback == 0:
                    await MarspyUtilities.smart_await(self.callback())
                elif number_of_parameters_to_callback == 1:
                    await MarspyUtilities.smart_await(self.callback(result))
                else:
                    await MarspyUtilities.smart_await(self.callback(error, result, *self.arguments, **self.keyarguments))
                logging.info("called callback from MarspyQueueEntry")
            except Exception as e:
                logging.error(f"MarspyQueue got an exception when calling the callback", e)

class MarspyQueue:
    def __init__(self, size=0, count=0):
        if size:
            self.space = size
        elif count:
            self.space = count
        else:
            raise Exception('MarspyQueue needs a value for "size" or "count" (depricated)')

        self.counter = 0
        self.queue = []

    async def stop(self):
        return await self.empty()

    async def empty(self):
        while len(self.queue) or self.counter:
            await asyncio.sleep(0.005)

    async def call(self, fn, *arguments, **keyarguments):
        if len(arguments) and isinstance(arguments[0], types.FunctionType):
            arguments = list(arguments)
            callback = arguments[0]
            arguments.remove(callback)
            arguments = tuple(arguments)

            return await self._call_with_callback(fn, callback, arguments, keyarguments)
        else:
            return await self._call_without_callback(fn, arguments, keyarguments)

    def set_space(self, space):
        self.space = space

    def get_length(self):
        return len(self.queue) + self.counter

    async def _call_with_callback(self, fn, callback, arguments, keyarguments):
        entry = MarspyQueueEntry(self, fn, callback, arguments, keyarguments)

        # wait for free queue entry
        while len(self.queue) >= self.space:
            await asyncio.sleep(0.005)

        self.queue.append(entry)
        loop = asyncio.get_event_loop()
        loop.create_task(entry.call())

    async def _call_without_callback(self, fn, arguments, keyarguments):
        await self._wait()

        try:
            self.counter += 1
            result = await fn(*arguments, **keyarguments)
            return result
        except RuntimeError:
            pass
        except Exception as e:
            logging.error(e, type(e).__name__)
            raise e
        finally:
            self.counter -= 1

    def _dequeue(self, entry):
        self.queue.remove(entry)

    async def _wait(self):
        while self.counter >= self.space:
            await asyncio.sleep(0.001)
