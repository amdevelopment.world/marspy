import os
from marspy.marspy_utilities import MarspyUtilities

class Handler:
    @staticmethod
    def recognizes_url(url):
        url_bits = MarspyUtilities.splitURL(url)
        scheme = url_bits["scheme"].lower()
        return scheme == 'file'

    @staticmethod
    async def getsize(url):
        pass