import os
import ftplib 
from marspy.marspy_utilities import MarspyUtilities

path_to_logs = MarspyUtilities.find_directory_containing("logs/") + '/logs'

class MarspyFTPFile:
    def __init__(self, url):
        self.url = url
        self.file = None

    def open(self, *args, **kwargs):
        url_bits = MarspyUtilities.splitURL(self.url)
        self.file_name = os.path.basename(url_bits["path"])
        self.path_to_temp_file = f"{path_to_logs}/to_{self.file_name}"
        self.file = open(self.path_to_temp_file, *args, **kwargs)

    def write(self, data):
        self.file.write(data)

    def close(self):
        if self.file is not None:
            self.file.close()

        ftp = Handler.connect(self.url)

        file = open(self.path_to_temp_file, 'rb')

        ftp.storlines('STOR ' + self.file_name, file)
        ftp.quit()
        os.remove(self.path_to_temp_file)
        

class Handler:
    @staticmethod
    def recognizes_url(url):
        url_bits = MarspyUtilities.splitURL(url)
        return url_bits["scheme"].lower() == 'ftp'

    @staticmethod
    def open(url, *args, **kwargs):
        ftp_file = MarspyFTPFile(url)
        ftp_file.open(*args, **kwargs)
        return ftp_file

    @staticmethod
    def connect(url):
        url_bits = MarspyUtilities.splitURL(url)

        path = url_bits["path"]
        dir_name = os.path.dirname(path)

        ftp = ftplib.FTP(url_bits["domain"])
        ftp.login(user=url_bits["username"], passwd=url_bits["password"])

        ftp.cwd(dir_name)

        return ftp

    @staticmethod
    def download(url):
        url_bits = MarspyUtilities.splitURL(url)
        file_name = os.path.basename(url_bits["path"])
        path_to_temp_file = f"{path_to_logs}/from_{file_name}"

        ftp = Handler.connect(url)

        with open(path_to_temp_file, 'wb') as file:
            ftp.retrbinary('RETR ' + file_name, file.write)

        ftp.quit()

        return f"file://{path_to_temp_file}"