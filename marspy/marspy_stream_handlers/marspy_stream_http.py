import os
from marspy.marspy_utilities import MarspyUtilities
from marspy import MarspyHTTP

class Handler:
    @staticmethod
    def recognizes_url(url):
        url_bits = MarspyUtilities.splitURL(url)
        scheme = url_bits["scheme"].lower()
        return scheme == 'http' or scheme == 'https'

    @staticmethod
    async def getsize(url):
        headers = await MarspyHTTP.head(url)
        return int(headers.get("Content-Length", "-1"))