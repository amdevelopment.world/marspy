import sys
import json
import asyncio
from aiobotocore.session import get_session
from marspy import MarspyUtilities
from marspy import MarspyTask
from marspy import MarspyEnvironment

class MarspySQSSubscriber():
    def __init__(self):
        self._running = True
        self._stopped = False

    def running(self):
        return self._running

    def stopped(self):
        self._stopped = True

    async def stop(self):
        self._running = False
        while self._stopped != True:
            await asyncio.sleep(0.1)

class MarspySQSManager():
    def __init__(self):
        self.aws_region = "eu-west-1"
        self.async_sqs = None
        self.async_sqs_context = None

        self.aws_access_key_id = MarspyEnvironment.get_value("AWS_ACCESS_KEY_ID")
        self.aws_secret_access_key=MarspyEnvironment.get_value("AWS_SECRET_ACCESS_KEY")

        self.queue_urls = {}
        self.subscribers = []

    async def get_client(self):
        if self.async_sqs is None:
            session = get_session()
            self.async_sqs_context = session.create_client('sqs', region_name=self.aws_region)
            self.async_sqs = await self.async_sqs_context.__aenter__()

        return self.async_sqs

    async def stop(self):
        for subscriber in self.subscribers:
            await subscriber.stop()
        self.subscribers = []

        if self.async_sqs_context is not None:
            await self.async_sqs_context.__aexit__(*sys.exc_info())

        self.async_sqs = None
        self.async_sqs_context = None

    async def set_region(self, region):
        self.aws_region = region
        await self.stop()

    async def _get_queue_url(self, name):
        if name in self.queue_urls:
            return self.queue_urls[name]
        else:
            client = await self.get_client()
            response = await client.get_queue_url(QueueName=name)
            queue_url = response['QueueUrl']
            self.queue_urls[name] = queue_url
            return queue_url

    async def create_queue(self, name):
        client = await self.get_client()
        response = await client.create_queue(QueueName=name)

    async def has_queue(self, name):
        client = await self.get_client()
        try:
            response = await client.get_queue_url(QueueName=name)
            return True
        except Exception as e:
            return False

    async def delete_queue(self, name):
        client = await self.get_client()
        queue_url = await self._get_queue_url(name)
        response = await client.delete_queue(QueueUrl=queue_url)
        print(response)

    async def subscribe(self, name, callback):
        client = await self.get_client()
        queue_url = await self._get_queue_url(name)
        
        async def loop(subscriber):
            session = get_session()

            async with session.create_client('sqs', region_name=self.aws_region) as client:
                while subscriber.running():
                    try:
                        response = await client.receive_message(QueueUrl=queue_url, MaxNumberOfMessages=1, WaitTimeSeconds=1)
                        if 'Messages' in response:
                            for message in response['Messages']:
                                await MarspyUtilities.smart_await(callback(message["Body"]))
                                await client.delete_message(QueueUrl=queue_url, ReceiptHandle=message['ReceiptHandle'])
                    except Exception as e:
                        print(e)

                subscriber.stopped()

        subscriber = MarspySQSSubscriber()
        MarspyTask(loop(subscriber))
        self.subscribers.append(subscriber)

        return subscriber
        

    async def put(self, name, body, attributes=None):
        client = await self.get_client()
        queue_url = await self._get_queue_url(name)

        if attributes is not None:
            response = await client.send_message(
                QueueUrl=queue_url,
                DelaySeconds=1,
                MessageAttributes=attributes,
                MessageBody=json.dumps(body, default=str)
            )
        else:
            response = await client.send_message(
                QueueUrl=queue_url,
                DelaySeconds=1,
                MessageBody=json.dumps(body, default=str)
            )
