import sys
import json
from aiobotocore.session import get_session
from marspy import MarspyUtilities
from marspy import MarspyEnvironment

InputSerializationForExtension = {
    "gz": {
        "CompressionType": "GZIP",
        "JSON": {
            "Type": "Lines"
        }
    },
    "bz2": {
        "CompressionType": "BZIP",
        "JSON": {
            "Type": "Lines"
        }
    },
    "json": {
        "JSON": {
            "Type": "Lines"
        }
    },
    "csv": {
        "CSV": {
            "FileHeaderInfo": "Use"
        }
    },
    "parquet": {
        "Parquet": {

        }
    }
}

class MarspyS3Manager():
    def __init__(self):
        self.client = None
        self.client_context = None

        self.aws_access_key_id = MarspyEnvironment.get_value("AWS_ACCESS_KEY_ID")
        self.aws_secret_access_key=MarspyEnvironment.get_value("AWS_SECRET_ACCESS_KEY")

    async def get_client(self):
        if self.client is None:
            session = get_session()
            self.client_context = session.create_client('s3')
            self.client = await self.client_context.__aenter__()

        return self.client

    async def stop(self):
        if self.client_context is not None:
            await self.client_context.__aexit__(*sys.exc_info())

        self.client = None
        self.client_context = None

    async def query(self, url, query, callback):
        client = await self.get_client()
        url_bits = MarspyUtilities.splitURL(url)

        Bucket = url_bits["domain"]
        Key =  url_bits["path"][1:]
        path_bits = url_bits["path"].split('.')

        if len(path_bits) > 1:
            extension = path_bits[-1]
        else:
            extension = None

        InputSerialization = InputSerializationForExtension[extension or "json"]

        result = await client.select_object_content(
            Bucket=Bucket,
            Key=Key,
            ExpressionType='SQL',
            Expression=query,
            InputSerialization=InputSerialization,
            OutputSerialization = {'JSON': {}},
        )

        last_line = None
        async for event in result["Payload"]:
            records = event.get("Records")
            if records is not None:
                payload = records.get("Payload").decode('utf-8')
                string = str(payload)
                lines = string.split('\n')
                if last_line is not None:
                    lines[0] = last_line + lines[0]

                last_line = lines.pop()

                if callback is not None:
                    for line in lines:
                        await MarspyUtilities.smart_await(callback(json.loads(line)))
                else:
                    result = []
                    for line in lines:
                        result.append(json.loads(line))

                    return result


    def sample(self, url, lines, callback):
        query = """SELECT * FROM S3Object"""
        
        if lines is not None:
            query += f" LIMIT {lines}"

        return self.query(url, query, callback)