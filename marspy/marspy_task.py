import asyncio, threading

_loop = None

class MarspyTask:
    def __init__(self, coroutine):
        global _loop
        if _loop is None:
            _loop = asyncio.new_event_loop()
            threading.Thread(target=_loop.run_forever, daemon=True).start()

        _loop.call_soon_threadsafe(asyncio.create_task, coroutine)