import sys

MarspySharedResources = sys.modules[__name__]

shared_resources = {}

def get(type, key):
    return shared_resources.get(f"{type}/{key}")

def set(type, key, value):
    shared_resources[f"{type}/{key}"] = value
