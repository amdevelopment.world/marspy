from .marspy_environment import MarspyEnvironment
from .marspy_sql import MarspySQL

class MarspyRedshift(MarspySQL):
    def __init__(self, configuration={}):
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "REDSHIFT_URL", "MarspyRedshift")
        self.aws_access_key_id = MarspyEnvironment.get_configuration_value(configuration, "aws_access_key_id", "AWS_ACCESS_KEY_ID", "MarspyRedshift")
        self.aws_secret_access_key = MarspyEnvironment.get_configuration_value(configuration, "aws_access_key_id", "AWS_SECRET_ACCESS_KEY", "MarspyRedshift")

        super().__init__({
            "url": self.url,
            **configuration
        })

    async def load(self, parameters):
        url = parameters.get("url")
        table = parameters.get("into", parameters.get("to"))
        
        if url is None:
            raise Exception('MarspyRedshift.load requires a source url, in the "from" parameter')

        if table is None:
            raise Exception('MarspyRedshift.load requires a table name, in the "to" or "into" parameters (you choose :-)')

        query = f"""
            COPY {table}
            FROM '{url}'
            CREDENTIALS 'aws_access_key_id={self.aws_access_key_id};aws_secret_access_key={self.aws_secret_access_key}'
            JSON 'auto'
            TRUNCATECOLUMNS
            ACCEPTANYDATE
            TIMEFORMAT 'auto'
            DATEFORMAT 'auto';COMMIT
        """

        return await self.query(query)

    @staticmethod
    def query(query, callback=None):
        return MarspySQL.query(query, callback, "REDSHIFT_URL")

    @staticmethod
    def get(table, callback=None, fields="*", limit=None):
        return MarspySQL.get(table, callback, fields, limit, "REDSHIFT_URL")

