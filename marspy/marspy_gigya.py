from marspy.marspy_gigya_lib.GSSDK import GSRequest
from .marspy_utilities import smart_await
from marspy import MarspyEnvironment

class MarspyGigya:
    def __init__(self, configuration={}):
        self.domain = configuration.get("domain", "eu1.gigya.com")
        self.batch_size = configuration.get("batch_size", 1000)

        self.apiKey = MarspyEnvironment.get_configuration_value(configuration, "apiKey", "GIGYA_APIKEY", "MarspyGigya")
        self.secretKey = MarspyEnvironment.get_configuration_value(configuration, "secretKey", "GIGYA_SECRET", "MarspyGigya")
        self.userKey = MarspyEnvironment.get_configuration_value(configuration, "userKey", "GIGYA_USERKEY", "MarspyGigya")

        self.query = self._instance_query
        self.query_api = self._instance_query_api

    async def stop(self):
        pass

    async def _instance_query_api(self, method, query, callback, limit=None):
        self._start_query()
        nextCursorId = None

        if limit is not None and limit <= 10000:
            pagingEnabled = False
            query += f" LIMIT {limit}"
        else:
            pagingEnabled = True
            query += f" LIMIT {self.batch_size}"

        number_of_lines = 0
        list_of_results = []

        while self._executing_query:
            if nextCursorId == None:
                # initial call
                params = {
                    "query": query,
                    "openCursor": pagingEnabled
                }
            else:
                 params = {
                    "cursorId" : nextCursorId
                }

            request = GSRequest(
                self.apiKey,
                self.secretKey,
                method,
                params,
                userKey=self.userKey,
                useHTTPS=True
            )

            request.setAPIDomain(self.domain)

            response = request.send()
            error_code = response.getErrorCode()

            if error_code == 0:
                data = response.getData()
                results = data.get("results", [])

                for line in results:
                    number_of_lines += 1
                    if limit is not None and number_of_lines > limit:
                        self.stop_query()
                    else:
                        if callback is not None:
                            await smart_await(callback(line))
                        else:
                            list_of_results.append(line)                        

                    if self._executing_query == False:
                        break

                nextCursorId = data.get("nextCursorId", None)
            else:
                print(f"error in gigya {method}")

            if nextCursorId == None:
                break

        return list_of_results

    def _instance_query(self, query, callback, limit):
        return self.query_api("accounts.search", query, callback, limit)

    def stop_query(self):
        self._executing_query = False

    def _start_query(self):
        self._executing_query = True

    @staticmethod
    async def query(query, callback=None, limit=None):
        gigya = MarspyGigya()
        result = await gigya.query(query, callback, limit)
        await gigya.stop()
        return result

    @staticmethod
    async def query_api(api, query, callback=None, limit=None):
        gigya = MarspyGigya()
        result = await gigya.query_api(api, query, callback, limit)
        await gigya.stop()
        return result

    @staticmethod
    async def get(callback=None, fields="*", limit=None):
        return await MarspyGigya.query(f"SELECT {fields} FROM accounts", callback, limit)