import asyncio
import time
import math
from marspy import MarspyEnvironment
from marspy import MarspyTask
from marspy import MarspyMongo

class MarspyMetrics:
    def __init__(self, configuration={}):
        self.running = True

        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "MONGO_URL", "MarspyMetrics")

        self.mongo = MarspyMongo({
            "url": self.url,
            "database": configuration.get("database", "mars-dashboard-prod")
        })

        self.service_name = configuration.get("name")
        self.service_id = time.time()
        self.duration = configuration.get("duration", 60 * 6)
        self.metrics = {}

        MarspyTask(self.task())

    async def stop(self):
        self.running = False
        await self.save_samples()
        await self.mongo.stop()

    def _create_sample(self, type, name, tag, start, duration):
        sample = MarspyMetricStorage(self, type, name, tag, start, duration, {
            "service_name": self.service_name,
            "service_id": self.service_id,
            "type": type,
            "name": name,
            "tag": tag,
            "date": start,
            "duration": duration
        })
        return sample

    def _get_sample(self, type, name, tag, specificDuration):
        key = f"{type}.{name}.{tag}"

        if key not in self.metrics.keys():
            # create initial sample, with a shorter duration
            start = time.time()
            end = (math.floor(start/self.duration) * self.duration) + self.duration
            duration = end - start
            self.metrics[key] = self._create_sample(type, name, tag, start, duration)
        else:
            sample = self.metrics[key]

            if sample.finished():
                if sample.modified:
                    self.save_sample(sample)

                if specificDuration:
                    duration = specificDuration
                else:
                    duration = self.duration
                start = math.floor(time.time() / duration) * duration
                self.metrics[key] = self._create_sample(type, name, tag, start, duration)

        return self.metrics[key]

    def count(self, what, specificDuration = 0):
        sample = self._get_sample("count", what, "", specificDuration)
        sample.increment()

    def signal(self, what, tag = "", specificDuration = 0):
        sample = self._get_sample("signal", what, tag, specificDuration)
        sample.increment()

    def measure(self, what, value, specificDuration = 0):
        sample = self._get_sample("dimension", what, "", specificDuration)
        sample.average(value)

    def save_sample(self, sample):
        sample.save(self.mongo)

    async def save_samples(self):
        for sample in self.metrics.values():
            if sample.modified:
                self.save_sample(sample)

    async def task(self):
        while self.running:
            await asyncio.sleep(15)
            await self.save_samples()


class MarspyMetricStorage:
    def __init__(self, metrics, type, name, tag, start, duration, properties):
        self.group_values = type == "signal"
        self.is_dimension = type == "dimension"
        self.is_counter = type != "dimension"

        self.metrics = metrics
        self.type = type
        self.name = name
        self.tag = tag
        self.start = start
        self.duration = duration
        self.properties = properties

        if self.group_values:
            # one record per group of services
            self.key = f"{metrics.service_name}.{name}.{tag}.{start}"
        else:
            # one record per instance
            self.key = f"{metrics.service_name}.{metrics.service_id}.{name}.{tag}.{start}"
        
        self.modified = False
        self.new = True

        self.total = 0
        self.counter = 0
        self.value = 0


    def increment(self):
        self.value += 1
        self.modified = True

    def average(self, value):
        self.counter += 1
        self.total += value
        self.value = self.total / self.counter
        self.modified = True

    async def create(self, mongo):
        if self.is_dimension:
            await mongo.create_document("metrics", {
                "key": self.key,
                "value": self.value,
                **self.properties,
            })
        else:
            await mongo.upsert_document("metrics", {
                "$setOnInsert": {
                    **self.properties,
                },
                "$inc": {
                    "value": self.value
                }
            }, {
                "key": self.key
            })

            self.value = 0

    async def update(self, mongo):
        if self.is_dimension:
            await mongo.update_document("metrics", {
                "value": self.value,
            },{
                "key": self.key
            })
        else:
            await mongo.upsert_document("metrics", {
                "$inc": {
                    "value": self.value
                }
            }, {
                "key": self.key
            })

            self.value = 0

    def save(self, mongo):
        if self.new:
            self.new = False
            MarspyTask(self.create(mongo))
        else:
            MarspyTask(self.update(mongo))

        self.modified = False

    def finished(self):
        return time.time() > (self.start + self.duration)