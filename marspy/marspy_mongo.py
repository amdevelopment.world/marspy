import asyncio
from motor.motor_asyncio import AsyncIOMotorClient
from .marspy_baseclass import MarspyBaseClass
from .marspy_logging import MarspyLogging as logging

global_mongo = None

class MarspyMongo(MarspyBaseClass):
    def __init__(self, configuration={}):
        from .marspy_environment import MarspyEnvironment

        self.collections = {}
        self.client = None
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "MONGO_URL", "MarspyMongo")
        self.database = configuration.get("database", "test")
        self.query = self._instance_query
        self.stop = self._instance_stop

        super().__init__(configuration)

    async def _instance_stop(self):
        if self.client != None:
            client = self.client
            self.client = None
            client.close()

    def get_client(self):
        if self.client:
            return self.client

        try:
            self.client = AsyncIOMotorClient(self.url)
            self.client.get_io_loop = asyncio.get_running_loop
        except Exception as e:
            print(f"Error when tryng to connect to MongoDB or DocumentDB {e}")
            raise e

        return self.client
    
    async def create_collection(self, configuration):
        collection_name = configuration["name"]
        indexes = configuration.get("indexes", [])

        client = self.get_client()
        db = client.get_database(self.database)

        collection = db[collection_name]

        try:
            for index in indexes:
                if isinstance(index, dict):
                    properties = index.get("properties")
                    await collection.create_index(properties, **index.get("options", {}))
                else:
                    await collection.create_index(index)
        except Exception as e:
            print(f"Warning: an error trying to create index {index}: {e}")
            print(f"this can be caused by a modification of some index options and is not neccesarely a problem")

    async def _get_collection(self, collection_name):
        if collection_name not in self.collections.keys():
            client = self.get_client()

            db = client.get_database(self.database)

            self.collections[collection_name] = db.get_collection(collection_name)

        return self.collections[collection_name]

    async def _instance_query(self, collection_name, query, callback=None, fields=[]):
        async def do():
            collection = await self._get_collection(collection_name)

            if len(fields):
                projection = {}
                for field in fields:
                    projection["field"] = True
                    cursor = collection.find(query, projection)
            else:
                cursor = collection.find(query)

            documents = []

            async for document in cursor:
                if callback:
                    callback(document)
                else:
                    documents.append(document)
            
            return documents

        return await self.queue_call(do)

    async def list_values(self, collection_name, field_name):
        async def do(): 
            collection = await self._get_collection(collection_name)
            query = {}
            projection = {field_name: True}

            cursor = collection.find(query, projection)
            documents = await cursor.to_list(None)
            values = list(map(lambda document: document[field_name], documents))
            return values
        return await self.queue_call(do)

    async def get_all_documents(self, collection_name, projection = {}):
        async def do(): 
            collection = await self._get_collection(collection_name)
            query = {}
            cursor = collection.find(query, projection)
            documents = await cursor.to_list(None)
            return documents

        return await self.queue_call(do)

    async def get_documents(self, collection_name, field_name, values):
        if isinstance(values, str):
            values = [values]

        async def do():    
            collection = await self._get_collection(collection_name)
            query = {field_name: {"$in": values}}
            cursor = collection.find(query)
            return await cursor.to_list(None)
        return await self.queue_call(do)

    async def create_document(self, collection_name, document):
        async def do():
            collection = await self._get_collection(collection_name)
            await collection.insert_one(document)
        await self.queue_call(do)

    async def delete_document(self, collection_name, query):
        async def do():
            collection = await self._get_collection(collection_name)
            await collection.delete_one(query)
        await self.queue_call(do)

    async def update_document(self, collection_name, document, query):
        async def do():
            collection = await self._get_collection(collection_name)
            await collection.update_one(query, { "$set": document })
        await self.queue_call(do)

    async def upsert_document(self, collection_name, document, query):
        async def do():
            collection = await self._get_collection(collection_name)
            await collection.update_one(query, document, upsert=True)
        await self.queue_call(do)

    async def get_missing_values_in_collection(self, collection_name, field_name, values):
        async def do():
            collection = await self._get_collection(collection_name)
            query = {field_name: {"$in": values}}
            projection = {field_name: True}

            cursor = collection.find(query, projection)
            existing_documents = await cursor.to_list(None)
            existing_values = list(map(lambda document: document[field_name], existing_documents))

            return list(set(values) - set(existing_values))

        return await self.queue_call(do)

    @staticmethod
    def query(collection_name, query={}, callback=None):
        global global_mongo
        mongo = global_mongo or MarspyMongo()
        return mongo.query(collection_name, query, callback)

    @staticmethod
    def get(collection_name, callback=None, limit=None):
        return MarspyMongo.query(collection_name, callback=callback)

    @staticmethod
    async def stop():
        global global_mongo

        if global_mongo is not None:
            await global_mongo.stop()
            global_mongo = None
