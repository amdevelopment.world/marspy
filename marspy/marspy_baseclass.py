import asyncio
from marspy import MarspyQueue


class MarspyBaseClass:
    def __init__(self, configuration={}):
        self.initialize_concurrency(configuration.get("concurrency"))
        self._number_of_queued_calls = 0
        self._number_of_completed_queued_calls = 0

    def initialize_concurrency(self, concurrency):
        self.concurrency = concurrency

        if isinstance(concurrency, int) and concurrency != 0:
            self._concurrency_queue = MarspyQueue(concurrency)
        else:
            self._concurrency_queue = None

    async def set_concurrency(self, concurrency):
        if concurrency == self.concurrency:
            return

        if concurrency is None or concurrency == 0:
            if self._concurrency_queue is not None:
                queue = self._concurrency_queue
                self._concurrency_queue = None
                await queue.stop()
        else:
            if self._concurrency_queue is None:
                self.initialize_concurrency(concurrency)
            else:
                self._concurrency_queue.set_space(concurrency)

    async def queue_call(self, task, *arguments, **keyarguments):
        if self._concurrency_queue is not None:
            await asyncio.sleep(0.0001)

            # check the queue again, in case it dissapeared while sleeping
            if self._concurrency_queue is None:
                return await self.queue_call(task, *arguments, **keyarguments)

            result = await self._concurrency_queue.call(task, *arguments, **keyarguments)
            self._number_of_completed_queued_calls += 1
            return result
        else:
            self._number_of_queued_calls += 1
            result = await task(*arguments, **keyarguments)
            self._number_of_queued_calls -= 1
            self._number_of_completed_queued_calls += 1
            return result 

    def tasks_pending(self):
        if self._concurrency_queue:
            return self._concurrency_queue.get_length()
        else:
            return self._number_of_queued_calls

    def tasks_completed(self):
        return self._number_of_completed_queued_calls