from .marspy_utilities import smart_await

class MarspyAccumulator:
    def __init__(self):
        self.deprecated_accumulate_warning_given = False
        self.reset()

    def accumulate(self, count, item_or_list, callback=None):
        self.count = count

        if isinstance(item_or_list, list):
            if callback == None:
                raise Exception("accumulator.accumulate can only be called with a list if the third parameter is a callback\nthe callback could be required in the middle of the call")
            else:
                self.callback = callback

            return self._accumulate_list(item_or_list, callback)
        else:
            self._accumulate_item(item_or_list)
    
            if callback != None:
                return self.then(callback)
            else:
                if self.deprecated_accumulate_warning_given == False:
                    self.deprecated_accumulate_warning_given = True
                    print("sorry, calling the accumulator without a callback, and using \"then(callback)\" instead is now deprecated, it will continue to work, but is not recommended, please just pass the callback, in return accumulate accepts lists now, not just values")
                return self

    def reset(self):
        self.items = []

    async def finish(self):
        await self._call()

    async def stop(self):
        await self.finish()

    #deprecated
    async def then(self, callback):
        self.callback = callback

        if len(self.items) == self.count:
            await self._call()

    async def _accumulate_list(self, list_of_items, callback):
        for item in list_of_items:
            self.items.append(item)

            if len(self.items) == self.count:
                await self.then(callback)
            
        return self

    def _accumulate_item(self, item):
        self.items.append(item)

    async def _call(self):
        items = self.items
        self.reset()

        if len(items) and self.callback != None:
            await smart_await(self.callback(items))

        return self


