import aiohttp

global_instance = None

class MarspyHTTP:
    def __init__(self, configuration={}):
        self.url = configuration.get("url")
        self.get = self._instance_get
        self.post = self._instance_post
        self.put = self._instance_put
        self.delete = self._instance_delete
        self.head = self._instance_head
        self.session = aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False))

    async def stop(self):
        self.session.close()

    async def get_result(self, response):
        if response.content_type == "application/json":
            return await response.json()
        else:
            return await response.text()

    async def _instance_get(self, payload, url=None, headers={}):
        if url == None:
            url = self.url

        async with self.session.get(url, params=payload, headers=headers) as response:
            if response.ok:
                return await self.get_result(response)
            else:
                raise Exception(await response.text())

    async def _instance_post(self, payload={}, url=None, headers={}):
        if url == None:
            url = self.url

        async with self.session.post(url, json=payload) as response:
            if response.ok:
                return await self.get_result(response)
            else:
                raise Exception(f'{await response.text()} when posting to "{response.real_url}"')

    async def _instance_put(self, payload={}, url=None, headers={}):
        if url == None:
            url = self.url

        async with self.session.put(url, json=payload) as response:
            if response.ok:
                return await self.get_result(response)
            else:
                raise Exception(f'{await response.text()} when putting in "{response.real_url}"')

    async def _instance_delete(self, payload={}, url=None, headers={}):
        if url == None:
            url = self.url

        async with self.session.delete(url, json=payload) as response:
            if response.ok:
                return await self.get_result(response)
            else:
                raise Exception(f'{await response.text()} when deleting "{response.real_url}"')
    
    async def _instance_head(self, url=None, headers={}):
        if url == None:
            url = self.url

        async with self.session.head(url) as response:
            if response.ok:
                await self.get_result(response)
                return dict(response.headers)
            else:
                raise Exception(f'{await response.text()} when head "{response.real_url}"')

    @staticmethod
    def _get_instance():
        global global_instance
        if global_instance == None:
            global_instance = MarspyHTTP({})
        return global_instance

    @staticmethod
    def get(url, payload=None, headers={}):
        return MarspyHTTP._get_instance().get(payload, url, headers)

    @staticmethod
    def post(url, payload=None, headers={}):
        return MarspyHTTP._get_instance().post(payload, url, headers)

    @staticmethod
    def put(url, payload=None, headers={}):
        return MarspyHTTP._get_instance().put(payload, url, headers)

    @staticmethod
    def delete(url, payload=None, headers={}):
        return MarspyHTTP._get_instance().delete(payload, url, headers)

    @staticmethod
    def head(url, payload=None, headers={}):
        return MarspyHTTP._get_instance().head(payload, url, headers)

