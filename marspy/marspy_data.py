import sys
import json
from marspy import MarspyUtilities
from .marspy_git import MarspyGit
from marspy.marspy_data_classes.marspy_data_manager import MarspyDataManager
from marspy.marspy_data_classes.marspy_data_schema import MarspyDataSchema
 
MarspyData = sys.modules[__name__]

manager = MarspyDataManager()

# mapping
def map(data, type=None, source=None, destination=None):
    if destination is None:
        raise Exception(f"MarspyData.mao(data, destination=\"the destination\") requires a value for \"destination\"")

    return manager.map(data, type, source, destination)

async def get_mapper_sourcecode(type, source, destination):
    function_definition = f"def {type}_from_{source}_to_{destination}(line):\n"
    return function_definition + await manager.get_mapper_sourcecode(type, source, destination, indent=1)

def get_mapper_function(type, source, destination):
    return manager.get_mapper_function(type, source, destination)

# schemas
async def get_schema(type, name):
    schema_object = await manager.get_schema(type, name)

    if schema_object is not None:
        return schema_object.schema
    else:
        raise Exception(f"schema for {type} {name} cannot be found")

def upload_schema_from_file(type=None, name=None, path=None):
    return manager.upload_schema_from_file(type, name, path)

def create_schema(data, lower_case_tags=False, upper_case_tags=True, snake_case_tags=False):
    data, changed = collect_properties(data, "create_schema")

    if changed == True:
        MarspyUtilities.prettyprint(data)
        schema = MarspyDataSchema.for_data(data, lower_case_tags, upper_case_tags, snake_case_tags)
        MarspyUtilities.prettyprint(schema)
        return schema

def delete_schema(type, name):
    return manager.delete_schema(type, name)

# data
def recognize(data, type=None):
    return manager.recognize_data(data, type)

def collect_properties(data, name="collection_of_properties"):
    return manager.get_collection(name).add(data)

# functions
def call(path, *arguments, **kwarguments):
    if ".py" not in path:
        path = f"{path}.py"
    return MarspyGit.call("bigdata-media/marspy_tool", f"functions/{path}", "function", *arguments, **kwarguments)

def get(type, source, *arguments, **kwarguments):
    return call(f"{type}/{source}/get.py", *arguments, **kwarguments)

def put(type, source, *arguments, **kwarguments):
    return call(f"{type}/{source}/put.py", *arguments, **kwarguments)