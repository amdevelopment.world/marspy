from sqlalchemy import create_engine
from .marspy_utilities import MarspyUtilities
from .marspy_environment import MarspyEnvironment
from .marspy_baseclass import MarspyBaseClass
from .marspy_shared_resources import MarspySharedResources;

class MarspySQL(MarspyBaseClass):
    def __init__(self, configuration={}):
        self.client = None
        
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "SQL_URL", "MarspySQL")
        self.url = MarspyUtilities.processURL(self.url,
            {
                "scheme": "postgresql",
                "port": 5435
            }, {
                "path": configuration.get('database'),
                **configuration
            })

        url_bits = MarspyUtilities.splitURL(self.url)

        if len(url_bits.get("path", "")) == 0:
            raise Exception("MarspySQL requires you to provide the name of a database, either in the url itself, or with the \"database\" parameter")

        self.query = self._instance_query
        self.number_of_queries = 0

        super().__init__(configuration)

    async def stop(self):
        if self.client is not None:
            client = self.client
            self.client = None
            client.dispose()

    def get_client(self):
        if self.client:
            return self.client

        try:
            self.client = create_engine(self.url, pool_pre_ping=True)
        except Exception as e:
            print(f"Erreur impossible de se connecter a pg {e}")
            raise e

        return self.client

    async def _instance_query(self, query, callback=None):
        if callback is not None:
            number_of_parameters_to_callback = MarspyUtilities.count_parameters(callback)
        else:
            number_of_parameters_to_callback = 0

        client = self.get_client()

        async def do(query, callback):
            with client.connect() as connection:
                results = connection.execute(query)
                self.number_of_queries += 1

                if results.returns_rows:
                    list = []
                    column_names = results.keys()
                    
                    for row in results:
                        row_dict = dict(zip(column_names, row))

                        if callback:
                            try:
                                if number_of_parameters_to_callback == 2:
                                    await MarspyUtilities.smart_await(callback(row_dict, row))
                                else:
                                    await MarspyUtilities.smart_await(callback(row_dict))
                            except Exception as e:
                                print(f"exception in callback to sql.query {e}")
                        else:
                            list.append(row_dict)
                    return list
                else:
                    return results

        return await self.queue_call(do, query, callback)

    @staticmethod
    def query(query, callback=None, url_name="SQL_URL"):
        url = MarspyEnvironment.get_value(url_name)
        sql = MarspySharedResources.get("MarspySQL", url)

        if sql is None:
            sql = MarspySQL({
                "url": url
            })
            MarspySharedResources.set("MarspySQL", url, sql)
  
        return sql.query(query, callback)

    @staticmethod
    def get(table, callback=None, fields="*", limit=None, url_name="SQL_URL"):
        if limit is not None:
            query = f"SELECT {fields} FROM {table} LIMIT {limit}"
        else:
            query = f"SELECT {fields} FROM {table}"

        return MarspySQL.query(query, callback, url_name)
