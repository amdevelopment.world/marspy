import types
import os
import json
import boto3
from smart_open import open
from .marspy_utilities import MarspyUtilities
from .marspy_environment import MarspyEnvironment

from marspy.marspy_stream_handlers.marspy_stream_ftp import Handler as FTP
from marspy.marspy_stream_handlers.marspy_stream_http import Handler as HTTP

protocol_handlers = [FTP, HTTP]

class MarspyStream:
    def __init__(self, configuration):
        if configuration is None:
            configuration={}
        elif isinstance(configuration, str):
            configuration = {
                "url": configuration
            }

        self.url = configuration.get("url")
        self.insert_newlines = configuration.get("insert_newlines", True)
        self.stream = None
        self.initialized = False

        session = boto3.Session(
            aws_access_key_id=MarspyEnvironment.get_value("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=MarspyEnvironment.get_value("AWS_SECRET_ACCESS_KEY"),
        )

        self.transport_params_per_scheme = {
            "s3": {'client': session.client('s3')}
        }

    def _initialize(self):
        if self.initialized:
            return

        handler = MarspyStream._get_protocol_handler(self.url, "open")

        if handler is not None:
            self.stream = handler.open(self.url, 'w')
        else:
            url_bits = MarspyUtilities.splitURL(self.url)
            scheme = url_bits["scheme"].lower()

            transport_params = self.transport_params_per_scheme.get(scheme)

            if transport_params is not None:
                self.stream = open(self.url, 'w', transport_params=transport_params)
            else:
                self.stream = open(self.url, 'w')

        self.initialized = True

    async def close(self):
        return await self.stop()

    async def stop(self):
        if self.stream is not None:
            stream = self.stream
            self.stream = None
            stream.close()

    def _put_str(self, string):
        self._initialize()
        buffer = string

        if self.insert_newlines:
            buffer += '\n'

        self.stream.write(buffer)
        return

    def _put_dict(self, dict):
        self._put_str(json.dumps(dict, default=str))

    def _put_list(self, list):
        self._initialize()
        
        buffer = ''

        for item in list:
            if type(item) == dict:
                buffer += json.dumps(item, default=str)
            elif type(item) == str:
                buffer += item
            else:
                buffer += f"{item}"

            if self.insert_newlines:
                buffer += '\n'

        self.stream.write(buffer)
        return

    def put(self, data):
        self._initialize()

        if type(data) == list:
            self._put_list(data)
        elif type(data) == dict:
            self._put_dict(data)
        elif type(data) == str:
            self._put_str(data)
        else:
            self._put_str(f"{data}")
        return

    @staticmethod
    def _get_protocol_handler(url, function_name):
        for protocol_handler in protocol_handlers:
            if hasattr(protocol_handler, function_name) == True:
                if protocol_handler.recognizes_url(url):
                    return protocol_handler

    @staticmethod
    def copy(_from, to):
        from_handler = MarspyStream._get_protocol_handler(_from, "download")
        if from_handler is not None:
            _from = from_handler.download(_from)

        to_handler = MarspyStream._get_protocol_handler(to, "open")
        if to_handler is not None:
            destination = to_handler.open(to, 'wb')
        else:
            destination = open(to, 'wb')

        with open(_from, 'rb') as source:
             while True:
                buffer = source.read(4096)
                if not buffer:
                    break
                destination.write(buffer)

        destination.close()

        if from_handler is not None:
            try:
                os.remove(_from.removeprefix("file://"))
            except Exception:
                pass

    @staticmethod
    def open(url, *args, **kwargs):
        protocol_handler = MarspyStream._get_protocol_handler(url, "download")

        if protocol_handler is not None:
            url = protocol_handler.download(url)

        file = open(url, *args, **kwargs)

        if protocol_handler is not None:
            def delete_and_close(self):
                try:
                    self._close()
                    os.remove(self.name)
                except Exception:
                    pass

            file._close = file.close
            file.close = types.MethodType(delete_and_close, file)

        return file

    @staticmethod
    async def getsize(url):
        protocol_handler = MarspyStream._get_protocol_handler(url, "getsize")

        if protocol_handler is None:
            url_bits = MarspyUtilities.splitURL(url)
            scheme = url_bits["scheme"].lower()
            raise Exception(f"MarspyStream.getsize cannot return the size for the \"{scheme}\"\nurl: {url}")
        else:
            return await protocol_handler.getsize(url)