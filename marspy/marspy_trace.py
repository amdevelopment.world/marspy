import sys
import asyncio
from marspy import MarspyKafka
from marspy import MarspyTask
from marspy import MarspyQueue

queue = MarspyQueue(count=5)

MarspyTrace = sys.modules[__name__]

kafka = None
trace_counter=0

async def stop():
    global kafka, trace_counter
    while trace_counter:
        await asyncio.sleep(0.1)

    await queue.stop()

    try:
        if kafka is not None:
            await kafka.stop()
            kafka = None
    except Exception:
        pass

async def trace_async(identifier, trace):
    global trace_counter
    def callback():
        pass

    async def send(identifier, trace):
        global kafka
        try:
            if kafka is None:
                kafka = MarspyKafka({
                "topic": "marspy-trace"
            })

            await kafka.put({
                "id": identifier,
                "trace": trace
            })
        except Exception as e:
            print(e)

    trace_counter -= 1
    await queue.call(send, callback, identifier, trace)

def trace(identifier, trace):
    global trace_counter
    trace_counter+= 1
    MarspyTask(trace_async(identifier, trace))