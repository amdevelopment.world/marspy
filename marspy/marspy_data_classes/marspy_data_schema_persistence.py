SCHEMA_COLLECTION_NAME = "marspy_data_schemas"
FUNCTION_COLLECTION_NAME = "marspy_data_functions"

from marspy.marspy_data_classes.marspy_data_schema import MarspyDataSchema

class MarspyDataSchemaPersistence:
    def __init__(self, configuration={}):
        from marspy import MarspyMongo

        self.mongo = MarspyMongo({
            "database": "mars-dashboard-prod"
        })

    async def initialize(self):
        await self.mongo.create_collection({
            "name": SCHEMA_COLLECTION_NAME,
            "indexes": [
                {
                    "properties": [("key", 1)],
                    "options": {
                        "unique": True
                    }
                },
                "type",
                "name",
                "paths",
                "tags"
            ]
        })

        await self.mongo.create_collection({
            "name": FUNCTION_COLLECTION_NAME,
            "indexes": [
                {
                    "properties": [("key", 1)],
                    "options": {
                        "unique": True
                    }
                },
                "type",
                "name",
                "paths",
                "tags"
            ]
        })
        
    async def store_schema(self, schema):
        name = schema.name
        _type = schema.type
        try:
            document = schema.toJSON()
            await self.mongo.create_document(SCHEMA_COLLECTION_NAME, document)
            print(f"the schema for \"{_type}\" called \"{name}\" was stored in the {SCHEMA_COLLECTION_NAME} collection")
            return True
        except Exception as e:
            print(e)
            print(f"the schema for \"{_type}\" called \"{name}\" was not stored, probably because a schema with that name and type already exists")
            return False

    async def delete_schema(self, type, name):
        try:
            await self.mongo.delete_document(SCHEMA_COLLECTION_NAME, {
                "type": type,
                "name": name
            })

            return True
        except Exception as e:
            return False
    async def get_schemas(self, paths=None, tags=None, type=None, name=None):
        query = {}
        
        if paths is not None:
            query["paths"] = {
                "$in": paths
            }

        if tags is not None:
            query["tags"] = {
                "$in": tags
            }

        if type is not None:
            query["type"] = type

        if name is not None:
            query["name"] = name

        documents = await self.mongo.query(SCHEMA_COLLECTION_NAME, query)
        return list(map(MarspyDataSchema, documents))