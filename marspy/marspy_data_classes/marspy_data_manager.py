import json
from marspy import MarspyUtilities

from marspy.marspy_data_classes.marspy_data_schema import MarspyDataSchema
from marspy.marspy_data_classes.marspy_data_collector import MarspyDataCollector
from marspy.marspy_data_classes.marspy_data_schema_persistence import MarspyDataSchemaPersistence
from marspy.marspy_data_classes.marspy_data_mapper import MarspyDataMapper

class MarspyDataManager:
    def __init__(self, configuration={}):
        self.schemas = []
        self.collections = {}
        self._persistence = None
        self.mapper = None

    async def map(self, data, type=None, source=None, destination=None):
        if self.mapper == None:
            self.mapper = await self.get_mapper_for_data(data, type, source, destination)

        if self.mapper != None:
            if isinstance(data, list):
                mapped_list = []
                for item in data:
                    mapped_list.append(self.mapper(item))
                return mapped_list
            elif isinstance(data, dict):
                return self.mapper(data)
            else:
                raise Exception("MarspyMapper can only map lists or dicts")
        else:
            raise Exception(f"no mapper available for {destination}")

    async def get_schema(self, type, name):
        if type is None or name is None:
            raise Exception(f"get_schema(type, name) requires a type and a name")

        for schema in self.schemas:
            if schema.type == type and schema.name == name:
                return schema

        persistence = await self.persistence()
        schemas = await persistence.get_schemas(type=type, name=name)

        if len(schemas):
            self.schemas.extend(schemas)
            return schemas[0]

    async def delete_schema(self, type, name):
        persistence = await self.persistence()
        return await persistence.delete_schema(type, name)

    async def load_schemas_with_paths(self, paths):
        persistence = await self.persistence()
        schemas = await persistence.get_schemas(paths=paths)
        self.schemas.extend(schemas)

    async def store_schema(self, schema):
        persistence = await self.persistence()
        persisted = await persistence.store_schema(schema)
        return persisted  

    def upload_schema_from_file(self, type=None, name=None, path=None):
        with open(path, 'r') as file:
            file_content = file.read()

        schema = MarspyDataSchema({
            "type": type,
            "name": name, 
            "schema": json.loads(file_content)
        })

        return self.store_schema(schema)

    def get_collection(self, name):
        collection = self.collections.get(name)
        
        if collection is None:
            collection = MarspyDataCollector(name)
            self.collections[name] = collection

        return collection

    async def get_mapper_sourcecode(self, type=None, source=None, destination=None, indent=0):
        from_schema = await self.get_schema(type, source)
        to_schema = await self.get_schema(type, destination)

        if to_schema is None:
            raise Exception(f"failed to find a schema for {type} {destination}")

        if from_schema is None:
            raise Exception(f"failed to find a schema for {type} {source}")

        source_code = MarspyDataMapper.source_code(from_schema, to_schema)

        return MarspyDataMapper.indent(source_code, indent)

    async def get_mapper_function(self, type=None, source=None, destination=None):
        function_name = f"{type}_from_{source}_to_{destination}"

        source_code = await self.get_mapper_sourcecode(type, source, destination, indent=1)
        source_code_for_function = f"""def {function_name}(line):\n{source_code}"""
       
        my_locals={}
        exec(source_code_for_function, {}, my_locals)
       
        return my_locals[function_name]

    async def get_mapper_for_data(self, data=None, type=None, source=None, destination=None):
        if destination is None:
            raise Exception(f"you have to provide a value for \"destination\" when calling MarspyData.map(data, destination)")

        if type is None or source is None:
            type, source = await self.recognize_data(data, type)

            if type is None:
                raise Exception(f"could not generate a mapper, no schema found for mapping to {destination}")
            else:
                self.type = type
                self.source = source

        mapper = await self.get_mapper_function(type, source, destination)

        return mapper

    async def recognize_data(self, data, type=None):
        best_match = None
        best_match_both = 0

        if isinstance(data, list):
            data = MarspyUtilities.merge_list(data)

        paths = MarspyUtilities.get_list_of_paths_in_dictionary(data)
        await self.load_schemas_with_paths(paths)

        for schema in self.schemas:
            if type is None or schema.type == type:
                in_both, in_data, in_schema = schema.match_data(data)

                # print(schema.type, schema.name, in_both, in_data, in_schema)
                if in_both and in_both > best_match_both:
                    best_match_both = in_both
                    best_match = schema

        if best_match is not None:
            return best_match.type, best_match.name
        else:
            return None, None

    async def persistence(self):
        if self._persistence is None:
            self._persistence = MarspyDataSchemaPersistence()
            await self._persistence.initialize()

        return self._persistence