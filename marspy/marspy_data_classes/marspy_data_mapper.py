import sys
import json
import re
from marspy import MarspyUtilities

MarspyDataMapper = sys.modules[__name__]

def typecaster(from_type, to_type, expression):
    if to_type == "string":
        return 'f""'
    elif to_type == "number" or to_type == "float":
        pass
    elif to_type == "int":
        pass
    elif to_type == "boolean":
        pass
    elif to_type == "array":
        pass
    elif to_type == "object":
        pass

def setter(from_schema, to_settings):
    from_tags = from_schema.tags

    to_type = to_settings.get("type", "object")
    tag = to_settings.get("tag")
    expression = to_settings.get("expression", tag)
    to_condition = to_settings.get("condition")
    default = to_settings.get("default")
    tags = to_settings.get("tags")

    if to_type == "string":
        return 'f"{}"'

    if tag is not None and tag in from_tags:
        from_settings = from_tags[tag]
        return f"#{from_settings.get('type')} => {to_type}\n"
    else:
        if to_condition is not None and expression is not None:
            return f"{to_condition} {expression}"
        else:
            return f"#{tags} unknown => {to_type}\n"

def find_used_tags(from_schema, to_schema):
    to_settings = to_schema.schema.get_settings()
    from_tags = from_schema.tags

    set_of_used_tags = set()
    def find_tags_in(str):
        set_of_tags = set()
        if str is not None:
            words = re.findall(r'\b\w+\b', str)
            for word in words:
                if word in from_tags:
                    set_of_tags.add(word)
        return list(set_of_tags)


    for path, settings in to_settings.items():
        expression = settings.get("expression")
        _if = settings.get("condition")
        tag = settings.get("tag")

        if tag is not None:
            if tag in from_tags:
                set_of_used_tags.add(tag)
            else:
                print(f'tag "{tag}"" not found in source !')

        set_of_tags = set()
        set_of_tags.update(find_tags_in(expression))
        set_of_tags.update(find_tags_in(_if))
        set_of_used_tags.update(set_of_tags)
        settings.set("tags", list(set_of_tags))

    return set_of_used_tags

def generate_reader(template_object, required, root_name = None):
    source_code = ""
    level_of_indentation = 0

    stack = [root_name]

    def indent():
        nonlocal level_of_indentation, source_code
        indentation = ""
        level = level_of_indentation
        while level:
            level -= 1
            indentation += '    '
        source_code += indentation

    def enter(data, path, name):
        nonlocal level_of_indentation, source_code
        if name is not None:
            source_code += "\n"
            indent()
            source_code += f"if \"{name}\" in {stack[-1]}:\n"
            level_of_indentation += 1
            indent()
            source_code += f"{name} = {stack[-1]}[\"{name}\"]\n\n"
            stack.append(name)
        else:
            stack.append(root_name)
        
    def exit(data, path, name):
        nonlocal level_of_indentation, source_code
        level_of_indentation -= 1
        stack.pop()

    def visit(settings, path, name):
        nonlocal level_of_indentation, source_code
        indent()

        tag = settings.get("tag")
        if tag is not None:
            default = settings.get("default")
            property_type = settings.get("type")

            if property_type == "array":
                if settings.is_array_of_objects():
                    source_code += f'{tag} = {stack[-1]}.get("{name}") # array of objects here\n'
                else:
                    source_code += f'array={stack[-1]}.get("{name}")\n'
                    indent()
                    source_code += 'if array and len(array):\n'
                    indent()
                    source_code += f'    {tag}=array[0]\n'
            else:
                if tag in required:
                    source_code += f'{tag} = {stack[-1]}["{name}"]'
                    
                    if default is not None:
                        source_code+= f' or {MarspyUtilities.stringify(default)}'
                    source_code += "\n"
                else:
                    if default is not None:
                        source_code += f'{tag} = {stack[-1]}.get("{name}",{MarspyUtilities.stringify(default)})\n'
                    else:
                        source_code += f'{tag} = {stack[-1]}.get("{name}")\n'

    MarspyUtilities.traverse_data(template_object, enter=enter, exit=exit, visit=visit)
    return source_code

def expression_for(expression, settings):
    property_type = settings.get("type")
    if property_type == "string":
        return f"f'{expression}'"
    else:
        return expression

def generate_writer(template_object, required, from_tags):
    level_of_indentation = 0

    set_optional_properties = ""
    paths_to_deleted_properties = []
    stack = []

    def enter(data, path, name):
        nonlocal level_of_indentation
        if name is not None:
            level_of_indentation += 1
            stack.append(name)
        else:
            stack.append("root_name")
        
    def exit(data, path, name):
        nonlocal level_of_indentation
        level_of_indentation -= 1
        stack.pop()

    def visit(settings, path, name, parent):
        nonlocal level_of_indentation,set_optional_properties
        tag = settings.get("tag")
        expression = settings.get("expression")
        _if = settings.get("condition")

        result = None

        safe_path = ""
        node = settings
        while node:
            if node.name is not None:
                safe_path += f"[\"{node.name}\"]{safe_path}"
            node = node.parent

        if expression is not None:
            expression_for_settings = expression_for(expression, settings)
            if _if is None:
                result = f'###{expression_for_settings}###'
            else:
                set_optional_properties += f'\nif {_if}:'
                set_optional_properties += f'\n    mapped{safe_path} = {expression_for_settings}'
                paths_to_deleted_properties.append(path)
        elif tag is not None:
            if tag not in from_tags:
                set_optional_properties += f'\n# {tag} not found in source'
                paths_to_deleted_properties.append(path)
            else:
                if tag in required:
                    result = f"###{tag}###"
                else:
                    set_optional_properties += f"\nif {tag} is not None:"
                    set_optional_properties += f"\n    mapped{safe_path} = {tag}"
                    paths_to_deleted_properties.append(path)
        else:
            result = "no tag"

        return result

    MarspyUtilities.traverse_data(template_object, enter=enter, exit=exit, visit=visit)
    MarspyUtilities.delete_properties(template_object, paths=paths_to_deleted_properties)

    source_code = MarspyUtilities.stringify(template_object)

    source_code = source_code.replace("\"###", "").replace("###\"", "")
    return f"mapped = {source_code}\n{set_optional_properties}\nreturn mapped"

def indent(source, level):
    indentation = ""
    while level:
        level -= 1
        indentation += "    "
    lines = source.split("\n")
    return indentation + f"\n{indentation}".join(lines)

def source_code(from_schema, to_schema):
    source_code = ""

    from_tags = from_schema.tags
    to_tags = to_schema.tags

    to_required_properties_dict = to_schema.schema.get_settings_for_required_properties()

    to_used_tags_set = find_used_tags(from_schema, to_schema)
    to_used_tags_list = list(to_used_tags_set)
    to_used_tags_list.sort()

    from_used_tags_set = set()
    to_required_tags = set()

    for tag in to_used_tags_list:
        from_path = from_tags[tag]
        from_used_tags_set.add(from_path)

        to_path = to_tags.get(tag)
        if to_path is not None and to_path in to_required_properties_dict:
            to_required_tags.add(tag)

    # initialize values for tags to None
    for tag in to_used_tags_list:
        from_path = from_tags[tag]

        source_code += f"{tag} = None\n"


    def callback(settings, path, name):
        return path in from_used_tags_set

    from_template = from_schema.schema.to_template(callback)

    source_code += "\n# get values\n"
    source_code += generate_reader(from_template, to_required_tags, "line")

    type = to_schema.get("type", "object")
    tag = to_schema.get("tag")
    expression = to_schema.get("expression", tag)
    condition = to_schema.get("condition")
    default = to_schema.get("default")

    source_code += "\n"

    # source_code += f"# return {setter(from_schema, to_schema)}"

    def add_condition():
        nonlocal source_code
        if condition is not None:
            source_code += f"if {condition}:\n    "

    def add_else():
        nonlocal source_code
        if condition is not None and default is not None:
            source_code += f"else:\n    return {json.dumps(default)}"

    if type == "object":
        to_template = to_schema.schema.to_template()
        source_code += generate_writer(to_template, to_required_tags, from_schema.tags)
    elif type == "array":
        add_condition()
        source_code += f"return [{expression}]\n"
        add_else()
    elif type == "number" or type == "float":
        add_condition()
        source_code += f"return float({expression})\n"
        add_else()
    elif type == "int":
        add_condition()
        source_code += f"return int({expression})\n"
        add_else()
    elif type == "boolean":
        add_condition()
        source_code += f"return !!({expression})\n"
        add_else()
    elif type == "string":
        add_condition()
        source_code += f"return f\"{expression}\"\n"
        add_else()
    else:
        raise Exception(f"unknown type for schema {type}")

    return source_code