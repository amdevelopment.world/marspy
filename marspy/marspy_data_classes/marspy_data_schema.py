from marspy import MarspyUtilities
from marspy import MarspyJSONSchema
from marspy import MarspyLogging as logging

class MarspyDataSchema:
    def __init__(self, configuration={}):
        self.type = configuration.get("type")
        self.name = configuration.get("name")

        schema = configuration.get("schema", {})
        self.schema = MarspyJSONSchema(schema)

        # --- create list and set of tags
        self.tags = configuration.get("tags") # returns a dict
        
        if self.tags is None:
            self.tags = self.get_tags()

        # --- create list and set of paths
        self.paths = configuration.get("paths") # returns a list

        if self.paths is None:
            self.paths = self.schema.get_list_of_paths()

        self.set_of_paths = set(self.paths)

    def get(self, key, default=None):
        return self.schema.get(key, default)

    def match_data(self, data):
        set_of_paths = MarspyUtilities.get_set_of_paths_in_dictionary(data)

        in_both = 0
        in_data = 0
        in_schema = 0

        for path in set_of_paths:
            if path in self.set_of_paths:
                in_both += 1
                logging.info(f"match_data to schema, in both: {path}")
            else:
                in_data += 1
                logging.info(f"match_data to schema, only in data: {path}")

        for path in self.set_of_paths:
            if path not in set_of_paths:
                in_schema += 1
                logging.info(f"match_data to schema, only in schema: {path}")

        return in_both, in_data, in_schema

    def get_tags(self):
        tags = {}

        def callback(settings, path, name):
            tag = settings.get("tag")
            type = settings.get("type")

            if tag is not None:
                if tag in tags:
                    raise Exception(f"tag {tag} is used at {path} and at {tags[tag]}")
                tags[tag] = path

        self.schema.traverse(callback)
        return tags

    def get_settings_per_tag(self, schema):
        tags = {}

        def callback(settings, path, name):
            tag = settings.get("tag")
            if tag is not None:
                tags[tag] = path

        MarspyJSONSchema.traverse(schema, callback)
        return tags

    def toJSON(self):
        return {
            "key": f"{self.type} called {self.name}",
            "type": self.type,
            "name": self.name,
            "paths": self.paths,
            "tags": self.tags,
            "schema": self.schema.schema
        }

    @staticmethod
    def for_data(data, lower_case_tags=False, upper_case_tags=True, snake_case_tags=False):
        schema = MarspyJSONSchema.for_data(data)
        
        used_tags = {}

        def callback(settings, path, name):
            if snake_case_tags == True:
                if MarspyUtilities.is_camel_case(name):
                    name = MarspyUtilities.to_snake_case(name)

                if MarspyUtilities.is_camel_case(path):
                    path = MarspyUtilities.to_snake_case(path)

            if name in used_tags:
                name = path

            used_tags[name] = True

            try:
                if name is not None:
                    if upper_case_tags == True:
                        settings.set("tag", name.upper())
                    elif lower_case_tags == True:
                        settings.set("tag", name.lower())
                    else:
                        settings.set("tag", name)
            except Exception as e:
                print(e)

        MarspyJSONSchema.traverse(schema, callback)
        return schema