from marspy import MarspyUtilities

class MarspyDataCollector:
    def __init__(self, name):
        self.name = name
        self.number_of_properties = 0
        self.merged = {}
        self.counter = 0

    def add(self, dictionary):
        self.merged = MarspyUtilities.merge(self.merged, dictionary)
        MarspyUtilities.merge_items_in_lists_keeping_one(self.merged)

        number_of_properties = MarspyUtilities.count_properties_in_dictionary(self.merged)

        self.counter += 1

        if number_of_properties != self.number_of_properties:
            print(f"{number_of_properties - self.number_of_properties} new properties found in {self.counter} lines")
            self.number_of_properties = number_of_properties
            return self.merged, True
        else:
            if self.counter % 1000 == 0:
                print(f"{number_of_properties} properties found in {self.counter} lines")
            return self.merged, False