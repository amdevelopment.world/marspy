import aioredis

class Handler:
    def __init__(self, configuration={}):
        from ..marspy_environment import MarspyEnvironment

        self.client = None
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "REDIS_URL", "MarspyCacheRedis")

    async def stop(self):
        if self.client != None:
            client = self.client
            self.client = None
            client.close()

    def get_client(self):
        if self.client:
            return self.client
        try:
            self.client = aioredis.from_url(url=self.url)
        except Exception as e:
            print(f"Error when tryng to connect to Redis {e}")
            raise e

        return self.client

    async def has(self, key):
        client = self.get_client()
        return await client.exists(key) == 1

    async def get(self, key):
        client = self.get_client()
        return await client.get(key)

    async def set(self, key, data, expire_in=None):
        client = self.get_client()
        return await client.set(key, data)

    async def get_and_set(self, key, data, expire_in=None):
        client = self.get_client()
        return await client.getset(key, data)

    async def expire_at(self, key, date):
        pass

    async def delete(self, key):
        client = self.get_client()
        return await client.delete(key)

