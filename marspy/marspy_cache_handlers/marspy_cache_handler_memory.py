import asyncio
import datetime
from marspy import MarspyTask
from marspy import MarspyUtilities

caches = []

task_running = False

async def CacheTask():
    global task_running
    task_running = True
    while len(caches):
        for cache in caches:
            cache._remove_expired(datetime.datetime.now())
        await asyncio.sleep(1)
    task_running = False

class MarspyMemoryCacheEntry:
    def __init__(self, key, data, expire_at):
        self.key = key
        self.data = data
        self.expire_at = expire_at

    def set_expiration(self, date):
        self.expire_at = date

    def is_expired_at(self, date=None):
        if date is None:
            date = datetime.datetime.now()

        if self.expire_at is not None:
            return self.expire_at < date
        else:
            return False

class Handler:
    def __init__(self, configuration={}):
        self.cache = {}

        caches.append(self)

        if len(caches) == 1:
            MarspyTask(CacheTask())

    async def stop(self):
        global task_running
        caches.remove(self)

        if len(caches) == 0:
            while task_running == True:
                await asyncio.sleep(0.1)

    async def has(self, key):
        return key in self.cache

    async def get(self, key):
        entry = self.cache.get(key)

        if entry is not None:
            if entry.is_expired_at():
                await self.delete(key)
            else:
                return entry.data

    async def set(self, key, data, expire_in=None):
        if expire_in is not None:
            now = datetime.datetime.now()
            expire_at = now + datetime.timedelta(seconds=expire_in)
        else:
            expire_at = None

        self.cache[key] = MarspyMemoryCacheEntry(key, data, expire_at)

    async def get_and_set(self, key, data, expire_in=None):
        value = await self.get(key)
        await self.set(key, data, expire_in)
        return value

    async def expire_at(self, key, date):
        entry = self.cache.get(key)

        if entry is not None:
            entry.set_expiration(date)

    async def delete(self, key):
        if key in self.cache:
            del self.cache[key]

    def _remove_expired(self, date):
        expired_keys = []

        for key, entry in self.cache.items():
            if entry.is_expired_at(date):
                expired_keys.append(key)

        for expired_key in expired_keys:
            del self.cache[expired_key]

