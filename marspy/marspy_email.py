import boto3
from marspy import MarspyUtilities
from marspy import MarspyEnvironment

class MarspyEmail:
    def __init__(self, configuration={}):
        self._from = configuration.get("from")
        self.to = configuration.get("to")
        self.cc = configuration.get("cc")
        self.bcc = configuration.get("bcc")
        self.subject = configuration.get("subject")
        self.body = configuration.get("body")

        self.aws_access_key_id = MarspyEnvironment.get_configuration_value(configuration, "aws_access_key_id", "AWS_ACCESS_KEY_ID", "MarspyRedshift")
        self.aws_secret_access_key = MarspyEnvironment.get_configuration_value(configuration, "aws_access_key_id", "AWS_SECRET_ACCESS_KEY", "MarspyRedshift")

        aws_session = boto3.session.Session(
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key
        )
    
        self.ses = aws_session.client('ses')


    async def send(self, *arguments):
        settings = MarspyUtilities.get_a(dict, arguments, {})
        body = MarspyUtilities.get_a(str, arguments, settings.get("body", self.body), "MarspyEmail", "send", "body")
        _from = settings.get("from", self._from)
        to = settings.get("to", self.to)
        #cc = settings.get("cc", self.to)
        #bcc = settings.get("bcc", self.to)
        subject = settings.get("subject", self.subject)

        if isinstance(to, str):
            to = [to]

        self.ses.send_email(
            Source=_from,
            Destination={
                'ToAddresses': to,
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': "UTF-8",
                        'Data': body,
                    },
                    'Text': {
                        'Charset': "UTF-8",
                        'Data': body,
                    },
                },
                'Subject': {
                    'Charset': "UTF-8",
                    'Data': subject,
                }
            })