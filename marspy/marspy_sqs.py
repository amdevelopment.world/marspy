import sys
from .managers.marspy_sqs_manager import MarspySQSManager

MarspySQS = sys.modules[__name__]

manager = None

def _get_manager():
    global manager
    if manager is None:
        manager = MarspySQSManager()

    return manager

def set_region(region):
    return _get_manager().set_region(region)

def create(name):
    return _get_manager().create_queue(name)
    
def has(name):
    return _get_manager().has_queue(name)

def delete(name):
    return _get_manager().delete_queue(name)

def subscribe(name, callback):
    return _get_manager().subscribe(name, callback)

def put(name, body, attributes=None):
    return _get_manager().put(name, body, attributes)

def stop():
    return _get_manager().stop()