import types
import os
import sys
import json
import inspect
from marspy import MarspyLogging as logging
from urllib.parse import urlsplit, urlparse, urlunparse
from pathlib import Path

MarspyUtilities = sys.modules[__name__]

overide_root = sys.path[0]

def processURL(url, defaults = {}, overrides = {}):
    scheme = defaults.get('scheme', overrides.get('scheme', 'MISSING SCHEME'))

    if url is None:
        url = ''

    if "://" not in url:
        url = f"{scheme}://{url}"

    parsed_url = urlparse(url)

    def get(values, name, alternateName):
        value = values.get(name)

        if value is not None:
            return value

        if alternateName is not None:
            return values.get(alternateName)

    def merged(name, alternateName=None):
        overidingValue = get(overrides, name, alternateName)
        if overidingValue is not None:
            return overidingValue

        value = getattr(parsed_url, name)
        if value is not None and (value or len(value) != 0):
            return value

        defaultValue = get(defaults, name, alternateName)
        if defaultValue is not None:
            return defaultValue

        return ''

    scheme = merged("scheme")
    user = merged("username", "user")
    password = merged("password", "pass")
    domain = merged("hostname", "domain")
    port = merged("port")
    path = merged("path")
    query = merged("query")
    fragment = merged("fragment")

    if port:
        netloc = f"{user}:{password}@{domain}:{port}"
    else:
        netloc = f"{user}:{password}@{domain}"

    url = urlunparse((scheme, netloc, path, None, query, fragment))

    if scheme == '':
        url = url[2:]

    return url

def splitURL(url):
    url_bits = urlsplit(url)

    return {
        "scheme": url_bits.scheme,
        "username": url_bits.username,
        "password": url_bits.password,
        "domain": url_bits.hostname,
        "port": url_bits.port,
        "path": url_bits.path,
        "query": url_bits.query,
        "fragment": url_bits.fragment
    }

def joinURL(url_bits):
    if url_bits.get('user'):
        user_password = f"{url_bits['user']}:{url_bits['password']}@"
    else:
        user_password = ''

    if url_bits['port']:
        netloc = f"{user_password}{url_bits['domain']}:{url_bits['port']}"
    else:
        netloc = f"{user_password}{url_bits['domain']}"

    url = urlunparse((url_bits['scheme'], netloc, url_bits['path'], None, url_bits['query'], url_bits['fragment']))

    if url_bits['scheme'] == '':
        url = url[2:]

    return url

def is_awaitable(result):
    return inspect.iscoroutinefunction(result) or inspect.isawaitable(result)

def is_async(result):
    return inspect.iscoroutinefunction(result) or inspect.isawaitable(result)

async def smart_await(result):
    if is_awaitable(result):
        return await result
    else:
        return result

def count_parameters(function):
    if function is None:
        return -1
    signature = inspect.signature(function)
    return len(signature.parameters)

def get_a(the_type, list, default=None, class_name=None, method_name=None, parameter_name=None):
    if parameter_name is None and method_name is not None:
        accept_parameter_only = True
        parameter_name = method_name
        method_name = class_name
        class_name = default
        default = None
    else:
        accept_parameter_only = False

    for value in list:
        if isinstance(value, the_type):
            return value

    if default is None:
        if accept_parameter_only:
            raise Exception(f"{parameter_name} is required by {class_name}.{method_name}, pass a {the_type} parameter to the \"{method_name}\" method")
        else:
            raise Exception(f"{parameter_name} is required by {class_name}.{method_name}, either pass it as a parameter to the \"{method_name}\" method or when instantiating {class_name}")

    return default

def get_a_mandatory(the_type, arguments, class_name=None, method_name=None, parameter_name=None):
    for value in arguments:
        if isinstance(value, the_type):
            return value

    raise Exception(f"{parameter_name} is required by {class_name}.{method_name}, pass a {the_type} parameter to the \"{method_name}\" method")

def traverse_data(data, visit=None, enter=None, exit=None, path=None, name=None, parent=None):
    if isinstance(data, dict):
        if enter is not None:
            value = enter(data, path, name)
        path = f"{path}." if path is not None else ""

        for key in data:
            value = data[key]
            result = traverse_data(value, visit, enter, exit, f"{path}{key}", key, data)
            if result is not None:
                data[key] = result
        if exit is not None:
            exit(data, path, name)
    elif isinstance(data, list):
        if enter is not None:
            enter(data, path, name)

        path = f"{path}" if path is not None else ""
        length = len(data)
        index = 0

        while index < length:
            item = data[index]
            result = traverse_data(item, visit, enter, exit, f"{path}[{index}]", f"{index}", data)
            if result is not None:
                data[index] = result
            index += 1
        if exit is not None:
            exit(data, path, name)
    else:
        if visit is not None:
            if MarspyUtilities.count_parameters(visit) == 4:
                return visit(data, path, name, parent)
            else:
                return visit(data, path, name)

def delete_properties(data, paths):
    for path in paths:
        if path is not None:
            segments = path.split(".")

            value = data
            parent = None
            segment = None

            for segment in segments:
                parent = value
                value = value.get(segment)

            del parent[segment]

def is_camel_case(string):
    haslower = False
    hasupper = False

    for character in string:
        if character.islower():
            haslower = True
        elif character.isupper():
            hasupper = True

    return haslower and hasupper

def to_snake_case(string):
    snake_case = ""

    for character in string:
        if character.islower() == True:
            snake_case += character
        else:
            snake_case += f"_{character.lower()}"

    return snake_case

def count_properties_in_dictionary(dictionary):
    number_of_properties = 0
    
    def visit(value, path, name):
        nonlocal number_of_properties

        if value is not None:
            number_of_properties += 1

    traverse_data(dictionary, visit)

    return number_of_properties

def get_set_of_paths_in_dictionary(dictionary):
    set_of_paths = set()
    
    def visit(value, path, name):
        set_of_paths.add(path)

    traverse_data(dictionary, visit)

    return set_of_paths

def get_list_of_paths_in_dictionary(dictionary):
    return list(get_set_of_paths_in_dictionary(dictionary))

def merge_items_in_lists_keeping_one(dictionary):
    def enter(value, path, name):
        if isinstance(value, list) and len(value) > 1:
            merged = merge_list(value)
            value[:] = [merged]

    traverse_data(dictionary, enter=enter)

def merge_values(a, b):
    if a is None or isinstance(a, str) or isinstance(a, int) or isinstance(a, float):
        if b is not None:
            a = b
    elif isinstance(a, list):
        if isinstance(b, list):
            a.extend(b)
        else:
            a.append(b)
    elif isinstance(a, dict):
        if isinstance(b, dict):
            for key in b:
                if key in a:
                    a[key] = merge_values(a[key], b[key])
                else:
                    a[key] = b[key]
    return a

def merge_list(list):
    merged = None

    for item in list:
        merged = MarspyUtilities.merge_values(merged, item)

    return merged

def merge(*arguments):
    if len(arguments) < 1 or len(arguments) > 2:
        raise Exception("MarspyData.merge expects one (a list) or two (dicts) parameters")
    elif len(arguments) == 1:
        if isinstance(arguments[0], list):
            return merge_list(arguments[0])
    elif len(arguments) == 2:
        if isinstance(arguments[0], dict) and isinstance(arguments[1], dict):
            return MarspyUtilities.merge_values(arguments[0], arguments[1])


def find_directory_containing(file_or_folder) -> Path:
    if overide_root != None:
        path = Path(overide_root)
    else:
        path = Path(__file__).parent

    logging.debug(f"find_directory_containing(\"{file_or_folder}\") starting at \"{path}\"")

    while str(path) != path.anchor:
        logging.debug(f"looking in \"{path}\"")

        if os.path.exists(f"{str(path)}/{file_or_folder}"):
            logging.debug(f"found {file_or_folder} in \"{path}\"")
            return str(path)

        path = path.parent

def root_directory():
    return find_directory_containing("logs/")

def stringify(value):
    return json.dumps(value, default=str, indent=4, ensure_ascii=False)

def prettyprint(value):
    if isinstance(value, str):
        print(value)
    else:
        print(stringify(value))

async def collect(limit, functions, *arguments):
    result = []
    callback = get_a(types.FunctionType, arguments, None)
    
    arguments = list(arguments)

    if callback is not None:
        arguments.remove(callback)

    number_of_parameters_to_callback = count_parameters(callback)
    def call_callback(name, lines, error):
        if callback is not None:
            if number_of_parameters_to_callback == 2:
                callback(limit, lines)
            elif number_of_parameters_to_callback == 3:
                callback(limit, lines, error)

    for function in functions:
        remaining_lines = limit - len(result)
        if remaining_lines:
            try:
                number_of_parameters_to_function = count_parameters(function) - 1
                lines = await smart_await(function(remaining_lines, *arguments[:number_of_parameters_to_function]))

                if lines is not None:
                    lines = lines[:remaining_lines]
                    
                    call_callback(function.__name__, lines, None)

                    result.extend(lines)
                else:
                    call_callback(function.__name__, [], None)

            except Exception as e:
                call_callback(function.__name__, [], e)

    return result