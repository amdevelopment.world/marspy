import os
import sys
import logging
from pathlib import Path
from logging.config import dictConfig

default_logger = logging.getLogger()
default_logger.setLevel(logging.WARN)
logger = logging.getLogger("marspy_logger")

def find_directory_containing(file_or_folder) -> Path:
    path = Path(sys.path[0])

    logging.info(f"find_directory_containing(\"{file_or_folder}\") starting at \"{path}\"")

    while str(path) != path.anchor:
        logging.info(f"looking in \"{path}\"")

        if os.path.exists(f"{str(path)}/{file_or_folder}"):
            logging.info(f"found {file_or_folder} in \"{path}\"")
            return str(path)

        path = path.parent

def initialize():
    handlers = {
        'stdout_handler': {
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout'
        }
    }

    handlers_names = ['stdout_handler']
    
    root_path = find_directory_containing("logs/")

    if root_path == None:
        print("could not find the \"logs\" directory in the root of your project")
    else:
        handlers['myapp_handler'] = {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': f"{root_path}/logs/logging.log",
            'when': 'midnight',
            'interval': 1,
            'backupCount': 30,
            'level': 'WARNING',
            "encoding": "utf8",
            'formatter': 'standard'
        }

        handlers_names.append('myapp_handler')
    try:
        dictConfig({
            'version': 1,
            'formatters': {
                'standard': {
                    'format': '[%(asctime)s] [%(levelname)s] %(message)s'
                }
            },
            'handlers': handlers,
            'loggers': {
                '': {
                    'level': 'WARNING',
                    'handlers': handlers_names
                }
            },
        })

    except Exception as e:
        print(f"logging could not be configured {e}")

initialize()

def setLevelFirst(level):
    if logger.level == logging.NOTSET:
        logger.setLevel(level)

logger.setLevelFirst = setLevelFirst

# patch setLevel, so that the level of the "special" logger in MarspyUtilities can be set simultaneously
# Utilities require a separate logger, to avoid a cyclic dependence between Logging and Utilities

def setLevel(level):
    logger._setLevel(level)

logger._setLevel = logger.setLevel
logger.setLevel = setLevel

logger.NOTSET = logging.NOTSET
logger.DEBUG = logging.DEBUG
logger.INFO = logging.INFO
logger.WARNING = logging.WARNING
logger.ERROR = logging.ERROR
logger.CRITICAL = logging.CRITICAL

MarspyLogging = logger