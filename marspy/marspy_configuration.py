import asyncio
import types
import yaml
import json
from marspy import MarspyMongo
from marspy import MarspyTask
from marspy import MarspyUtilities
from marspy import MarspyEnvironment
from marspy import MarspyLogging as logging

PREFIX_FOR_SCHEMA = "_schema_for_"

class MarspyConfigurationSubscriber:
    def __init__(self, name, callback, values):
        self.name = name
        self.callback = callback
        self.values = values

    def values_changed(self, values):
        values_changed = False

        for key in values.keys():
            existing_value = self.values.get(key)
            new_value = values.get(key)

            if new_value != existing_value:
                values_changed = True
                self.values[key] = new_value

        return values_changed


class MarspyConfiguration:
    def __init__(self, configuration={}):
        root_path = MarspyUtilities.find_directory_containing("logs/")
        root_name = root_path.rsplit("/", 1)[-1]

        self.name = configuration.get("name", root_name)            
        self.callback = configuration.get("callback")
        self.collection_name = configuration.get("collection", "microservices_configurations")
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "MONGO_URL", "MarspyConfiguration")
        self.subscription_interval = configuration.get("subscription_interval", 10)

        self.schemas = {}
        schema = configuration.get("schema")

        if schema is not None:
            self.schemas[self.name] = schema

        self.subscribers = {}
        self.running = True
        
        self.mongo = MarspyMongo({
            "url": self.url,
            "database": configuration.get("database", "mars-dashboard-prod")
        })

    async def stop(self):
        self.running = False
        await self.mongo.stop()

    async def get(self, *arguments):
        name = MarspyUtilities.get_a(str, arguments, self.name, "MarspyConfiguration", "get", "name")
        documents = await self.mongo.get_documents(self.collection_name, "name", [name])
        
        if len(documents):
            document = documents[0]
            return document.get("configuration", {})

    async def set(self, *arguments):
        name = MarspyUtilities.get_a(str, arguments, self.name, "MarspyConfiguration", "set", "name")
        configuration = MarspyUtilities.get_a_mandatory(dict, arguments, "MarspyConfiguration", "set", "configuration")

        default_configuration = self.apply_schema({}, name)

        configuration = MarspyUtilities.merge(default_configuration, configuration)
        
        await self.mongo.upsert_document(self.collection_name, {
            "$set": {
                "configuration": configuration
            }
        }, {
            "name": name
        })

    async def delete(self, *arguments):
        name = MarspyUtilities.get_a(str, arguments, self.name, "MarspyConfiguration", "delete", "name")

        try:
            await self.mongo.delete_document(self.collection_name, {
                "name": name
            })
        except Exception:
            pass

        try:
            await self.mongo.delete_document(self.collection_name, {
                "name": f"{PREFIX_FOR_SCHEMA}{name}"
            })
        except Exception:
            pass
        

    async def set_schema(self, *arguments):
        name = MarspyUtilities.get_a(str, arguments, self.name, "MarspyConfiguration", "set_schema", "name")
        schema = MarspyUtilities.get_a_mandatory(dict, arguments, "MarspyConfiguration", "set_schema", "schema")

        self.schemas[name] = schema

        await self.mongo.upsert_document(self.collection_name, {
            "$set": {
                "schema": schema
            }
        }, {
            "name": f"{PREFIX_FOR_SCHEMA}{name}"
        })

    def apply_schema(self, *arguments):
        name = MarspyUtilities.get_a(str, arguments, self.name, "MarspyConfiguration", "apply_schema", "name")
        configuration = MarspyUtilities.get_a(dict, arguments, {})

        schema = self.schemas.get(name)

        if schema is not None:
            properties = schema.get("properties")
            if properties is not None:
                for key, settings in properties.items():
                    property_type = settings["type"]
                    if key not in configuration:
                        if "default" in settings:
                            configuration[key] = settings["default"]
                        elif property_type == "array":
                            # configuration[key] = []
                            pass
                        elif property_type == "boolean":
                            configuration[key] = True
                        elif property_type == "string":
                            configuration[key] = ""
                        elif property_type == "number":
                            configuration[key] = 0

        return configuration

    async def subscribe(self, *arguments):
        default_configuration = MarspyUtilities.get_a(dict, arguments, {})
        name = MarspyUtilities.get_a(str, arguments, self.name, "MarspyConfiguration", "subscribe", "name")
        callback = MarspyUtilities.get_a(types.FunctionType, arguments, self.callback, "MarspyConfiguration", "subscribe", "callback")

        default_configuration = self.apply_schema(default_configuration, name)

        configuration = await self.get(name)

        if configuration == None:
            configuration = default_configuration
            await self.set(default_configuration, name)

        # create subscriber
        subscriber = MarspyConfigurationSubscriber(name, callback, configuration)
        self.subscribers[name] = subscriber
        number_of_subscribers = len(self.subscribers)

        if number_of_subscribers == 1:
            MarspyTask(self.start_polling())

        return configuration

    async def start_polling(self):
        while self.running:
            await asyncio.sleep(self.subscription_interval)

            for subscriber in self.subscribers.values():
                new_values = await self.get(subscriber.name)
                values_changed = subscriber.values_changed(new_values)

                if values_changed:
                    try:
                        if MarspyUtilities.count_parameters(subscriber.callback) == 1:
                            await MarspyUtilities.smart_await(subscriber.callback(new_values))
                        else:
                            await MarspyUtilities.smart_await(subscriber.callback(subscriber.name, new_values))

                    except Exception as e:
                        print(f"exception when calling configuration callback{e}")
                    

    @staticmethod
    def read_yaml(env=None):
        root_path = MarspyUtilities.find_directory_containing("conf/")

        logging.debug(f"MarspyConfiguration root_path: {root_path}")

        configuration_for_env = None

        def read(name):
            try:
                file_name = f"{root_path}/conf/{name}.yaml"

                with open(file_name) as f:
                    logging.debug(f"MarspyConfiguration reading from {file_name}")
                    return yaml.safe_load(f)
            except Exception:
                return None

        default_configuration = read("all")

        if default_configuration is None:
            default_configuration = read("ALL")
        if default_configuration is None:
            default_configuration = read("default")
        
        if env is not None:
            configuration_for_env = read(f"{env}")
            if configuration_for_env is None:
                configuration_for_env = read(f"conf_{env}")

        merged = MarspyUtilities.merge(default_configuration or {}, configuration_for_env or {})

        logging.debug(f"MarspyConfiguration merged yaml into: {json.dumps(merged, indent=4)}")

        return merged

    @staticmethod
    def read(env):
        print("please use MarspyCpnfiguration.read_yaml instead of read")
        return MarspyConfiguration.read_yaml(env)
