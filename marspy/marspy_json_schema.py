import datetime
from dateutil import parser
from marspy import MarspyUtilities


class MarspyJSONSchema:
    def __init__(self, schema):
        self.schema = schema
        self.traverse = self._instance_traverse

    def get(self, key, default=None):
        return self.schema.get(key, default)

    def _instance_traverse(self, callback):
        return MarspyJSONSchema.traverse(self.schema, callback)

    def get_set_of_paths(self):
        set_of_paths = set()

        def callback(settings, path, name):
            set_of_paths.add(path)
        self.traverse(callback)

        return set_of_paths

    def get_list_of_paths(self):
        return list(self.get_set_of_paths())

    def get_settings(self):
        settings_per_path = {}

        def callback(settings, path, name):
            settings_per_path[path] = settings
        
        self.traverse(callback)

        return settings_per_path

    def get_settings_for_required_properties(self):
        settings_per_path = {}

        def callback(settings, path, name):
            if settings.parent is not None:
                required = settings.parent.get("required")

                if required is not None and settings.name in required:
                    settings_per_path[path] = settings
        
        self.traverse(callback)

        return settings_per_path
    
    def to_instance(self, include_if=None):
        value_map = {}

        def callback(settings, path, name):
            nonlocal value_map
            if include_if is not None:
                include = include_if(settings, path, name)
            else:
                include = True

            if include == True:
                list_of_settings = []

                while settings:
                    list_of_settings.insert(0, settings)
                    settings = settings.parent

                parent = None
                for setting in list_of_settings:
                    value = value_map.get(setting.path)

                    if value == None:
                        value = _instance_of(setting.get("type"))
                        value_map[setting.path] = value

                    if parent is not None:
                        if isinstance(parent, dict):
                            parent[setting.name] = value
                    parent = value

        self.traverse(callback)

        return value_map[None]

    def to_template(self, include_if=None):
        value_map = {}

        def callback(settings, path, name):
            nonlocal value_map

            if include_if is not None:
                include = include_if(settings, path, name)
            else:
                include = True

            if include == True:
                list_of_settings = []

                node = settings
                while node:
                    list_of_settings.append(node)
                    node = node.parent

                parent = None
                index = len(list_of_settings)-1

                while index >= 0:
                    setting_in_list = list_of_settings[index]
                    value = value_map.get(setting_in_list.path)
                    property_type = setting_in_list.get("type")

                    if value == None:
                        if index == 0 and property_type != "object":
                            value = settings
                        else:
                            value = _instance_of(property_type)
                        value_map[setting_in_list.path] = value

                    if parent is not None:
                        if isinstance(parent, dict):
                            parent[setting_in_list.name] = value
                    index -= 1
                    parent = value

        self.traverse(callback)

        return value_map.get(None)

    @staticmethod
    def traverse(schema, callback, settings = None, path=None, parent=None):
        if schema is None:
            return

        parent = MarspyJSONSchemaSettings(settings or schema, parent, path.split('.')[-1] if path else None)
        callback(parent, path,  path.split('.')[-1] if path else None)

        properties = schema.get("properties")
        
        if properties is not None:
            for name, settings in properties.items():
                sub_path = f"{path}.{name}" if path is not None else name
                
               # callback(MarspyJSONSchemaSettings(settings, parent, name), sub_path, name)
                
                items = settings.get("items")

                if items is not None:
                    MarspyJSONSchema.traverse(items, callback, settings, sub_path, parent)
                else:
                    MarspyJSONSchema.traverse(settings, callback, settings,  sub_path, parent)

    @staticmethod
    def for_data(data):
        schema = {}
        stack = [schema]

        def enter(value, path, name):
            current_properties = stack[-1]

            if name is not None:
                new_properties = {}
                current_properties[name] = new_properties
            else:
                new_properties = current_properties

            if value is not None:
                json_type = _json_type_of(value)

                new_properties["type"] = json_type
                
                if new_properties["type"] == "string":
                    json_format = _json_format_of(value)

                    if json_format is not None:
                        new_properties["format"] = json_format

            if isinstance(value, list):                    
                properties = {}
                stack.append(properties)

                if len(value):
                    new_properties["items"] = properties
            elif isinstance(value, dict):                    
                properties = {}
                stack.append(properties)

                new_properties["properties"] = properties                    

        def exit(value, path, name):
            stack.pop()

        MarspyUtilities.traverse_data(data, enter=enter, exit=exit, visit=enter)

        return schema

class MarspyJSONSchemaSettings:
    def __init__(self, settings, parent, name):
        self.settings = settings
        self.parent = parent
        self.name = name
        self.path = name

        node = self.parent
    
        while node:
            if node.parent is not None:
                self.path = f"{node.name}.{self.path}"
            node = node.parent

    def get(self, name, default=None):
        return self.settings.get(name, default)

    def set(self, name, value):
        self.settings[name] = value


    def is_array(self):
        return self.get("type") == "array"

    def is_in_array(self):
        node = self.parent
        while node is not None:
            if node.is_array():
                return True
        return True

    def is_array_of_objects(self):
        if self.is_array():
            items = self.get("items")
            if items is None:
                return False
            else:
                item_type = items.get("type")
                return item_type is not None and item_type == "object"

    def toJSON(self):
        return self.settings

# -----------------------------------------------------------------------------------------------
#                                           internal functions
# -----------------------------------------------------------------------------------------------

def _instance_of(property_type):
    if property_type == "object":
        return {}
    elif property_type == "array":
        return []
    elif property_type == "string":
        return ""
    elif property_type == "boolean":
        return False
    elif property_type == "number":
        return 0
    elif property_type == "int":
        return 0
    elif property_type == "float":
        return 0.0
    else: # default to string
        return ""

def _json_type_of(value):
    if isinstance(value, dict):
        return "object"
    elif isinstance(value, list):
        return "array"
    elif isinstance(value, str):
        return "string"
    elif isinstance(value, bool):
        return "boolean"
    elif isinstance(value, int):
        return "number"
    elif isinstance(value, float):
        return "number"
    else: # default to string
        return "string"

def _is_number_in_string(value):
    try:
        float_number = float(value)
        int_number = int(value)

        if str(int_number) == value or str(float_number) == value:
            return True
        else:
            return False
    except Exception as e:
        return False

def _is_date(value):
    try:
        date = parser.parse(value)
        return True
    except Exception as e:
        return False

def _json_format_of(value):
    if isinstance(value, str):
        if _is_number_in_string(value):
            return None
        elif _is_date(value):
            return "date"
        return None
    elif isinstance(value, datetime.datetime):
        return "date"