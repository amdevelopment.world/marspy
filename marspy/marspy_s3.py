import sys
from .managers.marspy_s3_manager import MarspyS3Manager

MarspyS3 = sys.modules[__name__]

manager = None

def _get_manager():
    global manager
    if manager is None:
        manager = MarspyS3Manager()

    return manager

def set_region(region):
    return _get_manager().set_region(region)

def query(url, query, callback=None):
    return _get_manager().query(url, query, callback)

def sample(url, lines=None, callback=None):
    return _get_manager().sample(url, lines, callback)

def stop():
    return _get_manager().stop()