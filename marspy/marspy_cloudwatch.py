import boto3
from botocore.config import Config
from marspy import MarspyEnvironment

class MarspyCloudwatch():
    def __init__(self, configuration={}):
        self.namespace = configuration.get("namespace")
        aws_region = configuration.get("region", "eu-west-1")

        aws_access_key_id = MarspyEnvironment.get_configuration_value(configuration, "aws_access_key_id", "AWS_ACCESS_KEY_ID", "MarspyCloudwatchPrototype")
        aws_secret_access_key = MarspyEnvironment.get_configuration_value(configuration, "aws_access_key_id", "AWS_SECRET_ACCESS_KEY", "MarspyCloudwatchPrototype")

        aws_config = Config(region_name=aws_region)
        aws_session = boto3.session.Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key
        )

        self.client = aws_session.client('cloudwatch', config=aws_config)

    async def put(self, metric_data):
        return self.client.put_metric_data(
            MetricData=metric_data,
            Namespace=self.namespace
        )