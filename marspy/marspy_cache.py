import datetime
import importlib
import json

class MarspyCache:
    def __init__(self, configuration={}):
        self.type = configuration.get("type", "memory")
        self.name = configuration.get("name")
        self.ttl = configuration.get("ttl")

        if self.name is None:
            raise Exception("MarspyCache requires a name to distinguish cache keys from eachother")

        # charger le bon module de marspy_cache)handlers
        module = importlib.import_module(f".marspy_cache_handlers.marspy_cache_handler_{self.type}", package='marspy')
        self.handler = module.Handler(configuration)

    def _prefix_key(self, key):
        return f"{self.name}{key}"

    def has(self, key):
        return self.handler.has(self._prefix_key(key))

    async def get(self, key):
        value = await self.handler.get(self._prefix_key(key))
        if value is not None:
            value = json.loads(value)
        return value

    def set(self, key, value, expire_in=None):
        return self.get_and_set(key, value, expire_in)
    
    def get_and_set(self, key, value, expire_in=None):
        value = json.dumps(value, default=str).encode('utf-8')

        if expire_in is None:
            expire_in = self.ttl

        return self.handler.get_and_set(self._prefix_key(key), value, expire_in)

    def expire_in(self, key, seconds):
        now = datetime.datetime.now()
        expire_at = now + datetime.timedelta(seconds=seconds)

        return self.handler.expire_at(self._prefix_key(key), expire_at)

    def expire_at(self, key, date):
        return self.handler.expire_at(self._prefix_key(key), date)

    def delete(self, key):
        return self.handler.delete(self._prefix_key(key))

    def stop(self):
        return self.handler.stop()
