from marspy import MarspyQueue
from marspy import MarspyHTTP


class MarspyFissionFunction:
    def __init__(self, configuration={}):
        if "://" in configuration['router']:
            url = f"{configuration['router']}{configuration['path']}"
        else:
            url = f"http://{configuration['router']}{configuration['path']}"

        self.http = MarspyHTTP({
          "url": url
          })

        self.queue = MarspyQueue(size=configuration['concurrency'])

    async def stop(self):
        return await self.finish()

    async def finish(self):
        await self.queue.empty()

    def set_concurrency(self, concurrency):
        self.queue.set_space(concurrency)

    async def get(self, payload, callback):
        async def get_function(payload):
          return await self.http.get(payload)
        await self.queue.call(get_function, callback, payload)

    async def post(self, payload, callback):
        async def post_function(payload):
          return await self.http.post(payload)
        await self.queue.call(post_function, callback, payload)

    async def put(self, payload, callback):
        print("put has not been tested yet")
        async def put_function(payload):
          return await self.http.put(payload)
        await self.queue.call(put_function, callback, payload)

    async def delete(self, payload, callback):
        print("delete has not been tested yet")
        async def delete_function(payload):
          return await self.http.delete(payload)
        await self.queue.call(delete_function, callback, payload)
