import sys
import urllib.parse
from .marspy_environment import MarspyEnvironment
from .marspy_http import MarspyHTTP
from .marspy_utilities import MarspyUtilities

BASE_URL = "https://gitlab.com/api/v4"

MarspyGit = sys.modules[__name__]

cache = {}

async def get(repository, path):
    private_token = MarspyEnvironment.get_value("GITLAB_PRIVATE_TOKEN")

    if private_token is None:
        raise Exception('MarspyGit, like most modules, requires env variables, please call await MarspyEnvironment.get("DEV or PROD") before calling MarspyGit')

    key = f"source/{repository}/{path}"

    content = cache.get(key)

    if content is None:
        content = await MarspyHTTP.get(f"{BASE_URL}/projects/{urllib.parse.quote_plus(repository)}/repository/files/{urllib.parse.quote_plus(path)}/raw?ref=master",
            headers={
                "PRIVATE-TOKEN": private_token
            }
        )

        cache[key] = content

    return content

async def compile(repository, path):
    key = f"functions/{repository}/{path}"
    functions = cache.get(key)

    if functions is None:
        source = await get(repository, path)

        if source is not None:
            scope={}
            exec(source, scope, scope)
            cache[key] = scope
            functions = scope

    return functions

async def call(repository, path, name, *arguments, **kwarguments):
    function_name = name

    functions = await compile(repository, path)

    if functions is not None:
        return await MarspyUtilities.smart_await(functions[function_name](*arguments, **kwarguments))


async def call_function_in_file(function_name, path, *arguments, **kwarguments):
    root = MarspyUtilities.find_directory_containing("functions")
    full_path = f"{root}/{path}"

    with open(full_path) as file:
        source = file.read()
        
        scope={}
        exec(source, scope, scope)
        function = scope.get(function_name)

        if function:
            return await MarspyUtilities.smart_await(function(*arguments, **kwarguments))
        else:
            print(f"function {function_name} could not be found in {full_path}")