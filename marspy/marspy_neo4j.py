import types
from neo4j import AsyncGraphDatabase
from .marspy_utilities import MarspyUtilities
from .marspy_environment import MarspyEnvironment
from .marspy_baseclass import MarspyBaseClass
from .marspy_shared_resources import MarspySharedResources

class MarspyNeo4jSession():
    def __init__(self, neo4j):
        self.neo4j = neo4j

    async def __aenter__(self):
        self.session = self.neo4j.create_session()
        return self.session

    async def __aexit__(self, type, value, traceback):
        await self.session.close()

class MarspyNeo4j(MarspyBaseClass):
    def __init__(self, configuration={}):
        self.driver = None
        self._shared_session = None
        self.url = MarspyEnvironment.get_configuration_value(configuration, "url", "NEO4J_URL", "MarspyNeo4j")

        self.url = MarspyUtilities.processURL(self.url,
            {
                "scheme": "bolt",
                "port": 7687
            }, {
                "path": configuration.get('database'),
                **configuration
            })
    
        url_bits = MarspyUtilities.splitURL(self.url)
        self.username = url_bits['username']
        self.password = url_bits['password']

        url_bits['username'] = ''
        url_bits['password'] = ''
        self.url = MarspyUtilities.joinURL(url_bits)

        self.query = self._instance_query
        super().__init__(configuration)

    async def stop(self):
        if self.driver is not None:
            driver = self.driver
            self.driver = None
            await driver.close()

    async def raw_query(self, query, *arguments, **keyarguments):

        if "session" in keyarguments:
            session = keyarguments["session"]
            del keyarguments["session"]
        else:
            session = self._get_shared_session()

        def do(query, session, arguments, keyarguments):
            return session.run(query, *arguments, **keyarguments)

        return await self.queue_call(do, query, session, arguments, keyarguments)

    async def _instance_query(self, query, *arguments, **keyarguments):
        arguments = list(arguments)

        if len(arguments) and type(arguments[0] == types.FunctionType):
            callback = arguments[0]
            arguments.remove(callback)
        elif "callback" in keyarguments:
            callback = keyarguments["callback"]
            del keyarguments["callback"]
        else:
            callback = None

        arguments = tuple(arguments)

        if "session" in keyarguments:
            session = keyarguments["session"]
            del keyarguments["session"]
        else:
            session = self._get_shared_session()
            

        if callback is not None:
            number_of_parameters_to_callback = MarspyUtilities.count_parameters(callback)
        else:
            number_of_parameters_to_callback = 0

        async def do(query, callback, session, arguments, keyarguments):
            result = await session.run(query, *arguments, **keyarguments)

            if callback is not None:
                if number_of_parameters_to_callback == 1:
                    async for record in result:
                        await MarspyUtilities.smart_await(callback(dict(record[0])))
                else:
                    async for record in result:
                        await MarspyUtilities.smart_await(callback(dict(record[0]), record))

                return []
            else:
                records = []

                async for record in result:
                    records.append(record)

                return records

        return await self.queue_call(do, query, callback, session, arguments, keyarguments)

    def create_session(self):
        driver = self._get_driver()
        return driver.session()

    def session(self):
        return MarspyNeo4jSession(self)

    # internal methods
    def _get_driver(self):
        if self.driver:
            return self.driver
        try:
            self.driver = AsyncGraphDatabase.driver(self.url, auth=(self.username, self.password))

        except Exception as e:
            print(f"Erreur impossible de se connecter a neo4j {e}")
            raise e

        return self.driver

    def _get_shared_session(self):
        if self._shared_session:
            return self._shared_session

        self._shared_session = self.create_session()
        return self._shared_session

    @staticmethod
    async def query(query, *arguments, **keyarguments):
        url = MarspyEnvironment.get_value("NEO4J_URL")
        neo4j = MarspySharedResources.get("MarspyNeo4j", url)

        if neo4j is None:
            neo4j = MarspyNeo4j({
                "url": url
            })
            MarspySharedResources.set("MarspyNeo4j", url, neo4j)

        return neo4j.query(query, *arguments, **keyarguments)

    @staticmethod
    def get(label, callback=None, limit=None):
        first_character = label[0].lower()

        if limit is not None:
            query = f"MATCH ({first_character}:{label}) RETURN {first_character} LIMIT {limit}"
        else:
            query = f"MATCH ({first_character}:{label}) RETURN {first_character}"

        return MarspyNeo4j.query(query, callback)
